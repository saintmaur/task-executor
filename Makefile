VERSION := $(shell git describe --tags 2> /dev/null || echo no-tag)
BRANCH := $(shell git symbolic-ref -q --short HEAD)
COMMIT := $(shell git rev-parse HEAD)

# Go related variables.
INSTALL_DIR ?= ~/go/bin
BUILD_TIME := $(shell date '+%Y-%m-%d_%H:%M:%S_%Z')
# Use linker flags to provide version/build settings
LDFLAGS := -X main.version=$(VERSION) -X main.commit=$(COMMIT) -X main.buildTime=$(BUILD_TIME)

docker:
	@docker build --rm --build-arg PROJECT_NAME="$(target)" -t saintmaur/$(target):$(VERSION) .
	@docker push saintmaur/$(target):$(VERSION)
docker_all:
	@target=tasker make docker
	@target=servlex make docker
	@target=tgbot make docker

docker-chrome:
	@docker build --force-rm -t saintmaur/chrome-tasker:$(VERSION) -f Dockerfile_chrome .
	@docker push saintmaur/chrome-tasker:$(VERSION)

deps:
	@echo Build with \"`go version`\"
	@echo "Install dependencies..."
	@time -p go mod download && time -p go mod tidy && time -p go mod vendor
	@echo "------------------"
	@echo "Inspect the code..."
	@time -p go vet ./cmd/...
	@echo "------------------"

build:
	@echo "Build '$(target)'..."
	@time -p go build -ldflags "$(LDFLAGS) -X main.appName=$(target)" -o $(INSTALL_DIR)/$(target) cmd/$(target)/main.go
	@echo "------------------"

build_all: deps
	@target=tasker make build
	@target=servlex make build
	@target=tgbot make build
	@echo "Build is finished!"

coverage:
	@./stop-mongo.sh || true
	@./start-mongo.sh
	@./stop-rabbitmq.sh || true
	@./start-rabbitmq.sh
	@sleep 3
	@go clean -testcache
	@go test -coverprofile=coverage.out ./...
	@go tool cover -html=coverage.out
	@./stop-rabbitmq.sh
	@./stop-mongo.sh

test_local:
	@./stop-mongo.sh || true
	@./start-mongo.sh
	@./stop-rabbitmq.sh || true
	@./start-rabbitmq.sh
	@go clean -testcache
	@go test -cover ./internal/...
	@./stop-rabbitmq.sh
	@./stop-mongo.sh

test:
	@go clean -testcache
	@go test -cover ./internal/...

bench:
	@go test -bench=. -benchmem -run=^$$ ./internal/executor

race:
	@./stop-mongo.sh || true
	@./start-mongo.sh
	@go test -race ./...
	@./stop-mongo.sh