FROM golang:1.22-alpine3.18 as build
RUN which go
RUN apk add make git g++ libc-dev
RUN mkdir /go/src/task-executor
COPY . /go/src/task-executor
WORKDIR /go/src/task-executor
ENV INSTALL_DIR=/go/src/task-executor/
RUN make build_all
FROM alpine:3.18 as final
ARG PROJECT_NAME
ENV PROJECT_NAME=${PROJECT_NAME}
WORKDIR /etc/task-executor
COPY --from=build /go/src/task-executor/${PROJECT_NAME} .