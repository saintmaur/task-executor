![coverage](https://gitlab.com/saintmaur/task-executor/badges/master/coverage.svg?job=test)

## Task types

---

- `http` – make an HTTP request

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `target` – the URL
  - `method` – the method, one of: (get|head|options|trace|put|delete|post|patch|connect)
  - `data` – the data to send
  - `extra` – a JSON-string of key-value pairs
    E.g.: `{"Accept": "application/json", "Content-Length": "100"}`

---

- `stat` – send a metric in Prometheus format

  params interpretation:

  - `name` – the name of the metric
  - `data` – the value to stat (must be convertible to integer)
  - `extra` – a JSON-string of key-value pairs for label/value

  The labels list is always appended with the name of the executor

---

- `json_get` – get the json value

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – the json data for the `target` to apply to
    If missing, the `target` is treated as json data and the parsing result is recorded instead.
  - `target`

    There are two cases:

    - if the `data` is provided represents a JSON path to apply
    - if the `data` is not provided represents a JSON string or a map to parse

---

- `json_set` – set the json value

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – the json data for the `target` to apply to
  - `target` – the path to store the value
  - `extra` – the value to store

---

- `print` – print a message to stdout or log file

  params interpretation:

  - `name` – formal
  - `data` – the message to print
  - `target` – "log" (to output to the log filepath) or "" (for stdout)
  - `extra` – the level for printing to the log file

---

- `set` – set the context value with the given key

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – the value to be set
  - `extra` – the type (int|float|bool|string(default)); if casting fails, a string typed value is set

---

- `unset` – unset (delete) the context value with the given key

  params interpretation:

  - `name` – the context key for the result value to be saved with

---

- `shell` – execute a shell command (`sh -c "<command_string>"`)

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `target` – the shell command (**_direct_**)
  - `data` – environment variables key=value list separated with the first symbol

---

- `remote_shell` – execute a remote shell command

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `target` – the shell command
  - `data` – a json string with ssh connection data
    ({"host":"<host>", "port": "(default:22)", "user": "(default:<current user>)", "key":"(default:<current user home dir>/.ssh/id_rsa)"})

---

- `file_read` – read the contents of the file

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `target` – the file path to read

---

- `file_write` – write the data to the file

  params interpretation:

  - `name` – formal
  - `target` – the file path to write to
  - `data` – the data to write
  - `extra` – any value, distinct from `0` or `false` is treated as `true`, signals to truncate the previous contents

---

- `file_stat` – get file info

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `target` – the file path
    Returns an object with methods:

  ```
  Name() string       // base name of the file
  Size() int64        // length in bytes for regular files; system-dependent for others
  Mode() FileMode     // file mode bits
  ModTime() time.Time // modification time
  IsDir() bool        // abbreviation for Mode().IsDir()
  Sys() any           // underlying data source (can return nil)
  ```

---

- `array_remove` – remove an element from the array by value

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – the key of an array (**_non-templatable_**)
  - `target` - the value key to remove (**_non-templatable_**)

---

- `array_join` – convert the given array to a string

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – the array
  - `extra` – the separator (**_direct_**)

---

- `array_insert` – insert a value into the given position in the array

  params interpretation:

  - `name` – the name to save the result value
  - `data` – the array name
  - `target` – the position
  - `extra` – the data to insert

---

- `loop` – process the given array with a predefined scenario

  params interpretation:

  - `name` – formal name to used in loop variable naming
  - `data` - the array to process (**_non-templatable_**)
  - `target` - the name of predefined scenario

  For each iteration a variable named by a pattern `<loop_task_name>_item_value` is set to the current value
  For each iteration a variable named by a pattern `<loop_task_name>_item_key` is set to the current index

---

- `regexp` – get values by regexp

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – the data to search with regexp
  - `target` – the regular expression
    The result is always an array

---

- `db_select` – make a `select` request to a data base

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `target` - the DSN
  - `data` - the request to execute
  - `extra` – the DB driver name (postgres|mysql)

  The result is a json string representing an array of objects with keys corresponding to the names of the selected fields and values of that fields

---

- `db_exec` – make other requests to a database

  params interpretation:

  - `name` – formal
  - `target` - the DSN
  - `data` - the request to execute
  - `extra` – the DB driver name (postgres|mysql)

---

- `wait` – sleep for the given duration

  params interpretation:

  - `name` – formal
  - `data` – a duration string (`200ms, 10s, 2m, etc.`)

---

- `run` – execute a predefined scenario

  params interpretation:

  - `name` – formal
  - `target` – the name of the predefined scenario
  - `async` – rather to run the chosen scenario asyncronously
  - `extra` – if `async` is true set custom name to the cloned executor or it'll be named `executor_cloned`

---

- `mongo_get` – get data from mongodb

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `target` – a dsn string, e.g.: "_mongodb://1.1.1.1:27017/?db=cms&collection=ips_"
  - `data` – a filter string, e.g.: "_{"address_from":{"$in":["3C3XxbaKKTpuZeogGCfWfXKpW8hXQiBupM","TXXAiYoKnUZ2wtjEc41oDkQxRkYk6X18SV"]}}_"
  - `extra` – a sort string, e.g.: "_{"timestamp": -1}_"

---

- `mongo_aggregate` – make an aggregation request to the mongodb

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `target` – a dsn string, e.g.: "_mongodb://1.1.1.1:27017/?db=cms&collection=ips_"
  - `data` – a string representing a pipeline, e.g.: "_[{"$lookup":{"from":"users","localField":"userId","foreignField":"id","as":"user"}}]_"
  - `extra` – a sort string, e.g.: "_{"timestamp": -1}_"

---

- `mongo_update` – update records in mongodb

  params interpretation:

  - `name` – the context key for the result value to be saved with

    The result is represented with two values: `<name>_matched` and `<name>_modified`.

  - `target` – a dsn string, e.g.: "_mongodb://1.1.1.1:27017/?db=cms&collection=ips_"
  - `extra` – a filter string, e.g.: "_{"hash":{"$in":["3C3XxbaKKTpuZeogGCfWfXKpW8hXQiBupM","TXXAiYoKnUZ2wtjEc41oDkQxRkYk6X18SV"]}}_"
  - `data` – an update string, e.g.: "_{$set: {bool_key: true}}_"

---

- `mongo_delete` – delete records from mongodb

  params interpretation:

  - `name` – the context key for the result value to be saved with

    The result is represented by the deleted records count.

  - `target` – a dsn string, e.g.: "_mongodb://1.1.1.1:27017/?db=cms&collection=ips_"
  - `data` – a filter string, e.g.: "_{"hash":{"$in":["3C3XxbaKKTpuZeogGCfWfXKpW8hXQiBupM","TXXAiYoKnUZ2wtjEc41oDkQxRkYk6X18SV"]}}_"

---

- `mongo_insert` – insert records into mongodb

  params interpretation:

  - `name` – the context key for the result value to be saved with

    The result is represented by the inserted records count.

  - `target` – a dsn string, e.g.: "_mongodb://1.1.1.1:27017/?db=cms&collection=ips_"
  - `data` – a string representing an array of objects

---

- `intern` – convert a string representing a JSON object to a map{string}interface{}

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – a JSON string

---

- `unintern` – convert a Golang object (map{string}interface{}) to JSON

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – the key name of the object to unintern

---

- `replace` – replace a substring in a string

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – the value where to search a substring
  - `target` – the substring
  - `extra` – the value to replace the substring with

---

- `replace_regexp` – replace a substring in a string by regexp

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – the value where to search a substring
  - `target` – the regexp string to replace
  - `extra` – the regexp value to replace the substring with

---

- `csv` – read a csv-structured data

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – the value to parse
  - `extra` – the separator string

---

- `sign` – sign a text with a key. In other words, create a MAC (message authentication code)
  !!! NB: result is a HEX value. For further processing it might be needed to decode it. !!!

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – the data to be signed
  - `target` – the algorithm to use (md5|sha1|sha256|sha512)
  - `extra` – the key to sign the data with

---

- `hash` – generate a hash of the data by an algorithm

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – the data to be hashed
  - `target` – the algorithm to use (md5|sha1|sha256|sha512)

---

- `case` – change the case of the string

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – the data to change
  - `target` – the case (upper|lower)

---

- `cron` – repeat a scenario by cron description

  params interpretation:

  - `name` – formal
  - `data` – the cron string (e.g. '0 0 \* \* \*') or time duration (e.g. '5s')
  - `target` – the predefined scenario name to run periodically

---

- `break` – stop the repeated scenario

  params interpretation:

  - `name` – the name of the periodical process

---

- `time` – get the time in nanoseconds

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – the string value to convert to time (if empty current time is recorded)
  - `extra` – the format of the date given in `data` parameter

---

- `time_shift` – shift the time

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `target` – the duration (a number with unit: s, m or h)
  - `data` – the value to shift

---

- `time_format` – convert time to the formatted string

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – the value to format
  - `extra` – the format

---

- `localize` – translate the text

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – the value to localize
  - `target` – the locale name (ru, en, etc.)
  - `extra` – the path to localization files

---

- `encode` – encode the data

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – the value to encode
  - `target` – the algorithm to use (base32|base58|base64|hex|url)

---

- `decode` – decode the data

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – the value to decode
  - `target` – the algorithm to use

---

- `template` – process the template

  params interpretation:

  - `name` – the context key for the result value to be saved with
  - `data` – the variable name with the template to process

---

- `pipe` – starts a thread with a channel of _interface{}_ to process stream data sequentially

  params interpretation:

  - `name` – the name of the channel to create
  - `target` – the name of the predefined scenario to run on each portion of data
  - `data` – the data to be sent to the channel (if set and the pipe named by `target` exists)
    Every time the data comes to the channel the process stores it into the context with the name pattern <pipe_name>\_item and runs `target` scenario.

---

- `websocket` – connects to a websocket for reading and writing

  params interpretation:

  - `name` – the name of the processor to create (will be stored)
  - `target` – the url
  - `data` – the name of the pipe to proccess the input
    A pipe named by <name>\_pipe is created for sending the data

---

- `while` – repeat executing the subscenario while the condition is true

  params interpretation:

  - `name` – formal
  - `target` – the name of the subscenario
  - `data` – the condition to continue the repeatition

---

- `set_map_value` – set a value by key in a map

  params interpretation:

  - `name` – the name of the map
  - `target` – the key
  - `data` – the value

---

- `inject` – add a scenario to the global storage

  params interpretation:

  - `name` – formal
  - `data` – the string representation of the scenario config
  - `extra` – the format string: yaml (default), json or other supported by viper lib

---

- `browse` – emulate a browser

  params interpretation:

  - `name` – the name to save the context for later using
  - `data` – additional parameters
  - `target` – the URL
    Additionally, two extra scenarios are searched: <name>\_before and <name>\_after to run in according stage.

---

- `wait_visible` – wait until the page element is visible

  params interpretation:

  - `name` – formal
  - `target` – the CSS selector of the desired element
  - `extra` – the context key containing the browser context

---

- `screenshot_viewport` – make a screenshot of the page

  params interpretation:

  - `name` – formal
  - `target` – file path to store the screenshot
  - `extra` – the context key containing the browser context

---

- `page_get_text` – get the text of the selected element

  params interpretation:

  - `name` – formal
  - `target` – the CSS selector of the desired element
  - `extra` – the context key containing the browser context

---

- `form_get_value` – get the value of the form element

  params interpretation:

  - `name` – formal
  - `target` – the CSS selector of the desired element
  - `extra` – the context key containing the browser context

---

- `form_set_value` – set the value to the form element

  params interpretation:

  - `name` – formal
  - `target` – the CSS selector of the desired element
  - `data` – the value
  - `extra` – the context key containing the browser context

---

- `set_cookie` – set a cookie

  params interpretation:

  - `name` – formal
  - `data` – json of cookie data: {"name": "", "value": "", "domain": "", "path": "", "secure": "", "http_only": ""}
  - `extra` – the context key containing the browser context

---

- `click` – emulate a click on an element

  params interpretation:

  - `name` – formal
  - `target` – the CSS selector of the desired element
  - `extra` – the context key containing the browser context

---

- `dom_nodes` – get a list of DOM nodes

  params interpretation:

  - `name` – the name to save the nodes
  - `target` – the CSS selector of the desired elements
  - `extra` – the context key containing the browser context

---

- `compare_images` – compare two images

  params interpretation:

  - `name` – the name to save the comparison result: `true` - if similar, `false` - if distinct
  - `target` – a path to the first image
  - `data` – a path to the second image

---

- `pg_listen` – listen for PG event

  params interpretation:

  - `name` – the name of subscenario to run on data and the name of the channel to listen
  - `target` – the DSN string

---

- `glob` – find files by a pattern

  params interpretation:

  - `name` – the name to save the value (Go array)
  - `target` – the pattern

---

- `calc` – calculate a math expression

  params interpretation:

  - `name` – the name to save the result value
  - `data` – the expression

---

- `html_query` – xpath on the HTML document

  params interpretation:

  - `name` – the name to save the result value
  - `data` – the HTML data
  - `target` – the path

---

- `parse` – parse structured data into the Golang map

  params interpretation:

  - `name` – the name to save the result value
  - `target` – the source data file path
  - `data` – the source data (preferred if both are present)
  - `extra` – format, one of: json, toml, yaml, yml, properties, props, prop, hcl, dotenv, env, ini **_(mandatory)_**

---

- `tcp` – establish a client TCP-connection

  params interpretation:

  - `name` – the name to save the result value (if any)
  - `data` – the pipe name to send data to
  - `target` – <host>:<port>

---

- `udp` – send UDP packets

  params interpretation:

  - `name` – the name to save the result value (if any)
  - `data` – the pipe name to send data to
  - `target` – <host>:<port>

---

- `poll` – read a file continuously

  params interpretation:

  - `name` – the name to save the result value ?
  - `data` – the filename to poll on
  - `target` – the scenario name to run on updates

---

- `amqp_consume` – read messages by AMQP

  params interpretation:

  - `name` – the name to save the connection
  - `target` – an AMQP config
  - `data` – the scenario name to run on new messages
  <details>
    <summary>Every message is an object of type amqp.Delivery:</summary>

  ```
      Headers Table // Application or header exchange table
      // Properties
      ContentType     string    // MIME content type
      ContentEncoding string    // MIME content encoding
      DeliveryMode    uint8     // queue implementation use - non-persistent (1) or persistent (2)
      Priority        uint8     // queue implementation use - 0 to 9
      CorrelationId   string    // application use - correlation identifier
      ReplyTo         string    // application use - address to reply to (ex: RPC)
      Expiration      string    // implementation use - message expiration spec
      MessageId       string    // application use - message identifier
      Timestamp       time.Time // application use - message timestamp
      Type            string    // application use - message type name
      UserId          string    // application use - creating user - should be authenticated user
      AppId           string    // application use - creating application id
      // Valid only with Channel.Consume
      ConsumerTag string
      MessageCount uint32
      DeliveryTag uint64
      Redelivered bool
      Exchange    string // basic.publish exchange
      RoutingKey  string // basic.publish routing key
      Body []byte
  ```

  </details>

---

- `amqp_publish` – publish a message by AMQP

  params interpretation:

  - `name` – the name to save the connection
  - `target` – an AMQP config
  - `data` – data to send

- `parse_rss` – parse the given data as RSS feed (rss, atom or json)

  params interpretation:

  - `name` – the name to save the result of inner type
  - `data` – a data string to parse

- `validate_json` – validate JSON by schema

  params interpretation:

  - `name` – the name to save the result
  - `data` – data JSON string to validate
  - `target` – schema JSON string

- `qr` – generate or read QR

  params interpretation:

  - `name` – the name to save the result
  - `data` – data to encode
  - `target` – file to:
    - if `data` is set - encode result path
    - if `data` is not set - decode data path

- `email` – send an email

  params interpretation:

  - `name` – the name to save the result
  - `target` – a json with connection data
  - `data` – message body
  - `extra` – a json with headers
    example:

  ```
  - type: email
    name: email
    target: >-
      {"host": "mail.host.com",
      "port": 587,
      "user": "name@host.com",
      "name": "Sender Name",
      "password": "password"}
    data: >-
      {{.body}}
    extra: >-
      {
      "From": "name@host.com",
      "To": "to@host.com",
      "Name": "Recipient Name",
      "Subject": "Subject",
      "Content-Type": "text/html"
      }
  ```

- `input` – interactively read the user's input

  params interpretation:

  - `name` – the name to save the result
  - `data` – a welcome message
  - `extra` – type restriction

## Builtin (sprig lib) functions available in templates

<details>
  <summary>A long list</summary>

```
  date
  date_in_zone
  date_modify
  now
  htmlDate
  htmlDateInZone
  dateInZone
  dateModify
  randAlphaNum
  randAlpha
  randAscii
  randNumeric
  randBytes
  uuidv4
  env
  expandenv
  getHostByName
  hello
  ago
  date
  date_in_zone
  date_modify
  dateInZone
  dateModify
  duration
  durationRound
  htmlDate
  htmlDateInZone
  must_date_modify
  mustDateModify
  mustToDate
  now
  toDate
  unixEpoch
  abbrev
  abbrevboth
  trunc
  trim
  upper
  lower
  title
  untitle
  substr
  repeat
  trimAll
  trimSuffix
  trimPrefix
  nospace
  initials
  randAlphaNum
  randAlpha
  randAscii
  randNumeric
  swapcase
  shuffle
  snakecase
  camelcase
  kebabcase
  wrap
  wrapWith
  contains
  hasPrefix
  hasSuffix
  quote
  squote
  cat
  indent
  nindent
  replace
  plural
  sha1sum
  sha256sum
  adler32sum
  toString
  atoi
  int64
  int
  float64
  seq
  toDecimal
  split
  splitList
  splitn
  toStrings
  until
  untilStep
  add1
  add
  sub
  div
  mod
  mul
  randInt
  add1f
  addf
  subf
  divf
  mulf
  biggest
  max
  min
  maxf
  minf
  ceil
  floor
  round
  join
  sortAlpha
  default
  empty
  coalesce
  all
  any
  compact
  mustCompact
  fromJson
  toJson
  toPrettyJson
  toRawJson
  mustFromJson
  mustToJson
  mustToPrettyJson
  mustToRawJson
  ternary
  deepCopy
  mustDeepCopy
  typeOf
  typeIs
  typeIsLike
  kindOf
  kindIs
  deepEqual
  env
  expandenv
  getHostByName
  base
  dir
  clean
  ext
  isAbs
  osBase
  osClean
  osDir
  osExt
  osIsAbs
  b64enc
  b64dec
  b32enc
  b32dec
  tuple
  list
  dict
  get
  set
  unset
  hasKey
  pluck
  keys
  pick
  omit
  merge
  mergeOverwrite
  mustMerge
  mustMergeOverwrite
  values
  append
  push
  mustAppend
  mustPush
  prepend
  mustPrepend
  first
  mustFirst
  rest
  mustRest
  last
  mustLast
  initial
  mustInitial
  reverse
  mustReverse
  uniq
  mustUniq
  without
  mustWithout
  has
  mustHas
  slice
  mustSlice
  concat
  dig
  chunk
  mustChunk
  bcrypt
  htpasswd
  genPrivateKey
  derivePassword
  buildCustomCert
  genCA
  genCAWithKey
  genSelfSignedCert
  genSelfSignedCertWithKey
  genSignedCert
  genSignedCertWithKey
  encryptAES
  decryptAES
  randBytes
  uuidv4
  semver
  semverCompare
  fail
  regexMatch
  mustRegexMatch
  regexFindAll
  mustRegexFindAll
  regexFind
  mustRegexFind
  regexReplaceAll
  mustRegexReplaceAll
  regexReplaceAllLiteral
  mustRegexReplaceAllLiteral
  regexSplit
  mustRegexSplit
  regexQuoteMeta
  urlParse
  urlJoin
```

</details>
