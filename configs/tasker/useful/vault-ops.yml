executors:
- name: get_vault_values
  cycles: 1
  defaults:
    timeout: 10m
    on_error: report_error
  predefined:
    get_keys:
      - type: set
        name: address
        data: "{{index .get_keys_arguments 0}}"
      - type: set
        name: path
        data: "{{index .get_keys_arguments 1}}"
      - type: set
        name: token
        data: "{{index .get_keys_arguments 2}}"
      - type: http
        method: get
        name: response
        target: "{{.address}}/v1/{{.mount}}/metadata/{{.path}}?list=true"
        headers: '{"X-Vault-Token": "{{.token}}"}'
        skip_if: not (hasSuffix "/" .path)
      - type: json_get
        name: get_keys_return_value
        data: "{{.response}}"
        target: data.keys
    report_error:
      - type: print
        data: |
          got an error: {{.current_task_error}}
    set_secret_key:
      - type: set
        name: url
        data: "{{index .set_arguments 0}}"
      - type: set
        name: key
        data: "{{index .set_arguments 1}}"
      - type: set
        name: value
        data: "{{index .set_arguments 2}}"
      - type: set
        name: token
        data: "{{index .set_arguments 3}}"
      - type: http
        method: get
        name: raw_data
        target: "{{.url}}"
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: json_get
        name: secrets
        data: "{{.raw_data}}"
        target: data
      - type: json_set
        name: new_secret
        data: >-
          {{.secrets}}
        target: data.{{.key}}
        extra: value
      - type: http
        method: post
        name: result
        target: "{{.url}}"
        data: "{{.new_secret}}"
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: set
        name: set_return_value
        data: |
          --------
          URL:    {{.url}}
          Result: {{.result}}
          --------
    deep_set:
      - type: set
        name: address
        data: "{{index .deep_set_arguments 0}}"
      - type: set
        name: path
        data: "{{index .deep_set_arguments 1}}"
      - type: set
        name: key
        data: "{{index .deep_set_arguments 2}}"
      - type: set
        name: value
        data: "{{index .deep_set_arguments 3}}"
      - type: set
        name: token
        data: "{{index .deep_set_arguments 4}}"
      - type: intern
        name: keys
        data: >-
          {{get_keys .address .path .token}}
        skip_if: not (hasSuffix "/" .path)
      - type: set
        name: deep_set_return_value
        data: |
          {{- set_secret_key (printf "%s/v1/%s/data/%s" .address .mount .path) .key .value .token -}}
        skip_if: .keys
      - type: set
        name: deep_set_return_value
        data: |
          {{- range $key := .keys -}}
          {{- if hasSuffix "/" $key -}}
          {{- deep_set $.address (printf "%s%s" $.path $key) $.key $.value $.token -}}
          {{- else -}}
          {{- set_secret_key (printf "%s/v1/%s/data/%s%s" $.address $.mount $.path $key) $.key $.value $.token -}}
          {{- end -}}
          {{- end -}}
        skip_if: not .keys
    do_update:
      - type: set
        name: url
        data: "{{index .update_arguments 0}}"
      - type: set
        name: key
        data: "{{index .update_arguments 1}}"
      - type: set
        name: value
        data: "{{index .update_arguments 2}}"
      - type: set
        name: token
        data: "{{index .update_arguments 3}}"
      - type: set
        name: strict
        data: "{{index .update_arguments 4}}"
        extra: bool
      - type: http
        method: get
        name: raw_data
        target: "{{.url}}"
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: intern
        name: secret
        data: "{{.raw_data}}"
      - type: json_get
        name: secrets
        data: "{{.raw_data}}"
        target: data
      - type: json_set
        name: secrets
        data: >-
          {{.secrets}}
        target: data.{{.key}}
        extra: value
        skip_if: and .strict (not (hasKey .secret.data.data .key))
      - type: http
        method: post
        name: result
        target: "{{.url}}"
        data: "{{.secrets}}"
        headers: '{"X-Vault-Token": "{{.token}}"}'
        skip_if: not (hasKey .secret.data.data .key)
      - type: print
        name: update_return_value
        data: |
          --------
          URL:    {{.url}}
          Result: {{.result}}
          --------
    update:
      - type: run
        name: do_update
        target: do_update
        async: true
    deep_update:
      - type: set
        name: address
        data: "{{index .deep_update_arguments 0}}"
      - type: set
        name: path
        data: "{{index .deep_update_arguments 1}}"
      - type: set
        name: key
        data: "{{index .deep_update_arguments 2}}"
      - type: set
        name: value
        data: "{{index .deep_update_arguments 3}}"
      - type: set
        name: token
        data: "{{index .deep_update_arguments 4}}"
      - type: set
        name: strict
        data: "{{index .deep_update_arguments 5}}"
        extra: bool
      - type: intern
        name: keys
        data: >-
          {{get_keys .address .path .token}}
        skip_if: not (hasSuffix "/" .path)
      - type: set
        name: deep_update_return_value
        data: |
          {{- update (printf "%s/v1/applications/data/%s" .address .path) .key .value .token .strict -}}
        skip_if: .keys
      - type: set
        name: deep_update_return_value
        data: |
          {{- range $key := .keys -}}
          {{- if hasSuffix "/" $key -}}
          {{- deep_update $.address (printf "%s%s" $.path $key) $.key $.value $.token $.strict -}}
          {{- else -}}
          {{- update (printf "%s/v1/applications/data/%s%s" $.address $.path $key) $.key $.value $.token $.strict -}}
          {{- end -}}
          {{- end -}}
        skip_if: not .keys
      - type: set
        name: deep_update_return_value
        data: ok
    copy:
      - type: run
        name: do_copy
        target: do_copy
        async: true
    do_copy:
      - type: set
        name: url_from
        data: "{{index .copy_arguments 0}}"
      - type: set
        name: url_to
        data: "{{index .copy_arguments 1}}"
      - type: set
        name: token_from
        data: "{{index .copy_arguments 2}}"
      - type: set
        name: token_to
        data: "{{index .copy_arguments 3}}"
      - type: http
        method: get
        name: raw_data
        target: "{{.url_from}}"
        headers: '{"X-Vault-Token": "{{.token_from}}"}'
      - type: json_get
        name: data
        data: "{{.raw_data}}"
        target: data
      - type: http
        method: post
        name: result
        target: "{{.url_to}}"
        data: "{{.data}}"
        headers: '{"X-Vault-Token": "{{.token_to}}"}'
      - type: print
        data: |
          --------
          From:   {{.url_from}}
          To:     {{.url_to}}
          Result: {{.result}}
          --------
    deep_copy:
      - type: set
        name: address_from
        data: "{{index .deep_copy_arguments 0}}"
      - type: set
        name: address_to
        data: "{{index .deep_copy_arguments 1}}"
      - type: set
        name: path_from
        data: "{{index .deep_copy_arguments 2}}"
      - type: set
        name: path_to
        data: "{{index .deep_copy_arguments 3}}"
      - type: set
        name: token_from
        data: "{{index .deep_copy_arguments 4}}"
      - type: set
        name: token_to
        data: "{{index .deep_copy_arguments 5}}"
      - type: set
        name: mount
        data: "{{.mount_from}}"
      - type: intern
        name: keys
        data: >-
          {{get_keys .address_from .path_from .token_from}}
        skip_if: not (hasSuffix "/" .path_from)
      - type: set
        name: deep_copy_return_value
        data: |
          {{- copy (printf "%s/v1/%s/data/%s" .address_from .mount_from .path_from) (printf "%s/v1/%s/data/%s" .address_to .mount_to .path_to) .token_from .token_to -}}
        skip_if: .keys
      - type: set
        name: deep_copy_return_value
        data: |
          {{- range $key := .keys -}}
          {{- if hasSuffix "/" $key -}}
          {{- deep_copy $.address_from $.address_to (printf "%s%s" $.path_from $key) (printf "%s%s" $.path_to $key) $.token_from $.token_to -}}
          {{- else -}}
          {{- copy (printf "%s/v1/%s/data/%s%s" $.address_from $.mount_from $.path_from $key) (printf "%s/v1/%s/data/%s%s" $.address_to $.mount_to $.path_to $key) $.token_from $.token_to -}}
          {{- end -}}
          {{- end -}}
        skip_if: not .keys
    drop:
      - type: set
        name: url
        data: "{{index .drop_arguments 0}}"
      - type: set
        name: token
        data: "{{index .drop_arguments 1}}"
      - type: http
        method: delete
        name: result
        target: "{{.url}}"
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: set
        name: drop_return_value
        data: |
          --------
          URL:         {{.url}}
          Drop result: {{.result}} ({{.result_status_code}})
          --------
    deep_drop:
      - type: set
        name: address
        data: "{{index .deep_drop_arguments 0}}"
      - type: set
        name: path
        data: "{{index .deep_drop_arguments 1}}"
      - type: set
        name: token
        data: "{{index .deep_drop_arguments 2}}"
      - type: intern
        name: keys
        data: >-
          {{get_keys .address .path .token}}
        skip_if: not (hasSuffix "/" .path)
      - type: set
        name: deep_drop_return_value
        data: |
          {{- drop (printf "%s/v1/%s/data/%s" .address .mount .path) .token -}}
          {{- drop (printf "%s/v1/%s/metadata/%s" .address .mount .path) .token -}}
        skip_if: .keys
      - type: set
        name: deep_drop_return_value
        data: |
          {{- range $key := .keys -}}
          {{- if hasSuffix "/" $key -}}
          {{- deep_drop $.address (printf "%s%s" $.path $key) $.token -}}
          {{- else -}}
          {{- drop (printf "%s/v1/%s/data/%s%s" $.address $.mount $.path $key) $.token -}}
          {{- drop (printf "%s/v1/%s/metadata/%s%s" $.address $.mount $.path $key) $.token -}}
          {{- end -}}
          {{- end -}}
        skip_if: not .keys
    do_find:
      - type: set
        name: url
        data: "{{index .find_arguments 0}}"
      - type: set
        name: token
        data: "{{index .find_arguments 1}}"
      - type: set
        name: needle
        data: "{{index .find_arguments 2}}"
      - type: http
        method: get
        name: result
        target: "{{.url}}"
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: intern
        name: secret
        data: "{{.result}}"
      - type: print
        name: find_return_value
        data: |
          {{- $needle := .needle}}
          {{- $url := .url -}}
          {{- range $key, $value := .secret.data.data -}}
          {{- if mustRegexMatch $needle $value }}
          {{$url}}
          {{$key}}: {{$value}}
          {{ end -}}
          {{- end -}}
    find:
      - type: run
        name: do_find
        target: do_find
        async: true
    deep_find:
      - type: set
        name: address
        data: "{{index .deep_find_arguments 0}}"
      - type: set
        name: path
        data: "{{index .deep_find_arguments 1}}"
      - type: set
        name: token
        data: "{{index .deep_find_arguments 2}}"
      - type: set
        name: needle
        data: "{{index .deep_find_arguments 3}}"
      - type: intern
        name: keys
        data: >-
          {{get_keys .address .path .token}}
        skip_if: not (hasSuffix "/" .path)
      - type: set
        name: deep_find_return_value
        data: |
          {{- find (printf "%s/v1/%s/data/%s" .address .mount .path) .token .needle  -}}
        skip_if: .keys
      - type: set
        name: deep_find_return_value
        data: |
          {{- range $key := .keys -}}
          {{- if hasSuffix "/" $key -}}
          {{- deep_find $.address (printf "%s%s" $.path $key) $.token $.needle -}}
          {{- else -}}
          {{- find (printf "%s/v1/%s/data/%s%s" $.address $.mount $.path $key) $.token $.needle -}}
          {{- end -}}
          {{- end -}}
        skip_if: not .keys
    do_find_key:
      - type: set
        name: url
        data: "{{index .find_key_arguments 0}}"
      - type: set
        name: token
        data: "{{index .find_key_arguments 1}}"
      - type: set
        name: needle
        data: "{{index .find_key_arguments 2}}"
      - type: http
        method: get
        name: result
        target: "{{.url}}"
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: intern
        name: secret
        data: "{{.result}}"
      - type: print
        name: find_key_return_value
        data: |
          {{- $needle := .needle}}
          {{- range $key, $value := .secret.data.data -}}
          {{- if mustRegexMatch $needle $key }}
          {{$.url}}
          {{$key}}: {{$value}}
          {{ end -}}
          {{- end -}}
    find_key:
      - type: run
        name: do_find_key
        target: do_find_key
        async: true
    deep_find_key:
      - type: set
        name: address
        data: "{{index .deep_find_key_arguments 0}}"
      - type: set
        name: path
        data: "{{index .deep_find_key_arguments 1}}"
      - type: set
        name: token
        data: "{{index .deep_find_key_arguments 2}}"
      - type: set
        name: needle
        data: "{{index .deep_find_key_arguments 3}}"
      - type: intern
        name: keys
        data: >-
          {{get_keys .address .path .token}}
        skip_if: not (hasSuffix "/" .path)
      - type: set
        name: deep_find_key_return_value
        data: |
          {{- find_key (printf "%s/v1/%s/data/%s" .address .mount .path) .token .needle  -}}
        skip_if: .keys
      - type: set
        name: deep_find_key_return_value
        data: |
          {{- range $key := .keys -}}
          {{- if hasSuffix "/" $key -}}
          {{- deep_find_key $.address (printf "%s%s" $.path $key) $.token $.needle -}}
          {{- else -}}
          {{- find_key (printf "%s/v1/%s/data/%s%s" $.address $.mount $.path $key) $.token $.needle -}}
          {{- end -}}
          {{- end -}}
        skip_if: not .keys
    do_delete:
      - type: set
        name: url
        data: "{{index .do_delete_arguments 0}}"
      - type: set
        name: data
        data: "{{toJson (index .do_delete_arguments 1)}}"
      - type: set
        name: token
        data: "{{index .do_delete_arguments 2}}"
      - type: print
        data: |
          {{.data}}
      - type: http
        method: post
        name: do_delete_result
        target: "{{.url}}"
        data: "{{.data}}"
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: set
        name: do_delete_return_value
        data: |
          --------
          URL:    {{.url}}
          Result: {{.do_delete_result}}
          --------
    delete:
      - type: set
        name: url
        data: "{{index .delete_arguments 0}}"
      - type: set
        name: token
        data: "{{index .delete_arguments 1}}"
      - type: set
        name: key
        data: "{{index .delete_arguments 2}}"
      - type: set
        name: raw_data
        data: >-
          {{get_raw_data .url}}
      - type: intern
        name: secrets
        data: "{{.raw_data}}"
      - type: print
        data: |
          {{.url}}
          {{.raw_data}}
      - type: set
        name: delete_return_value
        data: |
          {{$raw_data := fromJson .raw_data -}}
          {{- $url := .url -}}
          {{- range $key, $value := .secrets.data.data -}}
          {{- if mustRegexMatch $.key $key }}
          {{$url}} : {{$key}}
          {{$d := unset $raw_data.data.data $key -}}
          {{ do_delete $url $raw_data.data $.token -}}
          {{break}}
          {{end -}}
          {{end -}}
    deep_delete:
      - type: set
        name: address
        data: "{{index .deep_delete_arguments 0}}"
      - type: set
        name: path
        data: "{{index .deep_delete_arguments 1}}"
      - type: set
        name: token
        data: "{{index .deep_delete_arguments 2}}"
      - type: set
        name: key
        data: "{{index .deep_delete_arguments 3}}"
      - type: set
        name: mounts
        data: >-
          {{get_keys .address .path .token}}
        skip_if: not (hasSuffix "/" .path)
      - type: set
        name: deep_delete_return_value
        data: |
          {{- delete (printf "%s/v1/%s/data/%s" .address .mount .path) .token .key  -}}
        skip_if: .keys
      - type: set
        name: deep_delete_return_value
        data: |
          {{- range $key := .keys -}}
          {{- if hasSuffix "/" $key -}}
          {{- deep_delete $.address (printf "%s%s" $.path $key) $.token $.key -}}
          {{- else -}}
          {{- delete (printf "%s/v1/%s/data/%s%s" $.address $.mount $.path $key) $.token $.key -}}
          {{- end -}}
          {{- end -}}
        skip_if: not .keys
    do_replace:
      - type: set
        name: url
        data: "{{index .do_replace_arguments 0}}"
      - type: set
        name: data
        data: "{{index .do_replace_arguments 1}}"
      - type: set
        name: token
        data: "{{index .do_replace_arguments 2}}"
      - type: http
        method: post
        name: do_replace_result
        target: "{{.url}}"
        data: "{{.data}}"
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: set
        name: do_replace_return_value
        data: |
          --------
          URL:    {{.url}}
          Result: {{.do_replace_result}}
          --------
    get_raw_data:
      - type: http
        method: get
        name: get_raw_data_return_value
        target: "{{index .get_raw_data_arguments 0}}"
        headers: '{"X-Vault-Token": "{{.token}}"}'
    replace:
      - type: set
        name: url
        data: "{{index .replace_arguments 0}}"
      - type: set
        name: token
        data: "{{index .replace_arguments 1}}"
      - type: set
        name: find
        data: "{{index .replace_arguments 2}}"
      - type: set
        name: replace
        data: "{{index .replace_arguments 3}}"
      - type: set
        name: raw_data
        data: >-
          {{get_raw_data .url}}
      - type: intern
        name: secrets
        data: "{{.raw_data}}"
      - type: set
        name: replace_return_value
        data: |
          {{$raw_data := fromJson .raw_data -}}
          {{- range $key, $value := .secrets.data.data -}}
          {{- if mustRegexMatch $.find $value }}
          {{$.url}}
          {{$key}}: {{$.find}} -> {{$.replace}}
          {{$new_value := mustRegexReplaceAll $.find $value $.replace -}}
          {{$d := set $raw_data.data.data $key $new_value -}}
          {{end -}}
          {{end -}}
          {{ do_replace $.url (toJson $raw_data.data) $.token }}
    deep_replace:
      - type: set
        name: address
        data: "{{index .deep_replace_arguments 0}}"
      - type: set
        name: path
        data: "{{index .deep_replace_arguments 1}}"
      - type: set
        name: token
        data: "{{index .deep_replace_arguments 2}}"
      - type: set
        name: find
        data: "{{index .deep_replace_arguments 3}}"
      - type: set
        name: replace
        data: "{{index .deep_replace_arguments 4}}"
      - type: intern
        name: keys
        data: >-
          {{get_keys .address .path .token}}
        skip_if: not (hasSuffix "/" .path)
      - type: set
        name: deep_replace_return_value
        data: |
          {{- replace (printf "%s/v1/applications/data/%s" .address .path) .token .find .replace  -}}
        skip_if: .keys
      - type: set
        name: deep_replace_return_value
        data: |
          {{- range $key := .keys -}}
          {{- if hasSuffix "/" $key -}}
          {{- deep_replace $.address (printf "%s%s" $.path $key) $.token $.find $.replace -}}
          {{- else -}}
          {{- replace (printf "%s/v1/applications/data/%s%s" $.address $.path $key) $.token $.find $.replace -}}
          {{- end -}}
          {{- end -}}
        skip_if: not .keys
    get_secret_key:
      - type: set
        name: url
        data: >-
          {{(printf "%s%s" .address (clean (printf "/v1/%s/data/%s" .mount .path)))}}
      - type: http
        method: get
        name: response
        target: "{{.url}}"
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: set
        name: get_secret_key_return_value
        data: |
          -------
          URL:   {{.url}}
          Value: {{.response}}
          -------
    add_user:
      - type: set
        name: password
        data: >-
          {{randAlphaNum 10}}
        skip_if: .password
      - type: http
        method: post
        name: personal
        target: "{{.address}}/v1/personal/data/{{.username}}/vault"
        data: >-
          {"data": {"name": "{{.username}}", "password": "{{.password}}"}}
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: http
        method: get
        name: response
        target: "{{.address}}/v1/sys/auth"
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: json_get
        name: accessor
        data: >-
          {{.response}}
        target: "userpass/.accessor"
      - type: http
        method: post
        name: response
        target: "{{.address}}/v1/identity/entity"
        data: >-
          {"name": "{{.username}}", "policies": {{toJson (splitList "," .policies)}}}
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: set
        name: id
        data: >-
          {{$response := fromJson .response -}}
          {{$response.data.id}}
      - type: http
        method: post
        name: response
        target: "{{.address}}/v1/auth/userpass/users/{{.username}}"
        data: >-
          {"password": "{{.password}}", "policies": {{toJson (splitList "," .policies)}}}
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: http
        method: post
        name: response
        target: "{{.address}}/v1/identity/entity-alias"
        data: >-
          {"name": "{{.username}}", "canonical_id": "{{.id}}", "mount_accessor": "{{.accessor}}"}
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: set
        name: add_user_return_value
        data: |
          -------
          login: {{.username}} / {{.password}}
          -------
    del_user:
      - type: http
        method: get
        name: list
        target: "{{.address}}/v1/identity/entity-alias/id/?list=true"
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: intern
        name: data
        data: |
          {{$list := (fromJson .list)}}
          {{range $key, $data := $list.data.key_info -}}
          {{if eq $.username $data.name}}{"username": "{{$.username}}", "alias": "{{$key}}", "entity": "{{$data.canonical_id}}"}{{break}}{{end -}}
          {{end -}}
      - type: http
        method: delete
        name: response
        target: "{{.address}}/v1/identity/entity-alias/id/{{.data.alias}}"
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: set
        name: del_user_return_value
        data: |
          alias delete: {{.response}}: {{.response_status_code}}
      - type: http
        method: delete
        name: response
        target: "{{.address}}/v1/auth/userpass/users/{{.data.username}}"
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: set
        name: del_user_return_value
        data: |
          {{.del_user_return_value}}
          user delete: {{.response}}: {{.response_status_code}}
      - type: http
        method: delete
        name: response
        target: "{{.address}}/v1/identity/entity/id/{{.data.entity}}"
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: set
        name: del_user_return_value
        data: |
          {{.del_user_return_value}}
          entity delete: {{.response}}: {{.response_status_code}}
    add_token:
      - type: http
        method: post
        name: response
        target: "{{.address}}/v1/auth/token/create"
        data: >-
          {
            "policies": {{toJson (splitList "," .policies)}},
            "ttl": "{{.ttl}}",
            "num_uses": {{.num_uses}}
          }
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: set
        name: add_token_return_value
        data: |
          {{toPrettyJson (fromJson .response)}}
    dump:
      - type: set
        name: url
        data: "{{index .dump_arguments 0}}"
      - type: set
        name: token
        data: "{{index .dump_arguments 1}}"
      - type: http
        method: get
        name: response
        target: "{{.url}}"
        headers: '{"X-Vault-Token": "{{.token}}"}'
      - type: set
        name: dump_return_value
        data: >-
          {{$response := fromJson .response -}}
          {{toJson $response.data.data}}
    deep_dump:
      - type: set
        name: address
        data: "{{index .deep_dump_arguments 0}}"
      - type: set
        name: path
        data: "{{index .deep_dump_arguments 1}}"
      - type: set
        name: token
        data: "{{index .deep_dump_arguments 2}}"
      - type: intern
        name: keys
        data: >-
          {{get_keys .address .path .token}}
        skip_if: not (hasSuffix "/" .path)
      - type: set
        name: deep_dump_return_value
        data: |
          {"{{.path}}": {{ dump (printf "%s/v1/%s/data/%s" .address .mount .path) .token  -}}}
        skip_if: .keys
      - type: set
        name: deep_dump_return_value
        data: |
          {{- range $key := .keys -}}
          {{if hasSuffix "/" $key -}}
          {{- deep_dump $.address (printf "%s%s" $.path $key) $.token }},
          {{else -}}
          "{{$.path}}{{$key}}": {{- dump (printf "%s/v1/%s/data/%s%s" $.address $.mount $.path $key) $.token}},
          {{end -}}
          {{end -}}
        skip_if: not .keys
      - type: set
        name: deep_dump_return_value
        data: |
          {{.deep_dump_return_value}}
  period: 10m
  scenario:
    - type: set
      name: custom_result
      data: |
        {{ deep_copy .address_from .address_to .path_from .path_to .token_from .token_to}}
      skip_if: >-
        or (not .action) (ne .action "copy")
        (not .address_from) (not .address_to)
        (not .path_from) (not .path_to)
        (not .token_from) (not .token_to)
    - type: set
      name: result
      data: |
        {{ deep_drop .address .init_path .token}}
      skip_if: or (not .action) (ne .action "drop")
    - type: set
      name: custom_result
      data: |
        {{ deep_find .address .init_path .token .needle }}
      skip_if: >-
        or (not .action) (ne .action "find")
    - type: set
      name: custom_result
      data: |
        {{ deep_find_key .address .init_path .token .needle }}
      skip_if: >-
        or (not .action) (ne .action "find_key")
    - type: set
      name: result
      data: |
        {{ deep_replace .address .init_path .token .find .replace }}
      skip_if: >-
        or (not .action) (ne .action "replace")
        (not .address) (not .token)
        (not .find) (not .replace)
    - type: set
      name: result
      data: |
        {{ deep_delete .address .init_path .token .key }}
      skip_if: >-
        or (not .action) (ne .action "delete")
        (not .address) (not .token) (not .key)
    - type: set
      name: result
      data: |
        {{ deep_set .address .path .key .value .token }}
      skip_if: or (not .action) (ne .action "set_secret_key ")
    - type: set
      name: custom_result
      data: |
        {{ deep_update .address .init_path .key .value .token .strict }}
      skip_if: or (not .action) (ne .action "update")
    - type: set
      name: result
      data: |
        {{ get_secret_key .address .path .token }}
      skip_if: or (not .action) (ne .action "get_secret_key")
    - type: set
      name: result
      data: |
        {{ add_user .username .policy .address .token }}
      skip_if: or (not .action) (ne .action "add_user")
    - type: set
      name: result
      data: |
        {{ add_token }}
      skip_if: or (not .action) (ne .action "add_token")
    - type: set
      name: result
      data: |
        {{ del_user }}
      skip_if: or (not .action) (ne .action "del_user")
    - type: set
      name: result
      data: |
        {{ deep_dump .address .path .token }}
      skip_if: or (not .action) (ne .action "dump")
    - type: print
      data: |
        {{.result}}
      skip_if: not .result
    - type: print
      name: help
      data: |
        Usage:
          tasker -config vault-ops.yml -set action=<command> <params>
        command:
            name: get_secret_key
            description: get secrets
            params:
              - address
              - path
              - token
            name: set_secret_key
            description: set value by key in every secret on the path
            params:
              - address
              - init_path
              - key
              - value
              - token
            name: update
            description: update given keys in every secret on the path
            params:
              - address
              - init_path
              - key
              - value
              - token
              - strict
            name: copy
            description: copy secrets from one path and source to another
            params:
              - address_from
              - address_to
              - path_from
              - path_to
              - token_from
              - token_to
            name: find
            description: find secrets containing keys with given values
            params:
              - address
              - init_path
              - token
              - needle: text to search for in secrets' keys
            name: find_key
            description: find secrets containing keys with given name
            params:
              - address
              - init_path
              - token
              - needle: text to search for in secrets' keys
            name: drop
            description: delete a secret
            params:
              - address
              - init_path
              - token
            name: replace
            description: replace a part of the secret's key
            params:
              - address
              - init_path
              - token
              - find
              - replace
            name: delete
            description: delete a part of the secret's key
            params:
              - address
              - init_path
              - token
              - key
            name: add_user
            description: create a userpass entity
            params:
              - username
              - policies (comma separated list)
              - token
              - address
            name: del_user
            description: delete the userpass entity
            params:
              - username
              - token
              - address
            name: add_token
            description: create a token
            params:
              - policies (comma separated list)
              - ttl (e.g. 1h)
              - num_uses (0 means unlimited)
              - address
              - token
            name: dump
            description: dump a tree
            params:
              - address
              - token
              - path
      skip_if: >-
        and (.action) (or (eq .action "get_secret_key")
        (eq .action "set_secret_key") (eq .action "update")
        (eq .action "copy") (eq .action "find")
        (eq .action "drop") (eq .action "replace")
        (eq .action "delete") (eq .action "find_key")
        (eq .action "dump") (eq .action "restore")
        (eq .action "add_user") (eq .action "del_user")
        (eq .action "add_token"))
logger:
  level: warn
  # filename: vault.log
  # hvs.kZoFCpjG8svoT3AwFuZCnZQZ
  # WCpaJoYoOy1FoXMLvRtWv1fAt+GO9OHX8XvNxpU6Md4=