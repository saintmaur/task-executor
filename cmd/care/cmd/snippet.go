/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/saintmaur/task-executor/internal/executor"
)

type SnippetConfig struct {
	Prefix      string   `json:"prefix"`
	Body        []string `json:"body"`
	Description string   `json:"description"`
	Scope       string   `json:"scope"`
}

var snippetCmd = &cobra.Command{
	Use:   "snippet",
	Short: "Create configs schemas",
	Run: func(cmd *cobra.Command, args []string) {
		at := executor.GetActionTypes()
		result := make(map[string]SnippetConfig)
		for key, cfg := range at {
			fields := []string{
				fmt.Sprintf("- type: %s", key),
			}
			i := 1
			for key := range cfg.Fields {
				fields = append(fields, fmt.Sprintf("  %s: ${%d:%s}", key, i, key))
				i++
			}
			result[key] = SnippetConfig{
				Prefix:      key,
				Body:        fields,
				Description: fmt.Sprintf("Use a '%s' directive", key),
				Scope:       "yaml,jinja-yaml",
			}
		}
		b, err := json.Marshal(result)
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Printf("%s\n", b)
		}
	},
}

func init() {
	configCmd.AddCommand(snippetCmd)
}
