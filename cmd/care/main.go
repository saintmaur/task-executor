/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package main

import "gitlab.com/saintmaur/task-executor/cmd/care/cmd"

func main() {
	cmd.Execute()
}
