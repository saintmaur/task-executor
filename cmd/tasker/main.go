package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	httpex "gitlab.com/saintmaur/lib/http"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/lib/profiler"
	"gitlab.com/saintmaur/lib/stat"
	extUtils "gitlab.com/saintmaur/lib/utils"
	"gitlab.com/saintmaur/task-executor/cmd/tasker/config"
	"gitlab.com/saintmaur/task-executor/internal/executor"
)

var (
	version   string
	commit    string
	buildTime string
	appName   string
)

func main() {
	var configPath = flag.String("config", "", "the config path")
	var dryRun = flag.Bool("dry", false, "don't start the executor(s), just check the config")
	var prof = flag.Bool("profile", false, "enable profiling")
	var vars executor.VariablesConfig
	flag.Var(&vars, "set", "set new variables; format: name=value[:(bool|int|float|string)], default type is string")
	var versionFlag = flag.Bool("version", false, "don't start, just show the version")
	flag.Parse()
	if *versionFlag {
		extUtils.PrintVersion(appName, version, commit, buildTime)
		return
	}
	if !config.InitConfig(*configPath) {
		fmt.Println("Failed on initializing config. Exit")
		return
	}
	if *prof {
		for _, cfg := range config.GeneralConfig.Profile {
			prflr := new(profiler.Profiler)
			prflr.Start(cfg)
			defer prflr.Stop()
		}
	}
	defer logger.Stop()
	if !logger.InitLogger(config.GeneralConfig.Logger.Filename,
		config.GeneralConfig.Logger.Level,
		config.GeneralConfig.Logger.FormatJSON) {
		fmt.Println("Failed to init logger")
		return
	}
	logger.Info("Initialization complete")
	logger.Info("Starting the application")
	srv := httpex.New(&config.GeneralConfig.Stat)
	if srv == nil {
		logger.Info("Failed on creating a metrics server. Stopping")
		os.Exit(1)
	}
	var executors []*executor.Executor
	srv.AddHandler("/executor/", func(w http.ResponseWriter, r *http.Request) {
		handleHTTPAPI(w, r, executors)
	})
	srv.Run()
	statC := make(stat.Chan, 1000)
	defer close(statC)
	stat.Run(statC)
	config.GeneralConfig.Variables = append(config.GeneralConfig.Variables, vars...)
	for _, task := range config.GeneralConfig.Executors {
		if !task.Disabled {
			if c := executor.NewExecutor(
				task,
				config.GeneralConfig.Variables,
				config.GeneralConfig.Predefined,
				statC,
			); c != nil && !(*dryRun) {
				c.Start()
				executors = append(executors, c)
			}
		}
	}
	executorsCount := len(executors)
	if executorsCount == 0 {
		logger.Info("No executor has been started. Stopping.")
		stat.Stop()
		os.Exit(1)
	}
	logger.Infof("All configurable executors (%d) have been started.", executorsCount)
	sigs := make(chan os.Signal, executorsCount)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	for {
		select {
		case <-sigs:
			logger.Info("A signal has been received. Stopping.")
			start := time.Now()
			for _, c := range executors {
				c.Stop()
			}
			stat.Stop()
			srv.Stop()
			logger.Infof("Stopped for %s", time.Since(start))
			return
		case <-time.After(time.Second):
			stopped := 0
			for _, c := range executors {
				if c.Stopped {
					stopped++
				}
			}
			if stopped == executorsCount {
				return
			}
		}
	}
}

func handleHTTPAPI(w http.ResponseWriter, r *http.Request, executors []*executor.Executor) {
	name := r.URL.Query().Get("name")
	var ex *executor.Executor
	for _, executor := range executors {
		if executor.Name == name {
			ex = executor
			break
		}
	}
	if ex != nil {
		ctx := ex.Context()
		b, err := json.Marshal(ctx)
		if err != nil {
			w.Write([]byte(fmt.Sprintf("%s", err)))
		} else {
			switch r.URL.Path {
			case "/executor/set/":
				key := r.URL.Query().Get("key")
				if len(key) == 0 {
					w.Write([]byte("Need a key"))
					return
				}
				value := r.URL.Query().Get("value")
				if len(value) == 0 {
					w.Write([]byte("Need a value"))
					return
				}
				ex.SetContextValue(key, value)
			case "/executor/get/":
				key := r.URL.Query().Get("key")
				if len(key) == 0 {
					w.Write(b)
				} else {
					valueRaw, ok := ctx[key]
					if !ok {
						w.Write([]byte(fmt.Sprintf("The key '%s' is missing", key)))
					} else {
						value, err := json.Marshal(valueRaw)
						if err != nil {
							w.Write([]byte(fmt.Sprintf("%s", err)))
						}
						w.Write(value)
					}
				}
			}
		}
	} else {
		w.Write([]byte(fmt.Sprintf("Executor '%s' not found", name)))
	}
}
