# The purpose

Execute commands periodically

# Command line parameters

Here's the result of running the binary with `-help` key
```
Usage:
  -config string
        the config path
  -dry
        don't start the executor(s), just check the config
  -profile
        enable profiling
  -set value
        set new variables; format: name=value[:(bool|int|float|string)], default type is string
  -version
        don't start, just show the version
```

# Configuration

## General layout

```yaml
variables:
  - name: <string>
    value: <string>
predefined:
  see further
executors:
  - name: <string>
    period: <duration_string>
    skip_startup_run: <bool>
    before: <scenario>
    after: <scenario>
    cycles: <int>
    disabled: <bool>
    threads: <int>
    defaults:
      timeout: <string> (default timeout for task)
      on_timeout: <string> (default script for task)
      on_error: <string> (default script for task)
      stop_on_error: <string>
    variables:
      - name: <string>
        value: <string>
    predefined:
      <string>: <scenario>
    scenario: <array of tasks>
      - name: <string>
        type: <string>
        target: <string>
        data: <string>
        extra: <string>
        stop_on_error: <string>
        skip_if: <string>
        async: <string> -- applicable only to `run` task type
        on_error: <string>
        timeout: <string>
        on_timeout: <string>
        error_if: <string>
        error_message: <string>
logger:
    filename: <string> #if omitted log to stdout
    level: <debug|info|warn|error>
    json: bool
stat:
    listen: "[<host>]:<number>"
    probe_endpoint: /<string>
    tls:
      enabled: <bool>
      domain: <string>
      generate_certificate: <bool>
      cert_file: <string>
      key_file: <string>
profile:
  - type: (cpu|memory|mutex|block|trace|thread_create|goroutine)
    directory: <path>
```

Depending on its type the task may have different mandatory parameters

# Description

If the `type` of the `task` is not supported the `task` will be skipped.

If no `task` could be parsed the `executor` will not be started.

If no `executor` could be started the program will exit with an appropriate message.

Variables listed in `variables` section are set for each executor before their start.

Every `task`'s duration is recorded into the context with the key named by pattern: `<executor_name>_<task_name>_duration`.

Additionally for `http_*` task types the request duration and the result code are recorded into the context with the keys named by pattern: `<task_name>_duration` and `<task_name>_status_code` accordingly. Moreover, a map of response headers are recorded with the key `<task_name>_headers`.

Also there can be two more scenarios that will be run once `before` the start and `after` the end of the executor lifetime.

Each time when processing templates any of predefined scenario may be used. The arguments are stored as Golang array in `<scenario_name>_arguments` variable, and the return value **must be** stored with `<scenario_name>_return_value` key.
It should be noted that processing the arrays in template moves you from global context to local, so the better way to pass the result is using return value.

All task fields are assumed templatable, unless otherwise specified.

Using the CLI flag `--dry` prevents the app from running and may help in validating the configuration specified via `--config` parameter.

## Docker

The docker image is available on Docker Hub.
```
docker pull saintmaur/tasker:<tag>
```

NB: the `latest` tag is not provided