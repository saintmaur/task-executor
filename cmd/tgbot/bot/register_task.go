package bot

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/task-executor/cmd/tgbot/config"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/saintmaur/task-executor/internal/executor"
	"gitlab.com/saintmaur/task-executor/internal/utils"
)

func (b *Bot) register() {
	executor.RegisterTask(config.SendTextMessageKey, func(e *executor.Executor, task executor.Task) error {
		text, err := e.ProcessTemplate(task.Data)
		if err != nil {
			return fmt.Errorf("failed on getting the text for the message: %s", err)
		}
		target, _ := e.ProcessTemplate(task.Target)
		chatID, _ := utils.PrepareInt(e.GetContextValue(config.ChatIDKey))
		var cfg tgbotapi.MessageConfig
		if len(target) > 0 {
			cfg = tgbotapi.NewMessageToChannel(target, text)
		} else {
			cfg = tgbotapi.NewMessage(chatID, text)
		}
		parseMode, err := utils.PrepareString(e.GetContextValue(config.ParseModeKey))
		rm := b.prepareMarkup(e, task)
		if rm != nil {
			cfg.ReplyMarkup = rm
		}
		if err == nil {
			cfg.ParseMode = parseMode
		}
		cfg.DisableWebPagePreview = false
		b.send(e, cfg)
		return nil
	})
	executor.RegisterTask(config.SendAudioMessageKey, func(e *executor.Executor, task executor.Task) error {
		filepath, err := e.ProcessTemplate(task.Data)
		if err != nil {
			return err
		}
		chatID, err := utils.PrepareInt(e.GetContextValue(config.ChatIDKey))
		if err != nil {
			return fmt.Errorf("failed on preparing chatID: %s", err)
		}
		target, err := e.ProcessTemplate(task.Target)
		if len(target) > 0 {
			chatID, err = utils.PrepareInt(target)
		}
		if err != nil {
			return fmt.Errorf("failed on getting the chatID: %s", err)
		}
		cfg := tgbotapi.NewAudio(chatID, tgbotapi.FilePath(filepath))
		b.send(e, cfg)
		return nil
	})
	executor.RegisterTask(config.SendVideoMessageKey, func(e *executor.Executor, task executor.Task) error {
		filepath, err := e.ProcessTemplate(task.Data)
		if err != nil {
			return err
		}
		chatID, err := utils.PrepareInt(e.GetContextValue(config.ChatIDKey))
		if err != nil {
			return fmt.Errorf("failed on preparing chatID: %s", err)
		}
		target, err := e.ProcessTemplate(task.Target)
		if len(target) > 0 {
			chatID, err = utils.PrepareInt(target)
		}
		if err != nil {
			return fmt.Errorf("failed on getting the chatID: %s", err)
		}
		cfg := tgbotapi.NewVideo(chatID, tgbotapi.FilePath(filepath))
		b.send(e, cfg)
		return nil
	})
	executor.RegisterTask(config.SendPhotoMessageKey, func(e *executor.Executor, task executor.Task) error {
		filepath, err := e.ProcessTemplate(task.Data)
		if err != nil {
			return err
		}
		target, _ := e.ProcessTemplate(task.Target)
		caption, _ := e.ProcessTemplate(task.Extra)
		chatID, err := utils.PrepareInt(e.GetContextValue(config.ChatIDKey))
		logger.Warnf("Failed on getting the chatID: %s", err)
		var cfg tgbotapi.PhotoConfig
		if len(target) > 0 {
			cfg = tgbotapi.NewPhotoToChannel(target, tgbotapi.FilePath(filepath))
		} else {
			cfg = tgbotapi.NewPhoto(chatID, tgbotapi.FilePath(filepath))
		}
		parseMode, err := utils.PrepareString(e.GetContextValue(config.ParseModeKey))
		if err == nil {
			cfg.ParseMode = parseMode
		}
		cfg.Caption = caption
		rm := b.prepareMarkup(e, task)
		if rm != nil {
			cfg.ReplyMarkup = rm
		}
		b.send(e, cfg)
		return nil
	})
	executor.RegisterTask(config.SendDocumentMessageKey, func(e *executor.Executor, task executor.Task) error {
		filepath, err := e.ProcessTemplate(task.Data)
		if err != nil {
			return err
		}
		caption, _ := e.ProcessTemplate(task.Extra)
		chatID, chatIDErr := utils.PrepareInt(e.GetContextValue(config.ChatIDKey))
		target, _ := e.ProcessTemplate(task.Target)
		targetID, err := utils.PrepareInt(target)
		if err != nil {
			if chatIDErr != nil {
				return fmt.Errorf("failed to prepare the addressee chat ID: %s (target), %s (chat_id)", err, chatIDErr)
			} else {
				targetID = chatID
			}
		}
		cfg := tgbotapi.NewDocument(targetID, tgbotapi.FilePath(filepath))
		parseMode, err := utils.PrepareString(e.GetContextValue(config.ParseModeKey))
		if err == nil {
			cfg.ParseMode = parseMode
		}
		cfg.Caption = caption
		rm := b.prepareMarkup(e, task)
		if rm != nil {
			cfg.ReplyMarkup = rm
		}
		b.send(e, cfg)
		return nil
	})
	executor.RegisterTask(config.SendEditTextMessageKey, func(e *executor.Executor, task executor.Task) error {
		text, _ := e.ProcessTemplate(task.Data)
		chatID, _ := utils.PrepareInt(e.GetContextValue(config.ChatIDKey))
		messageIDStr, _ := e.ProcessTemplate(task.Target)
		messageID, _ := utils.PrepareInt(messageIDStr)
		if len(text) > 0 {
			cfg := tgbotapi.NewEditMessageText(chatID, int(messageID), text)
			parseMode, err := utils.PrepareString(e.GetContextValue(config.ParseModeKey))
			if err == nil {
				cfg.ParseMode = parseMode
			}
			b.send(e, cfg)
		}
		ikbd := new(tgbotapi.InlineKeyboardMarkup)
		mkp, _ := e.ProcessTemplate(task.Extra)
		err := json.Unmarshal([]byte(mkp), ikbd)
		if err == nil {
			cfg := tgbotapi.NewEditMessageReplyMarkup(chatID, int(messageID), *ikbd)
			b.send(e, cfg)
		}
		return nil
	})
	executor.RegisterTask(config.SendLocationMessageKey, func(e *executor.Executor, task executor.Task) error {
		text, _ := e.ProcessTemplate(task.Data)
		chatID, _ := utils.PrepareInt(e.GetContextValue(config.ChatIDKey))
		messageIDStr, _ := e.ProcessTemplate(task.Target)
		messageID, _ := utils.PrepareInt(messageIDStr)
		var latitude, longitude float64
		if len(text) > 0 {
			coords := make(map[string]string)
			err := json.Unmarshal([]byte(text), &coords)
			if err != nil {
				return err
			}
			if lat, ok := coords["lat"]; ok {
				latitude, err = utils.PrepareFloat(lat)
				if err != nil {
					logger.Warnf("%s", err)
				}
			}
			if long, ok := coords["long"]; ok {
				longitude, err = utils.PrepareFloat(long)
				if err != nil {
					logger.Warnf("%s", err)
				}
			}
			cfg := tgbotapi.NewLocation(chatID, latitude, longitude)
			b.send(e, cfg)
		}
		ikbd := new(tgbotapi.InlineKeyboardMarkup)
		mkp, _ := e.ProcessTemplate(task.Extra)
		err := json.Unmarshal([]byte(mkp), ikbd)
		if err == nil {
			cfg := tgbotapi.NewEditMessageReplyMarkup(chatID, int(messageID), *ikbd)
			b.send(e, cfg)
		}
		return nil
	})
	executor.RegisterTask(config.AnswerCallbackQueryKey, func(e *executor.Executor, task executor.Task) error {
		callbackQueryID, _ := e.ProcessTemplate(task.Target)
		text, _ := e.ProcessTemplate(task.Data)
		showAlert := task.Extra
		cfg := tgbotapi.NewCallback(callbackQueryID, text)
		cfg.ShowAlert = strings.ToLower(showAlert) == "true"
		b.send(e, cfg)
		return nil
	})
	executor.RegisterTask(config.GetFileURLKey, func(e *executor.Executor, task executor.Task) error {
		fileID, _ := e.ProcessTemplate(task.Data)
		fileURL, err := b.bot.GetFileDirectURL(fileID)
		if err != nil {
			return err
		}
		e.SetContextValue(task.Name, fileURL)
		return nil
	})
	executor.RegisterTask(config.DeleteMessageKey, func(e *executor.Executor, task executor.Task) error {
		chatID, err := utils.PrepareInt(e.GetContextValue(config.ChatIDKey))
		if err != nil {
			return err
		}
		messageIDStr, err := e.ProcessTemplate(task.Target)
		if err != nil {
			return err
		}
		messageID, err := utils.PrepareInt(messageIDStr)
		if err != nil {
			return err
		}
		cfg := tgbotapi.NewDeleteMessage(chatID, int(messageID))
		b.send(e, cfg)
		return nil
	})
	executor.RegisterTask(config.SendPollKey, func(e *executor.Executor, task executor.Task) error {
		data, err := e.ProcessTemplate(task.Data)
		if err != nil {
			return fmt.Errorf("failed on preparing the poll config: %s", err)
		}
		pollConfig := config.PollConfig{}
		json.Unmarshal([]byte(data), &pollConfig)
		if len(pollConfig.ChatID) == 0 {
			chatID, _ := utils.PrepareString(e.GetContextValue(config.ChatIDKey))
			pollConfig.ChatID = chatID
		}
		chatID, err := utils.PrepareInt(pollConfig.ChatID)
		if err != nil {
			return fmt.Errorf("failed on preparing the chatID: %s", err)
		}
		options := utils.SplitStringByFirstSymbol(pollConfig.Options)
		cfg := tgbotapi.NewPoll(chatID, pollConfig.Question, options...)
		cfg.AllowsMultipleAnswers = e.TrueCondition(pollConfig.AllowsMultipleAnswers)
		cfg.IsAnonymous = e.TrueCondition(pollConfig.IsAnonymous)
		b.send(e, cfg)
		return nil
	})
	executor.RegisterTask(config.SendStopPollKey, func(e *executor.Executor, task executor.Task) error {
		chatID, err := utils.PrepareInt(e.GetContextValue(config.ChatIDKey))
		if err != nil {
			return fmt.Errorf("failed on preparing the chat ID: %s", err)
		}
		messageIDStr, err := e.ProcessTemplate(task.Target)
		if err != nil {
			return fmt.Errorf("failed on preparing the message ID string: %s", err)
		}
		messageID, err := utils.PrepareInt(messageIDStr)
		if err != nil {
			return fmt.Errorf("failed on preparing the message ID: %s", err)
		}
		cfg := tgbotapi.NewStopPoll(chatID, int(messageID))
		b.send(e, cfg)
		return nil
	})
	executor.RegisterTask(config.SendInvoiceKey, func(e *executor.Executor, task executor.Task) error {
		data, err := e.ProcessTemplate(task.Data)
		if err != nil {
			return fmt.Errorf("failed on templating the invoice config: %s", err)
		}
		invoiceConfig := config.InvoiceConfig{}
		err = json.Unmarshal([]byte(data), &invoiceConfig)
		if err != nil {
			return fmt.Errorf("failed on preparing the invoice config: %s", err)
		}
		var chatID int64
		if len(invoiceConfig.ChatID) > 0 {
			chatID, err = utils.PrepareInt(invoiceConfig.ChatID)
			if err != nil {
				return fmt.Errorf("failed on preparing the chatID from config: %s", err)
			}
		} else {
			target, err := e.ProcessTemplate(task.Target)
			if err != nil {
				return fmt.Errorf("failed on templating the target: %s", err)
			}
			chatID, err = utils.PrepareInt(target)
			if err != nil {
				return fmt.Errorf("failed on preparing the chatID: %s", err)
			}
		}
		var labeledPrices []tgbotapi.LabeledPrice
		for _, price := range invoiceConfig.Prices {
			labeledPrices = append(labeledPrices, tgbotapi.LabeledPrice{Amount: price.Amount, Label: price.Label})
		}
		cfg := tgbotapi.NewInvoice(
			chatID, invoiceConfig.Title, invoiceConfig.Description,
			invoiceConfig.Payload, invoiceConfig.ProviderToken,
			invoiceConfig.StartParameter, invoiceConfig.Currency,
			labeledPrices)
		cfg.MaxTipAmount = invoiceConfig.MaxTipAmount
		cfg.SuggestedTipAmounts = invoiceConfig.SuggestedTipAmounts
		cfg.ProviderData = invoiceConfig.ProviderData
		cfg.PhotoURL = invoiceConfig.PhotoURL
		cfg.PhotoSize = invoiceConfig.PhotoSize
		cfg.PhotoWidth = invoiceConfig.PhotoWidth
		cfg.PhotoHeight = invoiceConfig.PhotoHeight
		cfg.NeedName = invoiceConfig.NeedName
		cfg.NeedPhoneNumber = invoiceConfig.NeedPhoneNumber
		cfg.NeedEmail = invoiceConfig.NeedEmail
		cfg.NeedShippingAddress = invoiceConfig.NeedShippingAddress
		cfg.SendPhoneNumberToProvider = invoiceConfig.SendPhoneNumberToProvider
		cfg.SendEmailToProvider = invoiceConfig.SendEmailToProvider
		cfg.IsFlexible = invoiceConfig.IsFlexible
		b.send(e, cfg)
		return nil
	})
	executor.RegisterTask(config.AnswerPrecheckoutQueryKey, func(e *executor.Executor, task executor.Task) error {
		data, err := e.ProcessTemplate(task.Data)
		if err != nil {
			return fmt.Errorf("failed on templating the precheckout config: %s", err)
		}
		preCheckoutConfig := config.PreCheckoutConfig{}
		err = json.Unmarshal([]byte(data), &preCheckoutConfig)
		if err != nil {
			return fmt.Errorf("failed on preparing the precheckout config: %s", err)
		}
		cfg := tgbotapi.PreCheckoutConfig{
			OK:                 preCheckoutConfig.OK,
			PreCheckoutQueryID: preCheckoutConfig.PreCheckoutQueryID,
			ErrorMessage:       preCheckoutConfig.ErrorMessage,
		}
		b.send(e, cfg)
		return nil
	})
	executor.RegisterTask(config.AnswerShippingQueryKey, func(e *executor.Executor, task executor.Task) error {
		data, err := e.ProcessTemplate(task.Data)
		if err != nil {
			return fmt.Errorf("failed on templating the shipping config: %s", err)
		}
		shippingConfig := config.ShippingConfig{}
		err = json.Unmarshal([]byte(data), &shippingConfig)
		if err != nil {
			return fmt.Errorf("failed on preparing the shipping config: %s", err)
		}
		var options []tgbotapi.ShippingOption
		for _, option := range shippingConfig.ShippingOptions {
			var labeledPrices []tgbotapi.LabeledPrice
			for _, price := range option.Prices {
				labeledPrices = append(labeledPrices, tgbotapi.LabeledPrice{Amount: price.Amount, Label: price.Label})
			}
			options = append(options, tgbotapi.ShippingOption{
				ID:     option.ID,
				Title:  option.Title,
				Prices: labeledPrices,
			})
		}
		cfg := tgbotapi.ShippingConfig{
			OK:              shippingConfig.OK,
			ShippingOptions: options,
			ShippingQueryID: shippingConfig.ShippingQueryID,
			ErrorMessage:    shippingConfig.ErrorMessage,
		}
		b.send(e, cfg)
		return nil
	})
	executor.RegisterTask(config.ChatActionKey, func(e *executor.Executor, task executor.Task) error {
		data, err := e.ProcessTemplate(task.Data)
		if err != nil {
			return fmt.Errorf("failed on templating the chat action: %s", err)
		}
		chatID, _ := utils.PrepareInt(e.GetContextValue(config.ChatIDKey))
		cfg := tgbotapi.NewChatAction(chatID, data)
		b.send(e, cfg)
		return nil
	})
}

func (b *Bot) prepareMarkup(e *executor.Executor, task executor.Task) interface{} {
	mkp, _ := e.ProcessTemplate(task.Extra)
	var rm interface{}
	if len(mkp) > 0 {
		var markup interface{}
		err := json.Unmarshal([]byte(mkp), &markup)
		if err == nil {
			rm = &markup
		} else {
			logger.Warnf("Failed on parsing the markup: %s", err)
		}
	}
	return rm
}
func (b *Bot) send(e *executor.Executor, message tgbotapi.Chattable) {
	resultChan := make(chan (int), 2)
	b.outgoingQueue <- &OutgoingMessage{
		c: resultChan,
		m: &message,
	}
	messageID := <-resultChan
	e.SetContextValue(config.MessageIDKey, messageID)
}
