package bot

import (
	"encoding/json"
	"fmt"
	"sync"
	"time"

	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/lib/stat"
	"gitlab.com/saintmaur/task-executor/cmd/tgbot/chat"
	"gitlab.com/saintmaur/task-executor/cmd/tgbot/config"
	"gitlab.com/saintmaur/task-executor/internal/executor"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

const (
	// MaxButtonsPerRow is what it names
	MaxButtonsPerRow = 5
)

type OutgoingMessage struct {
	c chan (int)
	m *tgbotapi.Chattable
}

// Bot wraps the API
type Bot struct {
	chats         map[int64]*chat.Chat
	bot           *tgbotapi.BotAPI
	wg            sync.WaitGroup
	incomingQueue chan *tgbotapi.Update
	outgoingQueue chan *OutgoingMessage
	throttleGap   time.Duration
	updateID      int
	stopChan      chan bool
	config        config.BotConfig
	controller    *executor.Executor
}

// New creates a new instance
func New(cfg config.BotConfig) *Bot {
	bAPI, err := tgbotapi.NewBotAPI(cfg.Token)
	if err != nil {
		logger.Errorf("%s", err)
		return nil
	}
	logger.Info(fmt.Sprintf("Authorized bot %s", bAPI.Self.UserName))
	bAPI.Debug = cfg.Debug
	dur, err := time.ParseDuration(cfg.ThrottleGap)
	if err != nil {
		dur = time.Second
	}
	b := Bot{
		bot:           bAPI,
		chats:         make(map[int64]*chat.Chat),
		stopChan:      make(chan bool),
		incomingQueue: make(chan *tgbotapi.Update, 1000),
		outgoingQueue: make(chan *OutgoingMessage, 1000),
		throttleGap:   dur,
	}
	_, ok := cfg.Commands[chat.DefaultCmd]
	if !ok {
		logger.Warnf("'%s' command hasn't been found. Too bad, exit.", chat.DefaultCmd)
		return nil
	}
	b.config = cfg
	b.register()
	return &b
}

func (b *Bot) processIncomingMessages() {
	for update := range b.incomingQueue {
		go b.dispatchMessage(update)
	}
}

func (b *Bot) processOutgongMessages() {
	logger.Debugf("Start waiting for messages")
	for message := range b.outgoingQueue {
		time.Sleep(b.throttleGap)
		sentMessage, err := b.bot.Send(*message.m)
		resultID := 0
		if err == nil {
			logger.Debugf("Successfully sent a message to the chat: %v", sentMessage.Chat.ID)
			resultID = sentMessage.MessageID
		} else {
			b, _ := json.Marshal(sentMessage)
			logger.Warnf("sending error: %s, %s", err, string(b))
		}
		message.c <- resultID
	}
}

// Stop saves contexts and stops recieving the updates
func (b *Bot) Stop() {
	b.controller.Stop()
	logger.Debug("Try to stop this bot")
	b.stopChan <- true
	b.wg.Wait()
}

// Start creates a bot API client and starts to listen for updates
func (b *Bot) Start(statChan stat.Chan) {
	b.controller = executor.NewExecutor(config.GeneralConfig.Bot.Controller, executor.VariablesConfig{}, map[string]executor.Scenario{}, statChan)
	if b.controller == nil {
		return
	}
	b.controller.Start()
	go b.processIncomingMessages()
	go b.processOutgongMessages()
	u := tgbotapi.NewUpdate(b.updateID)
	u.Timeout = 5
	updates := b.bot.GetUpdatesChan(u)
	b.wg.Add(1)
	go func() {
		for {
			select {
			case <-b.stopChan:
				b.bot.StopReceivingUpdates()
				logger.Info("Stop receiving updates")
				b.wg.Done()
				return
			case update := <-updates:
				if update.CallbackQuery != nil {
					update.Message = update.CallbackQuery.Message
				}
				b.enqueueIncomingMessage(&update)
			}
		}
	}()
}

func (b *Bot) enqueueIncomingMessage(update *tgbotapi.Update) {
	b.incomingQueue <- update
}

func (b *Bot) dispatchMessage(update *tgbotapi.Update) {
	chatID := b.getChatID(update)
	cht, ok := b.chats[chatID]
	if !ok {
		cht = chat.New(chatID, b.config)
		b.chats[chatID] = cht
		cht.Start()
	}
	cht.Process(update)
}

func (b *Bot) getChatID(update *tgbotapi.Update) int64 {
	var id int64
	if update.Message != nil {
		id = update.Message.Chat.ID
	}
	if update.EditedMessage != nil {
		id = update.EditedMessage.Chat.ID
	}
	if update.ChannelPost != nil {
		id = update.ChannelPost.Chat.ID
	}
	if update.EditedChannelPost != nil {
		id = update.ChannelPost.Chat.ID
	}
	if update.InlineQuery != nil {
		id = update.InlineQuery.From.ID
	}
	if update.ChosenInlineResult != nil {
		id = update.ChosenInlineResult.From.ID
	}
	if update.CallbackQuery != nil {
		id = update.CallbackQuery.Message.Chat.ID
	}
	if update.ShippingQuery != nil {
		id = update.ShippingQuery.From.ID
	}
	if update.PreCheckoutQuery != nil {
		id = update.PreCheckoutQuery.From.ID
	}
	if update.PollAnswer != nil {
		id = update.PollAnswer.User.ID
	}
	if update.MyChatMember != nil {
		id = update.MyChatMember.Chat.ID
	}
	if update.ChatMember != nil {
		id = update.ChatMember.Chat.ID
	}
	if update.ChatJoinRequest != nil {
		id = update.ChatMember.Chat.ID
	}
	return id
}
