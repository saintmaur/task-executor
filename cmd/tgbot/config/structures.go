package config

import (
	httpex "gitlab.com/saintmaur/lib/http"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/task-executor/internal/executor"
	statemachine "gitlab.com/saintmaur/task-executor/internal/state-machine"
)

// Keys for new tasks
const (
	SendTextMessageKey        = "send_message"
	SendEditTextMessageKey    = "send_edit"
	SendAudioMessageKey       = "send_audio"
	SendVideoMessageKey       = "send_video"
	SendPhotoMessageKey       = "send_photo"
	SendDocumentMessageKey    = "send_document"
	SendLocationMessageKey    = "send_location"
	AnswerCallbackQueryKey    = "answer_callback_query"
	GetFileURLKey             = "get_file_url"
	DeleteMessageKey          = "delete_message"
	SendPollKey               = "send_poll"
	SendStopPollKey           = "send_stop_poll"
	SendInvoiceKey            = "send_invoice"
	AnswerShippingQueryKey    = "answer_shipping_query"
	AnswerPrecheckoutQueryKey = "answer_precheckout_query"
	ChatActionKey             = "chat_action"

	MessageIDKey  = "message_id"
	ChatIDKey     = "chat_id"
	CallbackIDKey = "callback_id"
	StatusKey     = "status"
	UpdateKey     = "update"
	ParseModeKey  = "parse_mode"
)

// CommandConfig describes a command
type CommandConfig struct {
	States []statemachine.StateConfig `mapstructure:"states"`
}

type BotConfig struct {
	Debug          bool                     `mapstructure:"debug"`
	TimeoutSeconds int                      `mapstructure:"timeout_seconds"`
	Token          string                   `mapstructure:"token"`
	ThrottleGap    string                   `mapstructure:"throttle_gap"`
	Commands       map[string]CommandConfig `mapstructure:"commands"`
	Controller     executor.ExecutorConfig  `mapstructure:"controller"`
}
type LocaleConfig struct {
	Path      string   `mapstructure:"path"`
	Default   string   `mapstructure:"default"`
	Domain    string   `mapstructure:"domain"`
	Supported []string `mapstructure:"supported"`
}

type Config struct {
	Logger logger.LoggerConfig `mapstructure:"logger"`
	Bot    BotConfig           `mapstructure:"bot"`
	Stat   httpex.ServerConfig `mapstructure:"stat"`
}

type LabeledPriceConfig struct {
	Label  string `mapstructure:"label"`
	Amount int    `mapstructure:"amount"`
}

type PollConfig struct {
	ChatID                string `mapstructure:"chatID"`
	Question              string `mapstructure:"question"`
	Options               string `mapstructure:"options"`
	AllowsMultipleAnswers string `mapstructure:"allowsMultipleAnswers"`
	IsAnonymous           string `mapstructure:"isAnonymous"`
}

type InvoiceConfig struct {
	ChatID                    string               `mapstructure:"chatID"`
	Title                     string               `mapstructure:"title"`
	Description               string               `mapstructure:"description"`
	Payload                   string               `mapstructure:"payload"`
	ProviderToken             string               `mapstructure:"providerToken"`
	StartParameter            string               `mapstructure:"startParameter"`
	Currency                  string               `mapstructure:"currency"`
	Prices                    []LabeledPriceConfig `mapstructure:"prices"`
	SuggestedTipAmounts       []int                `mapstructure:"suggestedTipAmounts"`
	MaxTipAmount              int                  `mapstructure:"maxTipAmount"`
	ProviderData              string               `mapstructure:"providerData"`
	PhotoURL                  string               `mapstructure:"photoURL"`
	PhotoSize                 int                  `mapstructure:"photoSize"`
	PhotoWidth                int                  `mapstructure:"photoWidth"`
	PhotoHeight               int                  `mapstructure:"photoHeight"`
	NeedName                  bool                 `mapstructure:"needName"`
	NeedPhoneNumber           bool                 `mapstructure:"needPhoneNumber"`
	NeedEmail                 bool                 `mapstructure:"needEmail"`
	NeedShippingAddress       bool                 `mapstructure:"needShippingAddress"`
	SendPhoneNumberToProvider bool                 `mapstructure:"sendPhoneToProvider"`
	SendEmailToProvider       bool                 `mapstructure:"sendEmailToProvider"`
	IsFlexible                bool                 `mapstructure:"isFlexible"`
}

type PreCheckoutConfig struct {
	PreCheckoutQueryID string `mapstructure:"preCheckoutQueryID"`
	OK                 bool   `mapstructure:"ok"`
	ErrorMessage       string `mapstructure:"error"`
}

type ShippingOption struct {
	ID     string               `json:"id"`
	Title  string               `json:"title"`
	Prices []LabeledPriceConfig `json:"prices"`
}

type ShippingConfig struct {
	ShippingQueryID string           `mapstructure:"shippingQueryID"`
	OK              bool             `mapstructure:"ok"`
	ShippingOptions []ShippingOption `mapstructure:"shippingOptions"`
	ErrorMessage    string           `mapstructure:"error"`
}
