package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	httpex "gitlab.com/saintmaur/lib/http"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/lib/stat"
	extUtils "gitlab.com/saintmaur/lib/utils"
	"gitlab.com/saintmaur/task-executor/cmd/tgbot/bot"
	"gitlab.com/saintmaur/task-executor/cmd/tgbot/config"

	_ "golang.org/x/text/message/catalog"
)

var (
	version   string
	commit    string
	buildTime string
	appName   string
)

func main() {
	remote := false
	configPath := flag.String("config", "", "config path")
	if len(*configPath) == 0 {
		*configPath = os.Getenv("BOT_CONFIG_PATH")
	}
	var configServerAddress = flag.String("config-server", "", "config server address")
	if len(*configServerAddress) > 0 {
		remote = true
	}
	var versionFlag = flag.Bool("version", false, "don't start, just show the version")
	flag.Parse()
	if *versionFlag {
		extUtils.PrintVersion(appName, version, commit, buildTime)
		return
	}
	if !config.InitConfig(*configPath, remote) {
		return
	}
	defer logger.Stop()
	if !logger.InitLogger(config.GeneralConfig.Logger.Filename,
		config.GeneralConfig.Logger.Level,
		config.GeneralConfig.Logger.FormatJSON) {
		fmt.Println("Failed to init logger")
		return
	}
	logger.Info("Initialization complete")
	logger.Info("Starting the application")
	srv := httpex.New(&config.GeneralConfig.Stat)
	if srv == nil {
		logger.Info("Failed on creating a server. Stopping")
		os.Exit(1)
	}
	srv.Run()
	statC := make(stat.Chan, 1000)
	defer close(statC)
	stat.Run(statC)
	bot := bot.New(config.GeneralConfig.Bot)
	if bot == nil {
		fmt.Println("Failed to create the bot. See the log file for details.")
		return
	}
	// TODO: dry run
	bot.Start(statC)
	logger.Info("Started successfully")
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	sig := <-sigs
	logger.Infof("A signal has been received: %s. Stopping.", sig)
	bot.Stop()
	stat.Stop()
	srv.Stop()
	logger.Info("The bot has been stopped")
}
