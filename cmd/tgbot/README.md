# Purpose
Create a Telegram bot simply.

# Configuration
## General layout
```yaml
---
logger:
  filename: 'debug.log'
  level: debug
  json: true
bot:
  debug: false
  timeout_seconds: 60
  throttle_gap: 10ms
  token: <token>
  controller:
    period: 1h
    name: controller
    predefined:
      init:
        - type: set
          name: base_path
          data: /Users/seymour/repos/task-executor/configs/tgbot/culture_club_quests
        - type: set
          name: command
          data: "{{.update.Message.Text}}"
        - type: calc
          name: current_page
          data: 0
        - type: calc
          name: records_per_page
          data: 1
    scenario:
      - type: set
        name: status
        data: ready
      - type: unset
        name: update
  commands:
    default:
      states:
        - name: init
          scenario: init
          initial: true
          targets:
            dispatch: eq .status "ready"
        - name: search
          scenario: search
        - name: dispatch
          scenario: dispatch
          targets:
            process_answer: and .update (eq .status "waiting") (eq .action "/start")
            search: and .update (eq .status "waiting") (eq .action "/find")
        - name: process_answer
          scenario: process_answer
          rerun: true
stat:
  probe_endpoint: "/probe"
  listen: :8080
  ```