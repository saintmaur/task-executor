package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	httpex "gitlab.com/saintmaur/lib/http"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/lib/profiler"
	"gitlab.com/saintmaur/lib/stat"
	"gitlab.com/saintmaur/lib/utils"
	"gitlab.com/saintmaur/task-executor/cmd/simulator/config"
	"gitlab.com/saintmaur/task-executor/internal/executor"
)

var (
	version   string
	commit    string
	buildTime string
	appName   string
)

func main() {
	var configPath = flag.String("config", "", "the config path")
	var dryRun = flag.Bool("dry", false, "don't start the executor(s), just check the config")
	var prof = flag.Bool("profile", false, "enable profiling")
	var vars executor.VariablesConfig
	flag.Var(&vars, "set", "set new variables; format: name=value[:(bool|int|float|string)], default type is string")
	var versionFlag = flag.Bool("version", false, "don't start, just show the version")
	flag.Parse()
	if *versionFlag {
		utils.PrintVersion(appName, version, commit, buildTime)
		return
	}
	if !config.InitConfig(*configPath) {
		fmt.Println("Exit")
		return
	}
	if *prof {
		for _, cfg := range config.GeneralConfig.Profile {
			prflr := new(profiler.Profiler)
			prflr.Start(cfg)
			defer prflr.Stop()
		}
	}
	defer logger.Stop()
	if !logger.InitLogger(config.GeneralConfig.Logger.Filename,
		config.GeneralConfig.Logger.Level,
		config.GeneralConfig.Logger.FormatJSON) {
		fmt.Println("Failed to init logger")
		return
	}
	logger.Info("Initialization complete")
	logger.Info("Starting the application")
	srv := httpex.New(&config.GeneralConfig.Stat)
	if srv == nil {
		logger.Info("Failed on creating a metrics server. Stopping")
		os.Exit(1)
	}
	srv.Run()
	statC := make(stat.Chan, 1000)
	defer close(statC)
	stat.Run(statC)
	config.GeneralConfig.Variables = append(config.GeneralConfig.Variables, vars...)
	if config.GeneralConfig.Predefined == nil {
		config.GeneralConfig.Predefined = make(map[string]executor.Scenario)
	}
	for _, object := range config.GeneralConfig.Objects {
		state := make(map[string]interface{})
		for _, value := range object.State {
			state[value.Name] = value.Value
		}
		b, err := json.Marshal(state)
		if err != nil {
			logger.Warnf("Failed on parsing the state variables")
		}
		config.GeneralConfig.Executor.Before = append(config.GeneralConfig.Executor.Before,
			executor.Task{Type: "intern", Name: object.Name, Data: string(b)})
		for _, event := range object.Events {
			_, has := config.GeneralConfig.Predefined[event.Name]
			if has {
				stat.Stop()
				srv.Stop()
				logger.Fatalf("Found an event with non-unique name ('%s'). Exit.", event.Name)
				return
			}
			config.GeneralConfig.Predefined[event.Name] = event.Scenario
			config.GeneralConfig.Executor.Before = append(config.GeneralConfig.Executor.Before,
				executor.Task{Type: "cron", Name: fmt.Sprintf("%s_cron", event.Name), Data: event.Period, Target: event.Name},
			)
		}
	}
	e := executor.NewExecutor(
		config.GeneralConfig.Executor,
		config.GeneralConfig.Variables,
		config.GeneralConfig.Predefined,
		statC,
	)
	if e != nil && !(*dryRun) {
		e.Start()
	} else {
		logger.Info("No executor has been started. Stopping.")
		stat.Stop()
		os.Exit(1)
	}
	logger.Infof("Control executor has been started")
	sigs := make(chan os.Signal, 2)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	<-sigs
	logger.Info("A signal has been received. Stopping.")
	start := time.Now()
	e.Stop()
	stat.Stop()
	srv.Stop()
	logger.Infof("Stopped for %s", time.Since(start))
}
