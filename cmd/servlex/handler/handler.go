package handler

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"regexp"
	"sync"
	"sync/atomic"
	"time"

	"github.com/thanhpk/randstr"
	httpex "gitlab.com/saintmaur/lib/http"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/lib/stat"
	"gitlab.com/saintmaur/task-executor/cmd/servlex/config"
	executor "gitlab.com/saintmaur/task-executor/internal/executor"
	executorUtils "gitlab.com/saintmaur/task-executor/internal/utils"
)

const (
	bodyKey      = "body"
	paramsKey    = "params"
	paramsRawKey = "params_raw"
	pathKey      = "path"
	methodKey    = "method"
	headersKey   = "headers"
	hostKey      = "host"
	schemeKey    = "scheme"
	counterKey   = "counter"

	GlobalExecutorKey    = "global_executor"
	SetGlobalValueKey    = "set_global_value"
	GetGlobalValueKey    = "get_global_value"
	SetGlobalMapValueKey = "set_global_map_value"
	GetGlobalMapValueKey = "get_global_map_value"
)

// Handler has a quiet representative name
type Handler struct {
	stopChan chan struct{}
	config   config.Handler
	wg       sync.WaitGroup
	executor *executor.Executor
	counter  atomic.Int64
}

// New creates new handler
func New(cfg config.Handler,
	statChan stat.Chan,
	predefined map[string]executor.Scenario,
	variables executor.VariablesConfig,
	srv *httpex.Server,
) *Handler {
	var hndlr Handler
	hndlr.stopChan = make(chan struct{})
	hndlr.config = cfg
	eCfg := executor.ExecutorConfig{
		Name:       cfg.Executor.Name,
		Period:     cfg.Executor.Period,
		Defaults:   cfg.Executor.Defaults,
		Scenario:   cfg.Executor.Scenario,
		Predefined: cfg.Executor.Predefined,
		Variables:  cfg.Executor.Variables,
		Before:     cfg.Executor.Before,
		After:      cfg.Executor.After,
	}
	if eCfg.Predefined == nil {
		eCfg.Predefined = make(map[string]executor.Scenario)
	}
	pattern := `[\\/]{2,}`
	regex, err := regexp.Compile(pattern)
	if err != nil {
		logger.Errorf("Failed to compile the regexp (%s): %s", pattern, err)
		return nil
	}
	for subPath, e := range cfg.Endpoints {
		if len(e.Actions) == 0 {
			e.Actions = defaultScenario()
		}
		endpoint := fmt.Sprintf("/%s/%s", cfg.Executor.Name, subPath)
		endpoint = regex.ReplaceAllString(endpoint, "/")
		eCfg.Predefined[endpoint] = e.Actions
	}
	registerTasks()
	ex := executor.NewExecutor(eCfg, variables, predefined, statChan)
	if ex == nil {
		return nil
	}
	hndlr.executor = ex
	for subPath, data := range cfg.Endpoints {
		endpoint := fmt.Sprintf("/%s/%s", cfg.Executor.Name, subPath)
		endpoint = regex.ReplaceAllString(endpoint, "/")
		responseMessageTemplate := data.Response.Text
		responseCodeTemplate := data.Response.Code
		headers := data.Response.Headers
		scenario := eCfg.Predefined[endpoint]
		srv.AddHandler(endpoint,
			func(w http.ResponseWriter, r *http.Request) {
				hndlr.counter.Add(1)
				go hndlr.stat()
				cfg := eCfg
				cfg.Name = randstr.String(10)
				logger.WithField("executor", cfg.Name).Infof("Create an executor for the '%s' endpoint", endpoint)
				logger.Infof("Process host: '%s'", r.Host)
				ex := executor.NewExecutor(cfg, variables, predefined, statChan)
				if ex == nil {
					logger.Error("Failed to create an executor")
					w.WriteHeader(500)
					_, err := w.Write([]byte("Failure"))
					if err != nil {
						logger.Warnf("Failed to write the response: %s", err)
					}
					return
				}
				ex.SetParent(hndlr.executor)
				logger.WithField("executor", cfg.Name).Infof("Start processing the '%s' endpoint", endpoint)
				start := time.Now()
				body, err := io.ReadAll(r.Body)
				if err != nil {
					logger.Warnf("Failed to read the body: %s", err)
				}
				params := processQueryParams(r)
				ex.SetContextValue(hostKey, r.Host)
				ex.SetContextValue(schemeKey, r.URL.Scheme)
				ex.SetContextValue(methodKey, r.Method)
				ex.SetContextValue(headersKey, r.Header)
				ex.SetContextValue(pathKey, r.URL.Path)
				ex.SetContextValue(bodyKey, string(body))
				ex.SetContextValue(paramsKey, params)
				ex.SetContextValue(paramsRawKey, r.URL.RawQuery)
				ex.SetContextValue(counterKey, hndlr.counter.Load())
				ex.SetContextValue(GlobalExecutorKey, hndlr.executor)
				ex.RunScenario(scenario, fmt.Sprintf("%d", time.Now().UnixNano()))
				resultMessage, err := ex.ProcessTemplate(responseMessageTemplate)
				if err != nil {
					logger.WithField("executor", cfg.Name).Warnf("Failed to process the result message template: %s", err)
				}
				parseAndAddHeaders(&w, headers, ex)
				responseCode, err := ex.ProcessTemplate(responseCodeTemplate)
				if err != nil {
					logger.WithField("executor", cfg.Name).Warnf("Failed to process the result code template: %s", err)
				}
				code, err := executorUtils.PrepareInt(responseCode)
				if err != nil {
					logger.WithField("executor", cfg.Name).Warnf("Failed to prepare the response code int: %s", err)
					code = 500
				}
				w.WriteHeader(int(code))
				_, err = w.Write([]byte(resultMessage))
				if err != nil {
					logger.WithField("executor", cfg.Name).Warnf("Failed to write the response: %s", err)
				}
				logger.WithField("elapsed", time.Since(start).String()).WithField("executor", cfg.Name).Infof("Finish processing the '%s' endpoint", endpoint)
			})
	}
	ex.SetContextValue(GlobalExecutorKey, ex)
	ex.Start()
	return &hndlr
}

func (h *Handler) stat() {
	logger.Info("Send metrics")
	h.executor.StatChan <- stat.Rec{
		Name:  "request_count",
		Value: float64(h.counter.Load()),
	}
}

// Start the handler
func (h *Handler) Start() {
	h.wg.Add(1)
	go func() {
		<-h.stopChan
		logger.WithField("executor", h.config.Executor.Name).Info("Stop the process")
		h.executor.Stop()
		h.wg.Done()
	}()
}

// Stop the handler
func (h *Handler) Stop() {
	h.stopChan <- struct{}{}
	h.wg.Wait()
}

func defaultScenario() executor.Scenario {
	return executor.Scenario{
		{
			Name: "dumb",
			Type: "set",
			Data: "dumb",
		},
	}
}

func processQueryParams(r *http.Request) map[string]string {
	params := make(map[string]string)
	for key, value := range r.URL.Query() {
		if len(value) > 1 {
			data, err := json.Marshal(value)
			if err == nil {
				params[key] = string(data)
			}
		} else {
			params[key] = value[0]
		}
	}
	return params
}

func registerTasks() {
	executor.RegisterTask(SetGlobalValueKey, func(e *executor.Executor, task executor.Task) error {
		geRaw := e.GetContextValue(GlobalExecutorKey)
		var ge *executor.Executor
		if geRaw != nil {
			ge = geRaw.(*executor.Executor)
		} else {
			return fmt.Errorf("failed on retrieving the global executor")
		}
		name, err := e.ProcessTemplate(task.Name)
		if err == nil {
			task.Name = name
		}
		data, err := e.ProcessTemplate(task.Data)
		if err == nil {
			task.Data = data
		}
		extra, err := e.ProcessTemplate(task.Extra)
		if err == nil {
			task.Extra = extra
		}
		ge.SetContextValue(fmt.Sprintf("%s_type", task.Name), task.Extra)
		return new(executor.SetAction).Run(ge, task)
	})
	executor.RegisterTask(SetGlobalMapValueKey, func(e *executor.Executor, task executor.Task) error {
		geRaw := e.GetContextValue(GlobalExecutorKey)
		var ge *executor.Executor
		if geRaw != nil {
			ge = geRaw.(*executor.Executor)
		} else {
			return fmt.Errorf("failed on retrieving the global executor")
		}
		return new(executor.SetMapValueAction).Run(ge, task)
	})
	executor.RegisterTask(GetGlobalValueKey, func(e *executor.Executor, task executor.Task) error {
		geRaw := e.GetContextValue(GlobalExecutorKey)
		var ge *executor.Executor
		if geRaw != nil {
			ge = geRaw.(*executor.Executor)
		} else {
			return fmt.Errorf("failed on retrieving the global executor")
		}
		name, err := e.ProcessTemplate(task.Name)
		if err != nil {
			name = task.Name
		}
		key, err := e.ProcessTemplate(task.Target)
		if err != nil {
			key = task.Target
		}
		value := ge.GetContextValue(key)
		if value == nil {
			return nil
		}
		valueType := ge.GetContextValue(fmt.Sprintf("%s_type", key))
		return new(executor.SetAction).Run(e, executor.Task{
			Name:  name,
			Data:  fmt.Sprintf("%v", value),
			Extra: fmt.Sprintf("%s", valueType),
		})
	})
}

func parseAndAddHeaders(w *http.ResponseWriter, headersStr string, ex *executor.Executor) error {
	if len(headersStr) == 0 {
		return nil
	}
	var headersMap map[string]string
	extra, err := ex.ProcessTemplate(headersStr)
	if err != nil {
		return err
	}
	err = json.Unmarshal([]byte(extra), &headersMap)
	if err != nil {
		return err
	}
	for headerName, headerValue := range headersMap {
		(*w).Header().Set(headerName, headerValue)
	}
	return nil
}
