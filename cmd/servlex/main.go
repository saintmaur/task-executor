package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	httpex "gitlab.com/saintmaur/lib/http"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/lib/profiler"
	"gitlab.com/saintmaur/lib/stat"
	extUtils "gitlab.com/saintmaur/lib/utils"
	"gitlab.com/saintmaur/task-executor/cmd/servlex/config"
	"gitlab.com/saintmaur/task-executor/cmd/servlex/handler"
)

var (
	version   string
	commit    string
	buildTime string
	appName   string
)

func main() {
	var configPath = flag.String("config", "", "config path")
	var dryRun = flag.Bool("dry", false, "don't start, just check the config")
	var prof = flag.Bool("profile", false, "enable profiling")
	var versionFlag = flag.Bool("version", false, "don't start, just show the version")
	flag.Parse()
	if *versionFlag {
		extUtils.PrintVersion(appName, version, commit, buildTime)
		return
	}
	if !config.InitConfig(*configPath) {
		fmt.Println("Exit")
		os.Exit(1)
	}
	var profilers []*profiler.Profiler
	if *prof {
		for _, cfg := range config.GeneralConfig.Profile {
			prflr := new(profiler.Profiler)
			profilers = append(profilers, prflr)
			prflr.Start(cfg)
		}
	}
	defer logger.Stop()
	if !logger.InitLogger(config.GeneralConfig.Logger.Filename,
		config.GeneralConfig.Logger.Level,
		config.GeneralConfig.Logger.FormatJSON) {
		fmt.Println("Failed to init logger")
		return
	}
	logger.Info("Initialization complete")
	logger.Info("Starting the application")
	srv := httpex.New(&config.GeneralConfig.Server)
	if srv == nil {
		logger.Info("Failed on creating a server. Stopping")
		os.Exit(1)
	}
	statChan := make(stat.Chan, 1000)
	defer close(statChan)
	stat.Run(statChan)
	handlers := make(map[string]*handler.Handler)
	for _, bCfg := range config.GeneralConfig.Handlers {
		if b := handler.New(
			bCfg, statChan,
			config.GeneralConfig.Predefined,
			config.GeneralConfig.Variables, srv); b != nil && !(*dryRun) {
			b.Start()
			handlers[bCfg.Executor.Name] = b
		}
	}
	handlersCount := len(handlers)
	if handlersCount == 0 {
		logger.Info("No server has been started. Stopping.")
		stat.Stop()
		os.Exit(1)
	}
	srv.Run()
	logger.Info("The server has been started")
	sigs := make(chan os.Signal, handlersCount)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	<-sigs
	logger.Info("A signal has been received. Stopping")
	for _, b := range handlers {
		b.Stop()
	}
	srv.Stop()
	stat.Stop()
	for _, prflr := range profilers {
		prflr.Stop()
	}
}
