# Purpose
Create an HTTP server simply.

## Config general layout
```yaml
handlers:
  - executor:
      name: ""
      period: 24h
      scenario:
        - type: set
          name: status
          data: ready
        - type: unset
          name: update
      variables:
        - name: base_dir
          value: "configs/servlex/chaos"
      predefined:
        notify:
          - type: set
            name: message
            data: Message
    endpoints:
      "/":
        scenario:
          - type: glob
            name: list
            target: "{{.experiments_dir}}/*.yml"
          - type: file_read
            name: data_template
            target: "{{.index_template_path}}"
          - type: template
            name: data
            data: "{{.data_template}}"
        response:
          text: '{{.data}}'
          code: '200'
          headers: '{"content-type": "text/html", "server": "servlex"}'
logger:
  level: trace
  filename: debug.log
server:
  listen: 127.0.0.1:4444
```

# Command line parameters

Here's the result of running the binary with `-help` key
```
  -config string
        config path
  -dry
        don't start, just check the config
  -profile
        enable profiling
  -version
        don't start, just show the version
```

# Configuration

## Special variables available in scenario

- `body` - a string representing the request body
- `params` - a map representing request query params
- `params_raw` - a raw string representing request query params
- `path` - a string representing the request path
- `method` - a string representing the method
- `headers` - a map representing request headers
- `scheme` - protocol name
- `host` - serving host name
- `counter` - request counter per handler

## Additional task types

---

- `start_global_cron` – start a repeated task available globally

  params interpretation:

  - `name` – formal **_(templatable)_**
  - `data` – the cron string (e.g. '0 0 * * *')  or time duration (e.g. '5s')
  - `target` – the predefined scenario name to run periodically **_(templatable)_**

---

- `stop_global_cron` – stop the globally available repeated task

  params interpretation:

  - `name` – formal
  - `target` – the name of the executor (set by `name` in the start_global_cron task) **_(templatable)_**

---

- `set_global_value` – set a value available globally

  params interpretation:

  - `name` – a context key for the result value to be saved with (**_templatable_**)
  - `data` – a value to store (**_templatable_**)
  - `extra` – the type of the value

---

- `get_global_value`

  params interpretation:

  - `name` – the context key for the result value to be saved with (**_templatable_**)
  - `target ` – the variable name to get from the global context
