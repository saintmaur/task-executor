package config

import (
	"fmt"

	"github.com/spf13/viper"
	"gitlab.com/saintmaur/lib/profiler"
	"gitlab.com/saintmaur/task-executor/internal/executor"

	httpex "gitlab.com/saintmaur/lib/http"
	"gitlab.com/saintmaur/lib/logger"
)

// ResponseConfig describes an HTTP header
type ResponseConfig struct {
	Text    string `mapstructure:"text"`
	Code    string `mapstructure:"code"`
	Headers string `mapstructure:"headers"`
}

// Endpoint describes an endpoint activity
type Endpoint struct {
	Path     string            `mapstructure:"path"`
	Actions  executor.Scenario `mapstructure:"scenario"`
	Response ResponseConfig    `mapstructure:"response"`
}

// Handler describes a server
type Handler struct {
	StatPeriod string                  `mapstructure:"stat_period"`
	Endpoints  map[string]Endpoint     `mapstructure:"endpoints"`
	Executor   executor.ExecutorConfig `mapstructure:"executor"`
}

// Config is a highest level config type
type Config struct {
	Variables  executor.VariablesConfig     `mapstructure:"variables"`
	Predefined map[string]executor.Scenario `mapstructure:"predefined"`
	Logger     logger.LoggerConfig          `mapstructure:"logger"`
	Handlers   []Handler                    `mapstructure:"handlers"`
	Server     httpex.ServerConfig          `mapstructure:"server"`
	Profile    []profiler.ProfilerConfig    `mapstructure:"profile"`
}

var (
	GeneralConfig = new(Config)
)

func InitConfig(fileName string) bool {
	viper.SetConfigFile(fileName)
	fmt.Printf("Try to setup using the file: %s\n", viper.ConfigFileUsed())
	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("Failed on reading the config file (%s): %s\n", viper.ConfigFileUsed(), err)
		return false
	}
	if err := viper.Unmarshal(&GeneralConfig); err != nil {
		fmt.Println("Failed to unmarshal config: ", err)
		return false
	}
	return true
}
