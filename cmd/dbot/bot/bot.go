package bot

import (
	"encoding/json"
	"fmt"

	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/lib/stat"
	"gitlab.com/saintmaur/task-executor/cmd/dbot/chat"
	"gitlab.com/saintmaur/task-executor/cmd/dbot/config"
	"gitlab.com/saintmaur/task-executor/internal/executor"

	"github.com/bwmarrin/discordgo"
)

const (
	// MaxButtonsPerRow is what it names
	MaxButtonsPerRow = 5
)

// Bot wraps the API
type Bot struct {
	chats map[string]*chat.Chat
	bot   *discordgo.Session
	// incomingQueue chan *tgbotapi.Update
	// outgoingQueue chan *OutgoingMessage
	stopChan   chan bool
	config     config.BotConfig
	controller *executor.Executor
}

// New creates a new instance
func New(cfg config.BotConfig) *Bot {
	s, err := discordgo.New(config.GeneralConfig.Bot.Token)
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return nil
	}
	// TODO: control via config file
	s.Identify.Intents = discordgo.IntentsAll
	err = s.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return nil
	}
	logger.Info(fmt.Sprintf("Authorized bot %s", s.State.User.Username))
	b := Bot{
		bot:      s,
		chats:    make(map[string]*chat.Chat),
		stopChan: make(chan bool),
	}
	b.config = cfg
	// b.register()
	s.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
		if m.Author.ID == s.State.User.ID {
			return
		}
		b, err := json.Marshal(m)
		if err != nil {
			logger.Errorf("%s", err)
			return
		}
		logger.Infof("Message: %s", b)
		// b.getChat() <- m
		// if m.Content == "ping" {
		// 	s.ChannelMessageSend(m.ChannelID, "Pong!")
		// }

		// // If the message is "pong" reply with "Ping!"
		// if m.Content == "pong" {
		// 	s.ChannelMessageSend(m.ChannelID, "Ping!")
		// }
	})
	s.AddHandler(func(s *discordgo.Session, m *discordgo.MessageReactionAdd) {
		if m.UserID == s.State.User.ID {
			return
		}
		b, err := json.Marshal(m)
		if err != nil {
			logger.Errorf("%s", err)
			return
		}
		logger.Infof("Message: %s", b)
	})
	s.AddHandler(func(s *discordgo.Session, r *discordgo.Ready) {
		logger.Info("Bot is ready")
	})
	return &b
}

func (b *Bot) getChatID() {}

func (b *Bot) Stop() {
	b.controller.Stop()
	b.bot.Close()
	logger.Debug("Gonna stop this bot")
}

// Start creates a bot API client and starts to listen for updates
func (b *Bot) Start(statChan stat.Chan) {
	b.controller = executor.NewExecutor(config.GeneralConfig.Bot.Controller,
		executor.VariablesConfig{}, map[string]executor.Scenario{}, statChan)
	if b.controller == nil {
		return
	}
	b.controller.Start()
}

// func (b *Bot) dispatchMessage(event interface{}) {

// 	cht, ok := b.chats[chatID]
// 	if !ok {
// 		cht = chat.New(chatID, b.config)
// 		b.chats[chatID] = cht
// 		cht.Start()
// 	}
// }

// func (b *Bot) getChatID(update *tgbotapi.Update) int64 {
// 	var id int64
// 	if update.Message != nil {
// 		id = update.Message.Chat.ID
// 	}
// 	if update.EditedMessage != nil {
// 		id = update.EditedMessage.Chat.ID
// 	}
// 	if update.ChannelPost != nil {
// 		id = update.ChannelPost.Chat.ID
// 	}
// 	if update.EditedChannelPost != nil {
// 		id = update.ChannelPost.Chat.ID
// 	}
// 	if update.InlineQuery != nil {
// 		id = update.InlineQuery.From.ID
// 	}
// 	if update.ChosenInlineResult != nil {
// 		id = update.ChosenInlineResult.From.ID
// 	}
// 	if update.CallbackQuery != nil {
// 		id = update.CallbackQuery.Message.Chat.ID
// 	}
// 	if update.ShippingQuery != nil {
// 		id = update.ShippingQuery.From.ID
// 	}
// 	if update.PreCheckoutQuery != nil {
// 		id = update.PreCheckoutQuery.From.ID
// 	}
// 	if update.PollAnswer != nil {
// 		id = update.PollAnswer.User.ID
// 	}
// 	if update.MyChatMember != nil {
// 		id = update.MyChatMember.Chat.ID
// 	}
// 	if update.ChatMember != nil {
// 		id = update.ChatMember.Chat.ID
// 	}
// 	if update.ChatJoinRequest != nil {
// 		id = update.ChatMember.Chat.ID
// 	}
// 	return id
// }
