package chat

import (
	"strings"

	"gitlab.com/saintmaur/lib/logger"
)

// ChatStatus is a type alias
type ChatStatus int

// Context status enum
const (
	Ready ChatStatus = iota
	Busy
	Waiting
)

const (
	statusReadyKey   = "ready"
	statusWaitingKey = "waiting"
)

func mapStatusName(name string) ChatStatus {
	s := Ready
	switch strings.ToLower(name) {
	case statusReadyKey:
		s = Ready
	case statusWaitingKey:
		s = Waiting
	default:
		logger.Warnf("Failed to map status by name '%s', fallback to 'ready'", name)
	}
	return s
}
