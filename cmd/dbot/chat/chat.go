package chat

import (
	"fmt"
	"reflect"
	"sync"

	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/lib/stat"
	"gitlab.com/saintmaur/task-executor/cmd/dbot/config"
	"gitlab.com/saintmaur/task-executor/internal/executor"
	statemachine "gitlab.com/saintmaur/task-executor/internal/state-machine"
	"gitlab.com/saintmaur/task-executor/internal/utils"
)

type incomingChanType chan any

const (
	bufferSize = 1000
	DefaultCmd = "default"
)

// Chat is for each chat
type Chat struct {
	executor       *executor.Executor
	stateMachine   *statemachine.StateMachine
	stopChan       chan bool
	wg             sync.WaitGroup
	currentCommand string
	incomingChan   incomingChanType
	botConfig      config.BotConfig
	chatID         int64
}

// New creates a new chat
func New(id int64, cfg config.BotConfig) *Chat {
	c := Chat{
		stopChan:     make(chan bool),
		botConfig:    cfg,
		chatID:       id,
		incomingChan: make(incomingChanType, bufferSize),
	}
	return &c
}

// Stop does it
func (c *Chat) Stop() {
	c.stopChan <- true
	c.wg.Wait()
}

// Start does it
func (c *Chat) Start() {
	c.wg.Add(1)
	go func() {
		for {
			select {
			case <-c.stopChan:
				c.executor.Stop()
				c.wg.Done()
				return
			case event := <-c.incomingChan:
				if c.getStatus() == Ready {
					// c.setCurrentCommand(event)
					c.checkRunnable()
					c.initExecutionEnvironment()
				}
				status, _ := utils.PrepareString(c.executor.GetContextValue(config.StatusKey))
				logger.
					WithField("chat", fmt.Sprintf("%d", c.chatID)).
					WithField("status", status).
					WithField("command", c.currentCommand).
					WithField("state", c.stateMachine.CurrentState).
					Debugf("Process the incoming update")
				c.executor.SetContextValue("event", event)
				c.stateMachine.Run()
			}
		}
	}()
}

// func (c *Chat) setCurrentCommand(event interface{}) {
// 	if reflect.TypeOf(event) ==
// 	if update.Message != nil && len(update.Message.Text) > 0 {
// 		c.currentCommand = strings.ToLower(update.Message.Text)
// 	} else {
// 		c.currentCommand = DefaultCmd
// 	}
// }

func getEventType(input interface{}) reflect.Type {
	return reflect.TypeOf(input)
}

func prepareEvent(input interface{}) {}

func (c *Chat) initExecutionEnvironment() {
	scenarios := make(map[string]executor.Scenario)
	for name, scenario := range c.botConfig.Controller.Predefined {
		scenarios[name] = scenario
	}
	eCfg := executor.ExecutorConfig{
		Name:       fmt.Sprintf("chat_%d", c.chatID),
		Defaults:   config.GeneralConfig.Bot.Controller.Defaults,
		Scenario:   []executor.Task{},
		Predefined: scenarios,
	}
	c.executor = executor.NewExecutor(eCfg, executor.VariablesConfig{}, map[string]executor.Scenario{}, stat.StatC)
	c.executor.SetContextValue(config.ChatIDKey, c.chatID)
	c.executor.SetContextValue(config.StatusKey, statusReadyKey)
	c.stateMachine = statemachine.NewStateMachine(c.botConfig.Commands[c.currentCommand].States, c.executor)
}
func (c *Chat) getStatus() ChatStatus {
	status := Ready
	if c.executor != nil {
		statusName, _ := utils.PrepareString(c.executor.GetContextValue(config.StatusKey))
		status = mapStatusName(statusName)
	}
	return status
}

func (c *Chat) checkRunnable() {
	_, ok := c.botConfig.Commands[c.currentCommand]
	if !ok {
		c.currentCommand = DefaultCmd
	}
}
