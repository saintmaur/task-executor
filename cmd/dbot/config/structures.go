package config

import (
	httpex "gitlab.com/saintmaur/lib/http"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/task-executor/internal/executor"
	statemachine "gitlab.com/saintmaur/task-executor/internal/state-machine"
)

// Keys for new tasks
const (
	SetStatusKey              = "set_status"
	StartPeriodicKey          = "repeat"
	SubscribeKey              = "subscribe_on_periodic"
	UnsubscribeKey            = "unsubscribe_on_periodic"
	SendTextMessageKey        = "send_message"
	SendEditTextMessageKey    = "send_edit"
	SendPhotoMessageKey       = "send_photo"
	SendDocumentMessageKey    = "send_document"
	SendLocationMessageKey    = "send_location"
	AnswerCallbackQueryKey    = "answer_callback_query"
	GetFileURLKey             = "get_file_url"
	DeleteMessageKey          = "delete_message"
	SendPollKey               = "send_poll"
	SendStopPollKey           = "send_stop_poll"
	SendInvoiceKey            = "send_invoice"
	AnswerShippingQueryKey    = "answer_shipping_query"
	AnswerPrecheckoutQueryKey = "answer_precheckout_query"
	ChatActionKey             = "chat_action"

	MessageIDKey  = "message_id"
	ChatIDKey     = "chat_id"
	CallbackIDKey = "callback_id"
	StatusKey     = "status"
)

// CommandConfig describes a command
type CommandConfig struct {
	States []statemachine.StateConfig `mapstructure:"states"`
}

type BotConfig struct {
	Token      string                   `mapstructure:"token"`
	Commands   map[string]CommandConfig `mapstructure:"commands"`
	Controller executor.ExecutorConfig  `mapstructure:"controller"`
}

type Config struct {
	Logger logger.LoggerConfig `mapstructure:"logger"`
	Bot    BotConfig           `mapstructure:"bot"`
	Stat   httpex.ServerConfig `mapstructure:"stat"`
}
