package config

import (
	"fmt"

	"github.com/spf13/viper"
)

var (
	GeneralConfig = new(Config)
)

func InitConfig(address string, remote bool) bool {
	viper.SetConfigFile(address)
	fmt.Printf("Try to setup using the file: %s\n", viper.ConfigFileUsed())
	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("Failed on reading the config file (%s): %s\n", viper.ConfigFileUsed(), err)
		return false
	}
	if err := viper.Unmarshal(&GeneralConfig); err != nil {
		fmt.Println("Failed to unmarshal config: ", err)
		return false
	}
	return true
}
