# The purpose

Describe a state machine and run

# Command line parameters

Here's the result of running the binary with `-help` key
```
Usage:
  -config string
        the config path
  -dry
        don't start the executor(s), just check the config
  -profile
        enable profiling
  -version
        don't start, just show the version
```

# Configuration

## General layout

```
variables:
  - name: <string>
    value: <string>
predefined:
  name: <scenario>
objects:
  - name: <string>
    type: <string>
    state:
      - name: <string>
        value: <string>
    events:
      - period: <duration_string>
        scenario: <array of tasks>
logger:
    filename: <string> #if omitted log to stdout
    level: <debug|info|warn|error>
stat:
    listen: "[<host>]:<number>"
    probe_endpoint: /<string>
    tls:
      enabled: <bool>
      domain: <string>
      generate_certificate: <bool>
      cert_file: <string>
      key_file: <string>
profile:
  - type: (cpu|memory|mutex|block|trace|thread_create|goroutine)
    directory: <path>
```