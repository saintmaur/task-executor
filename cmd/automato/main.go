package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	httpex "gitlab.com/saintmaur/lib/http"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/lib/profiler"
	"gitlab.com/saintmaur/lib/stat"
	extUtils "gitlab.com/saintmaur/lib/utils"
	"gitlab.com/saintmaur/task-executor/cmd/automato/config"
	"gitlab.com/saintmaur/task-executor/internal/executor"
	statemachine "gitlab.com/saintmaur/task-executor/internal/state-machine"
)

var (
	version   string
	commit    string
	buildTime string
	appName   string
)

func main() {
	var configPath = flag.String("config", "", "the config path")
	var dryRun = flag.Bool("dry", false, "don't start the executor(s), just check the config")
	var prof = flag.Bool("profile", false, "enable profiling")
	var vars executor.VariablesConfig
	flag.Var(&vars, "set", "set new variables; format: name=value[:(bool|int|float|string)], default type is string")
	var versionFlag = flag.Bool("version", false, "don't start, just show the version")
	flag.Parse()
	if *versionFlag {
		extUtils.PrintVersion(appName, version, commit, buildTime)
		return
	}
	if !config.InitConfig(*configPath) {
		fmt.Println("Exit")
		return
	}
	if *prof {
		for _, cfg := range config.GeneralConfig.Profile {
			prflr := new(profiler.Profiler)
			prflr.Start(cfg)
			defer prflr.Stop()
		}
	}
	defer logger.Stop()
	if !logger.InitLogger(config.GeneralConfig.Logger.Filename,
		config.GeneralConfig.Logger.Level,
		config.GeneralConfig.Logger.FormatJSON) {
		fmt.Println("Failed to init logger")
		return
	}
	logger.Info("Initialization complete")
	logger.Info("Starting the application")
	srv := httpex.New(&config.GeneralConfig.Stat)
	if srv == nil {
		logger.Info("Failed on creating a metrics server. Stopping")
		os.Exit(1)
	}
	srv.Run()
	statC := make(stat.Chan, 1000)
	defer close(statC)
	stat.Run(statC)
	before := []executor.Task{}
	config.GeneralConfig.Variables = append(config.GeneralConfig.Variables, vars...)
	if config.GeneralConfig.Predefined == nil {
		config.GeneralConfig.Predefined = make(map[string]executor.Scenario)
	}
	e := executor.NewExecutor(
		executor.ExecutorConfig{
			Name:   "automator",
			Before: before,
		},
		config.GeneralConfig.Variables,
		config.GeneralConfig.Predefined,
		statC,
	)
	if e != nil && !(*dryRun) {
		e.Start()
	} else {
		logger.Info("No executor has been started. Stopping.")
		stat.Stop()
		os.Exit(1)
	}
	logger.Infof("Control executor has been started")
	stateMachine := statemachine.NewStateMachine(config.GeneralConfig.States, e)
	sigs := make(chan os.Signal, 2)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		cycles := config.GeneralConfig.Cycles
		if cycles == 0 {
			cycles = 1
		}
		for i := 0; i < cycles; i++ {
			stateMachine.Run()
			stateMachine.Reset()
		}
		sigs <- syscall.SIGINT
	}()
	<-sigs
	logger.Info("A signal has been received. Stopping.")
	start := time.Now()
	stateMachine.Stop()
	e.Stop()
	stat.Stop()
	srv.Stop()
	logger.Infof("Stopped for %s", time.Since(start))
}
