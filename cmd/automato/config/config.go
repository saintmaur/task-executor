package config

import (
	"fmt"

	"github.com/spf13/viper"
	httpex "gitlab.com/saintmaur/lib/http"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/lib/profiler"
	"gitlab.com/saintmaur/task-executor/internal/executor"
	statemachine "gitlab.com/saintmaur/task-executor/internal/state-machine"
)

var (
	GeneralConfig = new(Config)
)

func InitConfig(fileName string) bool {
	viper.SetConfigFile(fileName)
	fmt.Printf("Try to setup using the file: %s\n", viper.ConfigFileUsed())
	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("Failed on reading the config file (%s): %s\n", viper.ConfigFileUsed(), err)
		return false
	}
	if err := viper.Unmarshal(&GeneralConfig); err != nil {
		fmt.Println("Failed to unmarshal config: ", err)
		return false
	}
	return true
}

// Config is a highest level config type
type Config struct {
	Logger     logger.LoggerConfig          `mapstructure:"logger"`
	Variables  executor.VariablesConfig     `mapstructure:"variables"`
	Predefined map[string]executor.Scenario `mapstructure:"predefined"`
	Stat       httpex.ServerConfig          `mapstructure:"stat"`
	Profile    []profiler.ProfilerConfig    `mapstructure:"profile"`
	States     []statemachine.StateConfig   `mapstructure:"states"`
	Cycles     int                          `mapstructure:"cycles"`
}
