package config

import (
	"fmt"

	"github.com/spf13/viper"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/task-executor/internal/executor"
)

var (
	GeneralConfig = new(Config)
)

func InitConfig(fileName string) bool {
	viper.SetConfigFile(fileName)
	fmt.Printf("Try to setup using the file: %s\n", viper.ConfigFileUsed())
	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("Failed on reading the config file (%s): %s\n", viper.ConfigFileUsed(), err)
		return false
	}
	if err := viper.Unmarshal(&GeneralConfig); err != nil {
		fmt.Println("Failed to unmarshal config: ", err)
		return false
	}
	return true
}

type ClientConfig struct {
	URL      string            `mapstructure:"url"`
	Pipeline executor.Scenario `mapstructure:"pipeline"`
}

// Config is a highest level config type
type Config struct {
	Keys   []string            `mapstructure:"keys"`
	Logger logger.LoggerConfig `mapstructure:"logger"`
	Input  ClientConfig        `mapstructure:"input"`
	Output ClientConfig        `mapstructure:"output"`
}
