package main

import (
	"flag"
	"fmt"
	"time"

	"gitlab.com/saintmaur/task-executor/cmd/redis-sync/config"
	"gitlab.com/saintmaur/task-executor/cmd/redis-sync/rdbw"

	"gitlab.com/saintmaur/lib/logger"
)

var (
	version   string
	commit    string
	buildTime string
	appName   string
)

func main() {
	var configPath = flag.String("config", "", "the config path")
	flag.Parse()
	if !config.InitConfig(*configPath) {
		fmt.Println("Exit")
		return
	}
	defer logger.Stop()
	if !logger.InitLogger(config.GeneralConfig.Logger.Filename,
		config.GeneralConfig.Logger.Level,
		config.GeneralConfig.Logger.FormatJSON) {
		fmt.Println("Failed to init logger")
		return
	}
	logger.Info("Initialization complete")
	logger.Info("Starting the application")
	client := rdbw.NewClient(config.GeneralConfig.Input, config.GeneralConfig.Output)
	start := time.Now()
	count := 0
	var failedKeys []string
	for _, key := range config.GeneralConfig.Keys {
		for _, k := range client.Keys(key) {
			tp := client.Type(k)
			logger.Infof("<<< Get the key %s", k)
			data := client.Get(k, tp)
			logger.Infof(">>> Store the value of type (%s) by the key %s", tp, k)
			err := client.Store(k, tp, data)
			if err != nil {
				logger.Errorf("Failed on storing the key %s: %s", k, err)
				failedKeys = append(failedKeys, k)
			} else {
				count++
			}
		}
	}
	client.Stop()
	logger.Infof("Synchronized %d key(s) in %s", count, time.Since(start))
	if len(failedKeys) > 0 {
		logger.Infof("Failed on synchronizing keys:")
		for _, key := range failedKeys {
			logger.Infof("--> %s", key)
		}
	}
}
