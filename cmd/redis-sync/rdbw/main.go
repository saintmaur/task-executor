package rdbw

import (
	"context"
	"fmt"
	"time"

	"github.com/redis/go-redis/v9"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/task-executor/cmd/redis-sync/config"
	"gitlab.com/saintmaur/task-executor/internal/executor"
	"gitlab.com/saintmaur/task-executor/internal/utils"
)

const (
	maxIndex   = 1000000
	stringType = "string"
	listType   = "list"
	setType    = "set"
	hashType   = "hash"
)

type Valuable interface {
	Err() error
}

type Processor struct {
	ex *executor.Executor
	c  *redis.Client
}

type Client struct {
	input  *Processor
	output *Processor
}

func NewClient(inputCfg config.ClientConfig,
	outputCfg config.ClientConfig) *Client {
	client := Client{
		input: &Processor{
			ex: executor.NewExecutor(executor.ExecutorConfig{
				Name:     "redis-sync-input",
				Scenario: inputCfg.Pipeline,
			}, nil, nil, nil),
			c: newRedisClient(inputCfg.URL).WithTimeout(time.Hour),
		},
		output: &Processor{
			ex: executor.NewExecutor(executor.ExecutorConfig{
				Name:     "redis-sync-output",
				Scenario: outputCfg.Pipeline,
			}, nil, nil, nil),
			c: newRedisClient(outputCfg.URL).WithTimeout(time.Hour),
		},
	}
	return &client
}

func newRedisClient(url string) *redis.Client {
	opts, err := redis.ParseURL(url)
	if err != nil {
		logger.Errorf("Failed on parsing the options: %s", err)
	}
	return redis.NewClient(opts)
}

func (c *Client) Keys(pattern string) []string {
	ctx := context.Background()
	keys := c.input.c.Keys(ctx, pattern)
	return keys.Val()
}

func (c *Client) Type(key string) string {
	ctx := context.Background()
	tp := c.input.c.Type(ctx, key)
	return tp.Val()
}

func (c *Client) Get(key, tp string) interface{} {
	var data interface{}
	ctx := context.Background()
	switch tp {
	case stringType:
		value := c.input.c.Get(ctx, key)
		if value.Err() != nil {
			logger.Errorf("Failed on getting the key %s: %s", key, value.Err())
			return nil
		}
		data = value.Val()
	case listType:
		value := c.input.c.LRange(ctx, key, 0, maxIndex)
		if value.Err() != nil {
			logger.Errorf("Failed on getting the key %s: %s", key, value.Err())
			return nil
		}
		data = value.Val()
	case setType:
		value := c.input.c.SMembers(ctx, key)
		if value.Err() != nil {
			logger.Errorf("Failed on getting the key %s: %s", key, value.Err())
			return nil
		}
		data = value.Val()
	case hashType:
		value := c.input.c.HGetAll(ctx, key)
		if value.Err() != nil {
			logger.Errorf("Failed on getting the key %s: %s", key, value.Err())
			return nil
		}
		data = value.Val()
	default:
		logger.Errorf("No store handler is implemented for type '%s'", tp)
	}
	return data
}
func (c *Client) Store(key, tp string, data interface{}) error {
	var value Valuable
	ctx := context.Background()
	h := func(key, value string) string {
		c.output.ex.SetContextValue("key", key)
		c.output.ex.SetContextValue("value", value)
		c.output.ex.RunScenario(c.output.ex.Scenario, fmt.Sprintf("%d", time.Now().UnixNano()))
		result, _ := utils.PrepareString(c.output.ex.GetContextValue("value"))
		return result
	}
	switch tp {
	case stringType:
		processed := h(key, data.(string))
		if len(processed) > 0 {
			value = c.output.c.Set(ctx, key, processed, 0)
		}
	case listType:
		var filtered []string
		for _, value := range data.([]string) {
			processed := h(key, value)
			if len(processed) > 0 {
				filtered = append(filtered, processed)
			}
		}
		value = c.output.c.RPush(ctx, key, filtered)
	case setType:
		var filtered []string
		for _, value := range data.([]string) {
			processed := h(key, value)
			if len(processed) > 0 {
				filtered = append(filtered, processed)
			}
		}
		value = c.output.c.SAdd(ctx, key, filtered)
	case hashType:
		filtered := make(map[string]string)
		for k, value := range data.(map[string]string) {
			processed := h(key, value)
			if len(processed) > 0 {
				filtered[k] = processed
			}
		}
		value = c.output.c.HSet(ctx, key, filtered)
	default:
		return fmt.Errorf("no store handler is implemented for type '%s'", tp)
	}
	return value.Err()
}

func (c *Client) Stop() {
	if e := c.input.c.Close(); e != nil {
		logger.Errorf("%s", e)
	}
	if e := c.output.c.Close(); e != nil {
		logger.Errorf("%s", e)
	}
}
