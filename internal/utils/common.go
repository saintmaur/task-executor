package utils

import (
	"database/sql"
	"fmt"
	"reflect"
	"strconv"
	"strings"

	"github.com/vitali-fedulov/images"

	"github.com/tidwall/gjson"
)

// direction constants
const (
	UPPER = "upper"
	LOWER = "lower"
)

// SplitStringByFirstSymbol does the thing
func SplitStringByFirstSymbol(valueStr string) (values []string) {
	if len(valueStr) > 0 {
		valueSep := string(valueStr[0])
		valueStr = strings.Trim(valueStr, valueSep)
		values = strings.Split(valueStr, valueSep)
	}
	return values
}

// CheckIsArray checks the value being of array [of strings] type
func CheckIsArray(value interface{}) bool {
	return (CheckIsType(value, reflect.TypeOf(gjson.Result{})) &&
		value.(gjson.Result).IsArray()) ||
		CheckIsType(value, reflect.TypeOf([]string{}))
}

// CheckIsString checks the value being of string type
func CheckIsString(value interface{}) bool {
	return CheckIsType(value, reflect.TypeOf(""))
}

// CheckIsInt checks the value being of int type
func CheckIsInt(value interface{}) bool {
	return CheckIsType(value, reflect.TypeOf(int(0)))
}

// CheckIsType checks the value being the type of t
func CheckIsType(value interface{}, t reflect.Type) bool {
	return value != nil && reflect.TypeOf(value) == t
}

// PrepareString return a string or an error
func PrepareString(value interface{}) (string, error) {
	var result string
	var err error
	if value == nil {
		return result, fmt.Errorf("the value is nil")
	}
	switch value := value.(type) {
	case gjson.Result:
		result = value.String()
	case []byte:
		result = string(value)
	default:
		result = fmt.Sprintf("%v", value)
	}
	return result, err
}

// PrepareInt return a int64 or an error
func PrepareInt(value interface{}) (int64, error) {
	result := int64(0)
	var err error
	switch value := value.(type) {
	case gjson.Result:
		result = int64(value.Int())
	case string:
		var flt float64
		flt, err = PrepareFloat(value)
		if err != nil {
			return 0, err
		}
		result, err = PrepareInt(flt)
	case int:
		m := value
		result = int64(m)
	case int32:
		m := value
		result = int64(m)
	case int64:
		result = value
	case float64:
		result = int64(value)
	default:
		err = fmt.Errorf("failed to convert %v to int64", reflect.TypeOf(value))
	}
	return result, err
}

// PrepareInt return a int64 or an error
func PrepareBool(value interface{}) (bool, error) {
	result := false
	var err error
	switch value := value.(type) {
	case gjson.Result:
		result = value.Bool()
	case string:
		prepared := strings.TrimSpace(value)
		result = prepared != "0" && prepared != "false"
	case int:
		result = !(value == 0)
	case int32:
		result = !(value == 0)
	case int64:
		result = !(value == 0)
	case float64:
		result = !(value == 0)
	default:
		err = fmt.Errorf("failed to convert %v to int64", reflect.TypeOf(value))
	}
	return result, err
}
func PrepareByte(value interface{}) ([]byte, error) {
	var err error
	stringValue, err := PrepareString(value)
	if err != nil {
		return nil, err
	}
	return []byte(stringValue), err
}

// PrepareFloat return a float64 or an error
func PrepareFloat(value interface{}) (float64, error) {
	result := float64(0)
	var err error
	switch value := value.(type) {
	case gjson.Result:
		result = float64(value.Float())
	case string:
		result, err = strconv.ParseFloat(value, 64)
	case int:
		m := value
		result = float64(m)
	case int32:
		m := value
		result = float64(m)
	case int64:
		m := value
		result = float64(m)
	case float64:
		result = value
	default:
		err = fmt.Errorf("failed to convert %s to float64", reflect.TypeOf(value))
	}
	return result, err
}

// Prepare returns the value of given type if possible
func Prepare(v interface{}, tp string) (interface{}, error) {
	var value interface{}
	var err error
	switch tp {
	case "string":
		value, err = PrepareString(v)
	case "float":
		value, err = PrepareFloat(v)
	case "int":
		value, err = PrepareInt(v)
	case "bool":
		value, err = PrepareBool(v)
	case "byte":
		value, err = PrepareByte(v)
	default:
		value, err = PrepareString(v)
	}
	return value, err
}

func PrepareArray(origin interface{}) []interface{} {
	var result []interface{}
	if origin != nil {
		switch reflect.TypeOf(origin).Kind() {
		case reflect.TypeOf(gjson.Result{}).Kind():
			if origin.(gjson.Result).IsArray() {
				result = PrepareArray(origin.(gjson.Result).Array())
			}
		case reflect.Slice, reflect.Array:
			s := reflect.ValueOf(origin)
			for i := 0; i < s.Len(); i++ {
				result = append(result, s.Index(i).Interface())
			}
		case reflect.Map:
			type T struct {
				Key   string      `json:"key"`
				Value interface{} `json:"value"`
			}
			for key, value := range origin.(map[string]interface{}) {
				result = append(result, T{
					Key:   key,
					Value: value,
				})
			}
		case reflect.String:
			gjsonned := gjson.Parse(origin.(string))
			result = PrepareArray(gjsonned)
		}
	}
	return result
}

// GetArray returns an array of strings from interface if it is a gjson array or builtin
func GetArray(a interface{}) []string {
	var result []string
	processGSON := func(a interface{}) {
		arr := a.(gjson.Result)
		for _, rec := range arr.Array() {
			result = append(result, rec.String())
		}
	}
	if CheckIsType(a, reflect.TypeOf(gjson.Result{})) &&
		a.(gjson.Result).IsArray() {
		processGSON(a)
	} else if CheckIsArray(a) {
		result = a.([]string)
	} else if CheckIsString(a) {
		parsed := gjson.Parse(a.(string))
		processGSON(parsed)
	}
	return result
}

// GetVariables prepares placeholders for db query
func GetVariables(types []*sql.ColumnType) []interface{} {
	var vars []interface{}
	for _, varType := range types {
		varP := reflect.New(varType.ScanType()).Interface()
		vars = append(vars, varP)
	}
	return vars
}

// GetGJSON ensures gjson.Result format
func GetGJSON(data interface{}) gjson.Result {
	var result gjson.Result
	if CheckIsString(data) {
		result = gjson.Parse(data.(string))
	} else if CheckIsType(data, reflect.TypeOf(gjson.Result{})) {
		result = data.(gjson.Result)
	}
	return result
}

// GetSQLNullableValue gets the underlying value
func GetSQLNullableValue(value interface{}) interface{} {
	var result interface{}
	switch value := value.(type) {
	case *sql.NullString:
		result, _ = value.Value()
	case *sql.NullBool:
		result, _ = value.Value()
	case *sql.NullInt16:
		result, _ = value.Value()
	case *sql.NullInt32:
		result, _ = value.Value()
	case *sql.NullInt64:
		result, _ = value.Value()
	case *sql.NullFloat64:
		result, _ = value.Value()
	case *sql.NullTime:
		result, _ = value.Value()
	case *sql.NullByte:
		result, _ = value.Value()
	default:
		// Super-duper hacky trick for the case when the type is `**interface{}`
		result = reflect.ValueOf(value).Elem().Elem().Elem()
	}
	return result
}

func CheckImagesAreSimilar(path1, path2 string) (bool, error) {
	imgA, err := images.Open(path1)
	if err != nil {
		return true, fmt.Errorf("failed while opening the image (%s): %s", path1, err)
	}
	imgB, err := images.Open(path2)
	if err != nil {
		return true, fmt.Errorf("failed while opening the image (%s): %s", path2, err)
	}
	// Calculate hashes
	hA, imgSizeA := images.Hash(imgA)
	hB, imgSizeB := images.Hash(imgB)
	// Image comparison
	if images.Similar(hA, hB, imgSizeA, imgSizeB) {
		return true, nil
	} else {
		return false, nil
	}
}
