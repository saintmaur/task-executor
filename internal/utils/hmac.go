package utils

import (
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/hex"
	"fmt"
	"hash"
	"io"
)

// Sign signs the data with the key by algo
func Sign(algo, key, data string) string {
	h := getHashFunc(algo)
	mac := hmac.New(h, []byte(key))
	mac.Write([]byte(data))
	return hex.EncodeToString(mac.Sum(nil))
}

// CheckSignature checks the signed data
func CheckSignature(algo, key, signed, data string) bool {
	signature := Sign(algo, key, data)
	return hmac.Equal([]byte(signed), []byte(signature))
}

func getHashFunc(algo string) func() hash.Hash {
	var h func() hash.Hash
	switch algo {
	case MD5:
		h = md5.New
	case SHA1:
		h = sha1.New
	case SHA256:
		h = sha256.New
	case SHA512:
		h = sha512.New
	default:
		h = sha256.New
	}
	return h
}

// Hash by algo
func Hash(algo, data string) string {
	h := getHashFunc(algo)()
	io.WriteString(h, data)
	return fmt.Sprintf("%x", h.Sum(nil))
}
