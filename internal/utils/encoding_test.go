package utils

import (
	"testing"
)

const text = "text?param=value&param=value"

var hashData = [][]string{
	{MD5, text, "9d478bc84e4c9c29d821e700f1cc845e"},
	{SHA256, text, "0f0e1d88049704c48bc2f66af6ba372e301bf81d6a3903c15843134750edc847"},
	{SHA512, text, "7d56238e4e4da52f33d19ea0fb83e44cc50ccaa65c2e34bd88b1533c55cce4f91dfb93c96c1a58655fa18bc9d3326381ae1a3085edf4e3081cee31f1d062dff5"},
}
var decodingData = [][]string{
	{URLQuery, text, "text%3Fparam%3Dvalue%26param%3Dvalue"},
	{Base32, text, "ORSXQ5B7OBQXEYLNHV3GC3DVMUTHAYLSMFWT25TBNR2WK==="},
	{Base64, text, "dGV4dD9wYXJhbT12YWx1ZSZwYXJhbT12YWx1ZQ=="},
	{HEX, "text", "74657874"},
	{Base58, "eb76c7399d0771b269dcd94d4b51ebe2sdfv626194fc", "1NU25wGt5Y2qUMbvLeYxNVPDawaRhh8WUD"},
}

func TestUtils_CheckHash(t *testing.T) {
	for _, arr := range hashData {
		result := Hash(arr[0], arr[1])
		if result != arr[2] {
			t.Errorf("Failed on %s:\n\tactual: %v\n\texpected: %s", arr[0], result, arr[2])
		}
	}
}
func TestUtils_CheckEncoding(t *testing.T) {
	for _, arr := range decodingData {
		decoded := Decode(arr[0], arr[2])
		encoded := Encode(arr[0], decoded, "")
		if encoded != arr[2] {
			t.Fatalf("Failed on '%s':\n\tencoded: %v\n\tdecoded: %s\n\texpected(encoded): %s\n", arr[0], encoded, decoded, arr[2])
		}
	}
}
