package utils

import (
	"crypto/tls"

	"github.com/gorilla/websocket"
	"gitlab.com/saintmaur/lib/logger"
)

func NewWebSocketConnection(url string) *websocket.Conn {
	d := websocket.Dialer{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	}
	conn, response, err := d.Dial(url, nil)
	if err != nil {
		logger.Warnf("Failed on dialing the '%s' with error: %s", url, err)
		return nil
	}
	if (response.StatusCode != 200) && (response.StatusCode != 101) {
		logger.Warnf("Failed on dialing the '%s' with status code: %d", url, response.StatusCode)
		return nil
	}
	return conn
}
