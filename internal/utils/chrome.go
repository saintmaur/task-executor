package utils

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/cdproto/network"
	"github.com/chromedp/cdproto/page"
	"github.com/chromedp/chromedp"
	"github.com/chromedp/chromedp/kb"
)

func VisibleViewportScreenshot(quality int64, res *[]byte) chromedp.Tasks {
	return chromedp.Tasks{
		chromedp.ActionFunc(func(ctx context.Context) error {
			// get layout metrics
			_, visualViewport, _, _, _, _, _ := page.GetLayoutMetrics().Do(ctx)
			var err error
			*res, err = page.CaptureScreenshot().
				WithQuality(quality).
				WithClip(&page.Viewport{
					X:      visualViewport.PageX,
					Y:      visualViewport.PageY,
					Width:  visualViewport.ClientWidth,
					Height: visualViewport.ClientHeight,
					Scale:  1,
				}).Do(ctx)
			if err != nil {
				return err
			}
			return nil
		}),
	}
}

func SetCookie(ctx context.Context, data string) chromedp.Tasks {
	return chromedp.Tasks{
		chromedp.ActionFunc(func(ctx context.Context) error {
			var cookie http.Cookie
			err := json.Unmarshal([]byte(data), &cookie)
			if err != nil {
				return err
			}
			if len(cookie.Name) == 0 || len(cookie.Value) == 0 {
				return fmt.Errorf("cookie name or value is missing")
			}
			expr := cdp.TimeSinceEpoch(time.Now().Add(180 * 24 * time.Hour))
			ns := network.SetCookie(cookie.Name, cookie.Value).
				WithDomain(cookie.Domain).
				WithPath(cookie.Path).
				WithExpires(&expr).
				WithHTTPOnly(cookie.HttpOnly).
				WithSecure(cookie.Secure)
			err = ns.Do(ctx)
			if err != nil {
				return err
			}
			return nil
		}),
	}
}

func FindKeyByCode(code string) string {
	for r, key := range kb.Keys {
		if key.Code == code {
			fmt.Printf("%U", r)
			//return fmt.Sprintf("%U", r)
			return key.Text
		}
	}
	return ""
}

func GetStringFromCodes(codes []string) string {
	var result string
	for _, code := range codes {
		result = result + FindKeyByCode(code)
		fmt.Println(result)
	}
	return result
}
