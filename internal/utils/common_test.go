package utils

import (
	"encoding/json"
	"fmt"
	"os"
	"reflect"
	"testing"

	"github.com/tidwall/gjson"
	"gitlab.com/saintmaur/lib/logger"
)

func TestMain(m *testing.M) {
	defer logger.Stop()
	if !logger.InitLogger("", "info", false) {
		fmt.Println("Failed to init the logger")
		os.Exit(1)
	}
	code := m.Run()
	os.Exit(code)
}

func TestUtils_SplitString(t *testing.T) {
	valueStr := "|valueOne,valueOneContinued|ValueTwo"
	expected := []string{"valueOne,valueOneContinued", "ValueTwo"}
	actual := SplitStringByFirstSymbol(valueStr)
	b1, err := json.Marshal(expected)
	if err != nil {
		t.Fatal(err)
	}
	b2, err := json.Marshal(actual)
	if err != nil {
		t.Fatal(err)
	}
	if string(b1) != string(b2) {
		t.Fatalf("Not equal:\n\texpected: %v\n\tactual: %v", expected, actual)
	}
}

func TestUtils_PrepareArray(t *testing.T) {
	process := func(value interface{}, l int) {
		arr := PrepareArray(value)
		if len(arr) != l {
			t.Fatalf("Failed to prepare the array: %v", arr)
		}
	}
	str := `["one", "two", "three"]`
	process(str, 3)
	arrStr := []string{"one", "two", "three"}
	process(arrStr, 3)
	arrInt := []int{1, 3, 4}
	process(arrInt, 3)
	arrJson := gjson.Parse(str)
	process(arrJson, 3)
}

func TestUtils_Prepare(t *testing.T) {
	data := []struct {
		d any
		e any
		t string
	}{
		{
			d: "1",
			t: "int",
			e: int64(1),
		},
		{
			d: 1,
			t: "int",
			e: int64(1),
		},
		{
			d: int32(1),
			t: "int",
			e: int64(1),
		},
		{
			d: int64(1),
			t: "int",
			e: int64(1),
		},
		{
			d: 1.1,
			t: "int",
			e: int64(1),
		},
		{
			d: "1",
			t: "bool",
			e: true,
		},
		{
			d: "0",
			t: "bool",
			e: false,
		},
		{
			d: 1,
			t: "bool",
			e: true,
		},
		{
			d: 1.0,
			t: "bool",
			e: true,
		},
		{
			d: int32(1),
			t: "bool",
			e: true,
		},
		{
			d: int64(1),
			t: "bool",
			e: true,
		},
		{
			d: "20.4",
			t: "float",
			e: 20.4,
		},
		{
			d: 20.4,
			t: "float",
			e: 20.4,
		},
		{
			d: 20,
			t: "float",
			e: 20.0,
		},
		{
			d: int32(20),
			t: "float",
			e: 20.0,
		},
		{
			d: int64(20),
			t: "float",
			e: 20.0,
		},
		{
			d: "string",
			t: "string",
			e: "string",
		},
		{
			d: []byte("string"),
			t: "string",
			e: "string",
		},
		{
			d: "string",
			t: "byte",
			e: []byte("string"),
		},
	}
	for _, rec := range data {
		logger.Debugf("Prepare %s from %s\n", rec.t, rec.d)
		r, err := Prepare(rec.d, rec.t)
		if err != nil {
			t.Fatal(err)
		}
		if !reflect.DeepEqual(r, rec.e) {
			t.Fatalf("Values are distinct: %v <> %v", r, rec.e)
		}
	}
}

func TestUtils_CheckType(t *testing.T) {
	str := "a string"
	arr := gjson.Parse("[1,2,3]")
	if !CheckIsArray(arr) {
		t.Fatal("Not an array")
	}
	if !CheckIsString(str) {
		t.Fatal("Not a string")
	}
	if !CheckIsInt(1) {
		t.Fatal("Not an integer")
	}
}

func TestUtils_HMAC(t *testing.T) {
	key := "123"
	data := "message"
	algo := "sha256"
	signed := Sign(algo, key, data)
	if !CheckSignature(algo, key, signed, data) {
		t.Fatal("MAC check has failed")
	}
}

func TestUtils_PrepareString(t *testing.T) {
	str := "a string"
	e1 := "a string"
	arr1 := gjson.Parse("[1,2,3]")
	e21 := "[1,2,3]"
	arr2 := []string{"1", "2", "3"}
	e22 := "[1 2 3]"
	num := int(5)
	e3 := "5"
	r1, _ := PrepareString(str)
	r21, _ := PrepareString(arr1)
	r22, _ := PrepareString(arr2)
	r3, _ := PrepareString(num)
	if r1 != e1 {
		t.Fatal("Not a string")
	}
	if r21 != e21 {
		t.Fatal("Not an array: ", r21)
	}
	if r22 != e22 {
		t.Fatal("Not an array: ", r22)
	}
	if r3 != e3 {
		t.Fatal("Not a number: ", r3)
	}
}
