package utils

import (
	"encoding/base32"
	"encoding/base64"
	"encoding/hex"
	"net/url"

	"github.com/btcsuite/btcutil/base58"
	"gitlab.com/saintmaur/lib/logger"
)

// Hashing algorithm names for switch
const (
	MD5      = "md5"
	SHA1     = "sha1"
	SHA256   = "sha256"
	SHA512   = "sha512"
	Base32   = "base32"
	Base58   = "base58"
	Base64   = "base64"
	URLPath  = "url_path"
	URLQuery = "url_query"
	HEX      = "hex"
	UNICODE  = "unicode"
)

// Encode by method
func Encode(method, data, extra string) string {
	var result string
	switch method {
	case Base32:
		result = base32.StdEncoding.EncodeToString([]byte(data))
	case Base58:
		var v byte = 0
		if len(extra) > 0 {
			v = byte(extra[0])
		}
		result = base58.CheckEncode([]byte(data), v)
	case Base64:
		result = base64.StdEncoding.EncodeToString([]byte(data))
	case URLQuery:
		result = url.QueryEscape(data)
	case URLPath:
		result = url.PathEscape(data)
	case HEX:
		result = hex.EncodeToString([]byte(data))
	}
	return result
}

// Decode the data
func Decode(method, data string) string {
	var result string
	var err error
	var b []byte
	switch method {
	case Base32:
		b, err = base32.StdEncoding.DecodeString(data)
		result = string(b)
	case Base58:
		b, _, err = base58.CheckDecode(data)
		result = string(b)
	case Base64:
		b, err = base64.StdEncoding.DecodeString(data)
		result = string(b)
	case URLQuery:
		result, err = url.QueryUnescape(data)
	case URLPath:
		result, err = url.PathUnescape(data)
	case HEX:
		b, err = hex.DecodeString(data)
		result = string(b)
	}
	if err != nil {
		logger.Errorf("Failed on decoding a string (%s) by algorithm (%s): %s", data, method, err)
		return result
	}
	return result
}
