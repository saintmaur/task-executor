package utils

import (
	"encoding/json"
	"fmt"

	"github.com/go-gomail/gomail"
)

type EmailTarget struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
	Name     string `json:"name"`
}

type EmailHeaders map[string]string

func ParseEmailHeaders(data string) (EmailHeaders, error) {
	var emailHeaders EmailHeaders
	err := json.Unmarshal([]byte(data), &emailHeaders)
	if err != nil {
		return nil, err
	}
	return emailHeaders, nil
}
func ParseEmailTransport(data string) (*EmailTarget, error) {
	var emailTarget EmailTarget
	err := json.Unmarshal([]byte(data), &emailTarget)
	if err != nil {
		return nil, err
	}
	return &emailTarget, nil
}

func SendEmail(emailTarget *EmailTarget, emailHeaders EmailHeaders, body string) error {
	d := gomail.NewDialer(emailTarget.Host, emailTarget.Port, emailTarget.User, emailTarget.Password)
	s, err := d.Dial()
	if err != nil {
		return err
	}
	m := gomail.NewMessage()
	if f, ok := emailHeaders["Attach"]; ok {
		m.Attach(f)
		delete(emailHeaders, "Attach") // Delete it to avoid unknown header
	}
	emailHeaders["From"] = m.FormatAddress(emailHeaders["From"], emailTarget.Name)
	emailHeaders["To"] = m.FormatAddress(emailHeaders["To"], emailHeaders["Name"])
	for key, value := range emailHeaders {
		fmt.Printf("%s: %s\n", key, value)
		m.SetHeader(key, value)
	}
	m.AddAlternative("multipart/alternative", "plan")
	m.SetBody("text/html", body)
	if err := gomail.Send(s, m); err != nil {
		return err
	}
	m.Reset()
	return nil
}
