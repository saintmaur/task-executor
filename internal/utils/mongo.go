package utils

import (
	"context"
	"encoding/json"
	"fmt"
	"net/url"
	"time"

	"gitlab.com/saintmaur/lib/logger"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

// MongoClient describes mongo connection
type MongoClient struct {
	client     *mongo.Client
	collection *mongo.Collection
	filter     map[string]interface{}
	documents  []interface{}
	update     map[string]interface{}
}

func mongoConnect(dsn, filter, update, documents string) (r *MongoClient, err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	r = &MongoClient{}
	options := options.Client()
	options.ApplyURI(dsn)
	r.client, err = mongo.Connect(ctx, options)
	if err != nil {
		return nil, err
	}
	err = r.client.Ping(ctx, readpref.Primary())
	if err != nil {
		return nil, err
	}
	u, err := url.Parse(dsn)
	if err != nil {
		return nil, err
	}
	dbName := u.Query().Get("db")
	collectionName := u.Query().Get("collection")
	err = json.Unmarshal([]byte(filter), &r.filter)
	if err != nil {
		return nil, fmt.Errorf("failed on parsing the filter: '%s'", filter)
	}
	err = json.Unmarshal([]byte(update), &r.update)
	if len(update) > 0 && err != nil {
		return nil, fmt.Errorf("failed on parsing the update query: '%s'", update)
	}
	err = json.Unmarshal([]byte(documents), &r.documents)
	if len(documents) > 0 && err != nil {
		return nil, fmt.Errorf("failed on parsing the documents list: '%s'", documents)
	}
	if len(dbName) == 0 || len(collectionName) == 0 {
		return nil, fmt.Errorf("DB or collection name is not set")
	}
	r.collection = r.client.Database(dbName).Collection(collectionName)
	return r, nil
}

// MongoGet gets data from mongo db
func MongoGet(dsn, filter, sort string) (string, error) {
	r, err := mongoConnect(dsn, filter, "", "")
	if err != nil {
		return "", fmt.Errorf("connect: %s", err)
	}
	defer r.client.Disconnect(context.Background())
	logger.
		WithField("type", "mongo_get").
		Debug("Prepare the filter")
	options := options.Find()
	if len(sort) > 0 {
		sortOptions := make(map[string]interface{})
		err = json.Unmarshal([]byte(sort), &sortOptions)
		if err != nil {
			logger.
				WithField("type", "mongo_get").
				Warnf("failed on parsing the sort options: %s", sort)
		}
		options.SetSort(sortOptions)
	}
	limit := int64(1000)
	skip := int64(0)
	options.SetLimit(limit)
	more := true
	var cur *mongo.Cursor
	var allResults bson.A
	logger.
		WithField("type", "mongo_get").
		Debug("Start reading the loop")
	for more {
		logger.
			WithField("type", "mongo_get").
			Debugf("Skip: %d, limit: %d", skip, limit)
		options.SetSkip(skip * limit)
		cur, err = r.collection.Find(context.TODO(), r.filter, options)
		if err != nil {
			return "", fmt.Errorf("find: %s", err)
		}
		more = false
		for cur.Next(context.TODO()) {
			more = true
			var result bson.M
			err := cur.Decode(&result)
			if err != nil {
				return "", fmt.Errorf("decode: %s", err)
			}
			allResults = append(allResults, result)
		}
		skip++
	}
	if err := cur.Err(); err != nil {
		return "", fmt.Errorf("cursor: %s", err)
	}
	cur.Close(context.TODO())
	dataBytes, err := json.Marshal(allResults)
	if err != nil {
		return "", fmt.Errorf("marshal: %s", err)
	}
	return string(dataBytes), nil
}

// MongoAggregate gets data from mongo db
func MongoAggregate(dsn, pipeline string) (string, error) {
	r, err := mongoConnect(dsn, "{}", "", pipeline)
	if err != nil {
		return "", fmt.Errorf("connect: %s", err)
	}
	defer r.client.Disconnect(context.Background())
	more := true
	var cur *mongo.Cursor
	var allResults bson.A
	i := 0
	logger.
		WithField("type", "mongo_get").
		Info("Start reading the loop")
	for more {
		cur, err = r.collection.Aggregate(context.TODO(), r.documents)
		if err != nil {
			return "", fmt.Errorf("aggregate: %s", err)
		}
		more = false
		for cur.Next(context.TODO()) {
			var result bson.M
			err := cur.Decode(&result)
			if err != nil {
				return "", fmt.Errorf("decode: %s", err)
			}
			i++
			allResults = append(allResults, result)
		}
	}
	if err := cur.Err(); err != nil {
		return "", fmt.Errorf("cursor: %s", err)
	}
	cur.Close(context.TODO())
	dataBytes, err := json.Marshal(allResults)
	if err != nil {
		return "", fmt.Errorf("marshal: %s", err)
	}
	return string(dataBytes), nil
}

// MongoUpdate updates data in mongo db
func MongoUpdate(dsn, filter, update string) (int64, int64, error) {
	r, err := mongoConnect(dsn, filter, update, "")
	if err != nil {
		return 0, 0, fmt.Errorf("connect: %s", err)
	}
	defer r.client.Disconnect(context.Background())
	updateResult, err := r.collection.UpdateMany(context.TODO(), r.filter, r.update)
	if err != nil {
		return 0, 0, err
	}
	return updateResult.MatchedCount, updateResult.ModifiedCount, nil
}

// MongoDelete deletes the data from the mongo db
func MongoDelete(dsn, filter string) (int64, error) {
	r, err := mongoConnect(dsn, filter, "", "")
	if err != nil {
		return 0, fmt.Errorf("connect: %s", err)
	}
	defer r.client.Disconnect(context.Background())
	deleteResult, err := r.collection.DeleteMany(context.TODO(), r.filter)
	if err != nil {
		return 0, err
	}
	return deleteResult.DeletedCount, nil
}

// MongoInsert inserts the data into the mongo db
func MongoInsert(dsn, documents string) (int64, error) {
	r, err := mongoConnect(dsn, "{}", "", documents)
	if err != nil {
		return 0, fmt.Errorf("connect: %s", err)
	}
	defer r.client.Disconnect(context.Background())
	insertResult, err := r.collection.InsertMany(context.TODO(), r.documents)
	if err != nil {
		return 0, err
	}
	return int64(len(insertResult.InsertedIDs)), nil
}
