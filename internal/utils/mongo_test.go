package utils

import "testing"

func TestUtils_CheckMongo(t *testing.T) {
	dsn := "mongodb://localhost:27017/?db=test&collection=test"
	deleteFilter := `{"id": {"$in": ["1","2"]}}`
	filter := "{}"
	expected := int64(2)
	inserted, err := MongoInsert(dsn, `[{"id":"1","name":"one"}, {"id":"2","name":"two"}]`)
	if err != nil {
		t.Fatal(err)
	}
	if inserted != expected {
		t.Fatalf("Inserted count (%v) is different from expected (%v)", inserted, expected)
	}
	deleted, err := MongoDelete(dsn, deleteFilter)
	if err != nil {
		t.Fatal(err)
	}
	if deleted != inserted {
		t.Fatalf("Deleted count (%v) is different from inserted (%v)", deleted, inserted)
	}
	found, err := MongoGet(dsn, filter, "")
	if err != nil {
		t.Fatal(err)
	}
	if found != "null" {
		t.Fatalf("Found: (%v), but shouldn't", found)
	}
}
