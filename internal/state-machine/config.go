package statemachine

// StateConfig describes a definite states machine configuration
type StateConfig struct {
	Name     string            `mapstructure:"name"`
	Scenario string            `mapstructure:"scenario"`
	Targets  map[string]string `mapstructure:"targets"`
	Initial  bool              `mapstructure:"initial"`
	Rerun    bool              `mapstructure:"rerun"`
}
