package statemachine

import (
	"fmt"
	"time"

	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/task-executor/internal/executor"
)

type StateMachine struct {
	cfg              []StateConfig
	CurrentState     string
	stateSwitched    bool
	ex               *executor.Executor
	scenarioRunCount int
}

const (
	stateKey        = "state"
	currentStateKey = "current_state"
)

var (
	signaled = false
)

func NewStateMachine(cfg []StateConfig, ex *executor.Executor) *StateMachine {
	sm := new(StateMachine)
	sm.ex = ex
	sm.cfg = cfg
	sm.Reset()
	return sm
}

func (sm *StateMachine) Reset() {
	logger.Debugf("Reset the state machine")
	for _, state := range sm.cfg {
		if state.Initial {
			sm.CurrentState = state.Name
			sm.scenarioRunCount = 0
		}
	}
	if len(sm.CurrentState) == 0 && len(sm.cfg) > 0 {
		sm.CurrentState = sm.cfg[0].Name
	}
}
func (sm *StateMachine) Stop() {
	signaled = true
}
func (sm *StateMachine) Run() {
	canContinue := true
	for canContinue && !signaled {
		sm.ex.SetContextValue(currentStateKey, sm.CurrentState)
		var scenario executor.Scenario
		var state StateConfig
		for _, stt := range sm.cfg {
			if stt.Name == sm.CurrentState {
				state = stt
				break
			}
		}
		sm.stateSwitched = false
		ok := false
		scenario, ok = sm.ex.PredefinedScenario[state.Scenario]
		if !ok {
			logger.
				WithField(stateKey, state.Name).
				Warnf("Scenario '%s' was not found", state.Scenario)
			return
		}
		if sm.scenarioRunCount == 0 || state.Rerun {
			sm.ex.RunScenario(scenario, fmt.Sprintf("%d", time.Now().UnixNano()))
			sm.scenarioRunCount++
		}
		canContinue = false
		for stateName, condition := range state.Targets {
			logger.
				WithField(stateKey, state.Name).
				Debugf("Check the next state condition '%s'", condition)
			if sm.ex.TrueCondition(condition) {
				logger.
					WithField(stateKey, state.Name).
					Debugf("The next state is '%s'", stateName)
				sm.CurrentState = stateName
				sm.scenarioRunCount = 0
				canContinue = true
				break
			}
		}
	}
}
