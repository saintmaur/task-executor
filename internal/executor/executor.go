package executor

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"reflect"
	"strings"
	"sync"
	"sync/atomic"
	"text/template"
	"time"

	"github.com/Masterminds/sprig/v3"
	"github.com/chromedp/chromedp"
	"github.com/krayzpipes/cronticker/cronticker"

	// "github.com/olekukonko/tablewriter"
	"github.com/panjf2000/ants/v2"
	"github.com/spf13/viper"
	"gitlab.com/saintmaur/lib/amqp"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/lib/stat"
	extUtils "gitlab.com/saintmaur/lib/utils"
	"gitlab.com/saintmaur/task-executor/internal/utils"
)

type controlledRoutine struct {
	name string
	c    chan bool
}

const (
	resultKey           = "result"
	currentTaskErrorKey = "current_task_error"
	executorKey         = "executor"
	currentCycleKey     = "current_cycle"
	taskKey             = "task"
	typeKey             = "type"
	idKey               = "id"
	elapsedKey          = "elapsed"
	defaultKey          = "default"
	defaultThreads      = 1000
)

// SubsriberFunc is a type alias
type SubsriberFunc func()

type Defaults struct {
	timeout     time.Duration
	onTimeout   string
	onError     string
	onStart     string
	onFinish    string
	stopOnError string
}

// Executor is an executor type
type Executor struct {
	tickerChan         <-chan time.Time
	skipStartupRun     bool
	stopChan           chan bool
	StatChan           chan stat.Rec
	Stopped            bool
	stopping           bool
	Scenario           Scenario
	Before             Scenario
	After              Scenario
	PredefinedScenario map[string]Scenario
	Name               string
	valuedCtx          *sync.Map
	mutex              sync.Mutex
	wg                 sync.WaitGroup
	grCount            int32
	cycles             int
	currentCycle       int
	defaults           Defaults
	repeated           []*controlledRoutine
	configParser       *viper.Viper
	funcMap            map[string]interface{}
	parent             *Executor
	tmpl               *template.Template
	tPool              *ants.Pool
	currentTaskID      string
}

func cloneSyncMap(src *sync.Map) *sync.Map {
	syncMap := new(sync.Map)
	f := func(key any, value any) bool {
		syncMap.Store(key, value)
		return true
	}
	src.Range(f)
	return syncMap
}

func (e *Executor) Context() map[string]interface{} {
	ctx := make(map[string]interface{})
	f := func(key any, value any) bool {
		ctx[key.(string)] = value
		return true
	}
	e.valuedCtx.Range(f)
	return ctx
}
func (e *Executor) SetParent(parent *Executor) {
	if e.parent == nil {
		e.parent = parent
	}
}
func (e *Executor) initThreadPool(threads int) {
	if threads <= 0 {
		threads = defaultThreads
	}
	logger.Infof("Init a pool with %d thread(s) limit", threads)
	e.tPool, _ = ants.NewPool(threads)
}

func (e *Executor) clone(name string) *Executor {
	newE := new(Executor)
	var err error
	newE.tmpl, err = e.tmpl.Clone()
	if err != nil {
		logger.Errorf("Failed on cloning the template: %s", err)
	}
	if len(name) > 0 {
		newE.Name, err = e.ProcessTemplate(name)
	}
	if err != nil {
		newE.Name = fmt.Sprintf("%s_cloned", e.Name)
	}
	newE.currentTaskID = e.currentTaskID
	newE.valuedCtx = cloneSyncMap(e.valuedCtx)
	newE.SetContextValue(fmt.Sprintf("%s_name", executorKey), newE.Name)
	newE.parent = e
	newE.PredefinedScenario = make(map[string]Scenario)
	for key, value := range e.PredefinedScenario {
		newE.PredefinedScenario[key] = value
	}
	newE.defaults = e.defaults
	newE.tPool = e.tPool
	newE.StatChan = e.StatChan
	newE.configParser = e.configParser
	newE.Scenario = e.Scenario
	newE.repeated = e.repeated
	newE.funcMap = make(map[string]interface{})
	newE.addFuncMap()
	return newE
}

// RegisterTask stores a new task globally
func RegisterTask(key string, f ActionFunc) error {
	_, alreadyHas := actionTypes[key]
	if alreadyHas {
		return fmt.Errorf("an action named '%s' already exists", key)
	}
	actionTypes[key] = ActionType{
		Fields: map[string]string{},
		Build: func() Action {
			return &CustomAction{f: f}
		},
	}
	return nil
}

// NewExecutor is a way to get a new executor
func NewExecutor(executorConfig ExecutorConfig,
	globalVarsConfig VariablesConfig,
	globalPredefinedConfig map[string]Scenario,
	statChan stat.Chan,
) *Executor {
	e := new(Executor)
	if e.init(executorConfig, globalVarsConfig, globalPredefinedConfig, statChan) {
		e.Stopped = false
		return e
	}
	e.Stopped = true
	return nil
}

func (e *Executor) initVariables(vars VariablesConfig) {
	for _, varConfig := range vars {
		value, err := utils.Prepare(varConfig.Value, varConfig.Type)
		if err == nil {
			e.SetContextValue(varConfig.Name, value)
		} else {
			logger.Warnf("Failed to prepare a variable value (%s:%s)", varConfig.Name, varConfig.Type)
		}
	}
}

func (e *Executor) initTicker(cronExpr string, dflt time.Duration) {
	getTickerChan := func(d time.Duration) <-chan time.Time {
		ticker := time.NewTicker(d)
		return ticker.C
	}
	if duration, err := extUtils.IsTimeDuration(cronExpr); err == nil {
		e.tickerChan = getTickerChan(duration)
	} else {
		ticker, err := cronticker.NewTicker(cronExpr)
		if err == nil {
			e.tickerChan = ticker.C
		} else {
			e.tickerChan = getTickerChan(dflt)
		}
	}
}

func (e *Executor) initGlobalPredefined(scenarios map[string]Scenario) {
	for key, scenario := range scenarios {
		e.tryAddScenario(key, scenario)
	}
}

func (e *Executor) ParseTimeout(tm string, def time.Duration) time.Duration {
	d, err := time.ParseDuration(tm)
	if err != nil {
		logger.
			WithField(executorKey, e.Name).
			Debugf("The timeout has been set to inappropriate value: '%s'. Fallback to default %s.", tm, def)
		d = def
	}
	return d
}

func (e *Executor) init(
	executorConfig ExecutorConfig,
	globalVarsConfig VariablesConfig,
	globalPredefinedConfig map[string]Scenario,
	statChan chan stat.Rec,
) bool {
	e.tmpl = template.New(e.Name).Funcs(sprig.FuncMap())
	e.PredefinedScenario = make(map[string]Scenario)
	e.initContext()
	e.funcMap = make(map[string]interface{})
	e.stopChan = make(chan bool, 2)
	e.StatChan = statChan
	e.Name = executorConfig.Name
	e.cycles = executorConfig.Cycles
	e.currentCycle = 1
	e.skipStartupRun = executorConfig.SkipStartupRun
	e.SetContextValue(currentCycleKey, e.currentCycle)
	e.initTicker(executorConfig.Period, 1*time.Minute)
	e.configParser = viper.New()
	e.Scenario = executorConfig.Scenario
	e.Before = executorConfig.Before
	e.After = executorConfig.After
	if executorConfig.Predefined != nil {
		for name, sc := range executorConfig.Predefined {
			e.PredefinedScenario[name] = sc
		}
	}
	e.initGlobalPredefined(globalPredefinedConfig)
	e.addFuncMap()
	e.initDefaults(executorConfig)
	e.initThreadPool(executorConfig.Threads)
	e.initVariables(executorConfig.Variables)
	e.initVariables(globalVarsConfig)
	e.SetContextValue(fmt.Sprintf("%s_name", executorKey), e.Name)
	return e.validateScenario(e.Scenario) &&
		e.validateScenario(e.Before) &&
		e.validateScenario(e.After)
}

func (e *Executor) addFuncMap() {
	for name, scenario := range e.PredefinedScenario {
		if !e.validateScenario(scenario) {
			logger.
				WithField(executorKey, e.Name).
				Warnf("Failed on validating '%s' scenario. Remove it.", name)
			delete(e.PredefinedScenario, name)
			continue
		}
		e.tryAddAppendFuncMap(name, scenario)
	}
}

func (e *Executor) initDefaults(executorConfig ExecutorConfig) {
	e.defaults.timeout = e.ParseTimeout(executorConfig.Defaults.Timeout, 10*time.Second)
	e.defaults.onError = executorConfig.Defaults.OnError
	e.defaults.onStart = executorConfig.Defaults.OnStart
	e.defaults.onFinish = executorConfig.Defaults.OnFinish
	e.defaults.stopOnError = executorConfig.Defaults.StopOnError
	e.defaults.onTimeout = executorConfig.Defaults.OnTimeout
}

func (e *Executor) tryAddAppendFuncMap(name string, scenario Scenario) {
	if extUtils.GoodTemplateFunctionName(name) {
		temp_scenario := scenario
		temp_name := name
		e.funcMap[temp_name] = func(arguments ...interface{}) (interface{}, error) {
			e.SetContextValue(fmt.Sprintf("%s_arguments", temp_name), arguments)
			e.RunScenario(temp_scenario, e.currentTaskID)
			resultName := fmt.Sprintf("%s_return_value", temp_name)
			result := e.GetContextValue(resultName)
			e.DeleteContextValue(resultName)
			return result, nil
		}
		e.tmpl = e.tmpl.Funcs(e.funcMap)
	}
}
func (e *Executor) validateScenario(scenario Scenario) bool {
	for _, task := range scenario {
		err := validate(task)
		if err != nil {
			logger.
				WithField(executorKey, e.Name).Warnf("%s", err)
			return false
		}
	}
	return true
}

// Stop does the thing
func (e *Executor) Stop() {
	if !e.stopping && !e.Stopped {
		e.stopping = true
		logger.
			WithField(executorKey, e.Name).
			Info("A stop signal has come to the executor. Wait for the running actions.")
		e.stopChan <- true
		e.StopRepeated()
		logger.
			WithField(executorKey, e.Name).
			Debugf("Goroutines count in work: %d", atomic.LoadInt32(&e.grCount))
		e.wg.Wait()
		e.Stopped = true
		e.RunScenario(e.After, "0")
		logger.
			WithField(executorKey, e.Name).
			Infof("Stopped gracefully")
	}
}

func (e *Executor) wgAdd() {
	atomic.AddInt32(&e.grCount, 1)
	e.wg.Add(1)
}

func (e *Executor) wgDone() {
	cnt := atomic.LoadInt32(&e.grCount)
	if cnt > 0 {
		atomic.AddInt32(&e.grCount, -1)
		e.wg.Done()
	}
}

// Start starts the main Executor loop
func (e *Executor) Start() {
	e.RunScenario(e.Before, "0")
	if !e.skipStartupRun {
		e.wgAdd()
		e.Run()
	}
	e.wgAdd()
	go func() {
		for {
			select {
			case <-e.stopChan:
				logger.
					WithField(executorKey, e.Name).
					Info("Exit the main loop")
				e.wgDone()
				return
			case <-e.tickerChan:
				if e.cycles > 0 && e.currentCycle < e.cycles || e.cycles == 0 {
					e.wgAdd()
					go func() {
						e.Run()
						e.wgDone()
					}()
					e.currentCycle++
					e.SetContextValue(currentCycleKey, e.currentCycle)
				}
				// case <-time.After(time.Second):
				// 	table := initTableWriter([]string{"pool size", "pool running", "pool waiting"})
				// 	table.Append([]string{
				// 		fmt.Sprintf("%d", e.tPool.Cap()),
				// 		fmt.Sprintf("%d", e.tPool.Running()),
				// 		fmt.Sprintf("%d", e.tPool.Waiting()),
				// 	})
				// 	table.Render()
				// 	table.ClearRows()
			}
		}
	}()
}

// func initTableWriter(headers []string) *tablewriter.Table {
// 	table := tablewriter.NewWriter(os.Stdout)
// 	table.SetHeader(headers)
// 	table.SetAutoFormatHeaders(false)
// 	table.SetBorder(true)
// 	return table
// }

// Run the scenario
func (e *Executor) Run() {
	started := time.Now()
	id := fmt.Sprintf("%d", started.UnixNano())
	logger.
		WithField(executorKey, e.Name).
		WithField(idKey, id).
		Debug("Start the scenario")
	e.RunScenario(e.Scenario, id)
	logger.
		WithField(executorKey, e.Name).
		WithField(idKey, id).
		WithField(elapsedKey, time.Since(started).String()).
		Debug("The scenario run has finished")
	e.wgDone()
	if e.currentCycle == e.cycles {
		e.Stop()
	}
}

// RunScenario does the thing
func (e *Executor) RunScenario(scenario Scenario, id string) {
	e.currentTaskID = id
	stop := false
	for _, task := range scenario {
		taskName, err := e.ProcessTemplate(task.Name)
		if err != nil {
			taskName = task.Name
		}
		panicRecover := func() {
			if r := recover(); r != nil {
				logger.
					WithField(executorKey, e.Name).
					WithField(taskKey, taskName).
					WithField(typeKey, task.Type).
					Warnf("Recovered after panic in the task: %s", r)
			}
		}
		defer panicRecover()
		if len(task.SkipCond) > 0 && e.TrueCondition(task.SkipCond) {
			logger.
				WithField(executorKey, e.Name).
				WithField(idKey, id).
				WithField(taskKey, taskName).
				WithField(typeKey, task.Type).
				Infof("The task has been skipped")
			continue
		}
		logger.
			WithField(executorKey, e.Name).
			WithField(idKey, id).
			WithField(taskKey, taskName).
			WithField(typeKey, task.Type).
			Debugf("Start the task")
		action := getAction(task.Type)
		if action == nil {
			logger.
				WithField(executorKey, e.Name).
				WithField(idKey, id).
				WithField(taskKey, taskName).
				WithField(typeKey, task.Type).
				Errorf("Skipping the unsupported action type")
			continue
		}
		var duration time.Duration
		timeout, err := e.handleTemplateError(task.Timeout, "timeout")
		if err == nil {
			duration, err = time.ParseDuration(timeout)
			if err != nil {
				duration = e.defaults.timeout
			}
			e.SetContextValue(fmt.Sprintf("%s_timeout", taskName), duration)
		}
		e.DeleteContextValue(fmt.Sprintf("%s_%s", taskName, resultKey))
		start := time.Now()
		resultC := make(chan error)
		e.runOnScenario(task.OnStart, e.defaults.onStart, id)
		go func(resultC chan error) {
			if err := e.ProcessTaskFields(&task); err != nil {
				resultC <- err
			}
			defer panicRecover()
			resultC <- action.Run(e, task)
		}(resultC)
		select {
		case <-time.After(duration):
			onTimeout := task.OnTimeout
			if len(onTimeout) == 0 {
				onTimeout = e.defaults.onTimeout
			}
			onTimeoutScenario, exists := e.PredefinedScenario[onTimeout]
			if exists {
				e.RunScenario(onTimeoutScenario, id)
			}
			logger.
				WithField(executorKey, e.Name).
				WithField(idKey, id).
				WithField(taskKey, taskName).
				WithField(typeKey, task.Type).
				Warnf("The task has timed out (%s)", duration)
		case result := <-resultC:
			if result != nil || (len(task.ErrorIf) > 0 && e.TrueCondition(task.ErrorIf)) {
				result = e.prepareResult(result, task, taskName)
				e.SetContextValue(currentTaskErrorKey, fmt.Sprintf("%s", result))
				e.SetContextValue(fmt.Sprintf("%s_%s", taskName, resultKey), result)
				logger.
					WithField(executorKey, e.Name).
					WithField(idKey, id).
					WithField(taskKey, taskName).
					WithField(typeKey, task.Type).
					Warnf("The task has finished with error: %s", result)
				e.runOnScenario(task.OnError, e.defaults.onError, id)
				stopOnError := task.StopOnError
				if len(stopOnError) == 0 {
					stopOnError = e.defaults.stopOnError
				}
				if len(stopOnError) > 0 && e.TrueCondition(stopOnError) {
					stop = true
				}
			} else {
				e.SetContextValue(currentTaskErrorKey, "")
			}
		}
		logger.
			WithField(executorKey, e.Name).
			WithField(idKey, id).
			WithField("task", taskName).
			WithField(typeKey, task.Type).
			WithField(elapsedKey, time.Since(start).String()).
			Debug("The task run has finished")
		e.runOnScenario(task.OnFinish, e.defaults.onFinish, id)
		e.saveTiming(fmt.Sprintf("%s_%s", e.Name, taskName), start)
		if stop {
			logger.
				WithField(executorKey, e.Name).
				WithField(idKey, id).
				WithField(taskKey, taskName).
				WithField(typeKey, task.Type).
				Warnf("No further processing is allowed")
			return
		}
		if e.Stopped && id != "0" {
			return
		}
	}
}

func (e *Executor) runOnScenario(name, defaultName, id string) {
	scenarioName := name
	if len(scenarioName) == 0 {
		scenarioName = defaultName
	}
	scenario, exists := e.PredefinedScenario[scenarioName]
	if exists {
		e.RunScenario(scenario, id)
	}
}

func (e *Executor) prepareResult(input error, task Task, taskName string) error {
	var result error
	e.SetContextValue(currentTaskErrorKey, fmt.Sprintf("%s", input))
	e.SetContextValue(fmt.Sprintf("%s_%s", taskName, resultKey), input)
	if len(task.ErrorMessage) > 0 {
		msg, _ := e.ProcessTemplate(task.ErrorMessage)
		result = errors.New(msg)
	} else {
		result = input
		if result == nil {
			result = fmt.Errorf("conditional error: %s", task.ErrorIf)
		}
	}
	return result
}

// TrueCondition executes the template and returns a bool value
func (e *Executor) TrueCondition(condition string) bool {
	const (
		trueVal = "1"
	)
	var res string
	var err error
	if len(condition) == 0 {
		logger.
			WithField(executorKey, e.Name).
			Warnf("The template is absent")
		return false
	}
	pattern := fmt.Sprintf("{{if %s}}1{{else}}0{{end}}", condition)
	res, err = e.ProcessTemplate(pattern)
	if err != nil {
		logger.
			WithField(executorKey, e.Name).
			Warnf("Failed to process the template: %s", err)
		return false
	}
	return res == trueVal
}

// GetContextValue retrieves the context value by key
func (e *Executor) GetContextValue(key string) interface{} {
	processed, err := e.ProcessTemplate(key)
	if err == nil {
		key = processed
	}
	v, _ := e.valuedCtx.Load(key)
	return v
}

// SetContextValue stores a value to the context
func (e *Executor) SetContextValue(key string, value interface{}) {
	if len(key) > 0 {
		processed, err := e.ProcessTemplate(key)
		if err == nil {
			key = processed
		}
		e.valuedCtx.Store(key, value)
	}
}

// DeleteContextValue removes a value from the context
func (e *Executor) DeleteContextValue(key string) {
	processed, err := e.ProcessTemplate(key)
	if err == nil {
		key = processed
	}
	e.valuedCtx.Delete(key)
}

// ProcessTemplate evaluates the template with context values
func (e *Executor) ProcessTemplate(dataStr string) (string, error) {
	result := ""
	tmpl, err := e.tmpl.Clone()
	if err != nil {
		logger.
			WithField(executorKey, e.Name).
			Warnf("Failed to clone the prepared template: '%s'", err)
		return result, err
	}
	_, err = tmpl.Parse(dataStr)
	if err != nil {
		logger.
			WithField(executorKey, e.Name).
			Warnf("Failed to parse the string: '%s'; input: '%s'", err, dataStr)
		return result, err
	}
	var buff bytes.Buffer
	ctx := e.Context()
	err = tmpl.Execute(&buff, ctx)
	return buff.String(), err
}

func (e *Executor) saveTiming(key string, tm time.Time) {
	e.SetContextValue(fmt.Sprintf("%s_duration", key), time.Since(tm))
}

func (e *Executor) parseAndAddHeaders(r *http.Request, headersStr string) error {
	if len(headersStr) == 0 {
		return nil
	}
	var headersMap map[string]string
	extra, err := e.handleTemplateError(headersStr, "extra")
	if err != nil {
		return err
	}
	err = json.Unmarshal([]byte(extra), &headersMap)
	if err != nil {
		return err
	}
	for headerName, headerValue := range headersMap {
		r.Header.Add(headerName, headerValue)
	}
	return nil
}

func (e *Executor) stopRepeat(index int) {
	reason := fmt.Sprintf("stopRepeat[%d]", index)
	e.lock(reason)
	defer e.unlock(reason)
	logger.
		WithField(executorKey, e.Name).
		Debugf("Try to stop ==| %s |==", e.repeated[index].name)
	e.repeated[index].c <- true
	logger.
		WithField(executorKey, e.Name).
		Debugf("Successfully stopped ==| %s |==", e.repeated[index].name)
	if index+1 >= len(e.repeated) {
		e.repeated = e.repeated[:index]
	} else {
		e.repeated = append(e.repeated[:index], e.repeated[index+1:]...)
	}
}
func (e *Executor) StopRepeat(name string) {
	if e.parent != nil {
		e.parent.StopRepeat(name)
	} else {
		reason := fmt.Sprintf("StopRepeat[%s]", name)
		e.lock(reason)
		index := -1
		for i, repeated := range e.repeated {
			if repeated.name == name {
				index = i
				break
			}
		}
		e.unlock(reason)
		if index > -1 {
			e.stopRepeat(index)
		}
	}
}

func (e *Executor) StopRepeated() {
	for i := len(e.repeated) - 1; i >= 0; i-- {
		e.stopRepeat(i)
	}
}

func (e *Executor) handleTemplateError(tmpl, key string) (string, error) {
	result, err := e.ProcessTemplate(tmpl)
	if err != nil {
		err = fmt.Errorf("failed on processing the template in the '%s' field: %s", key, err)
	}
	return result, err
}

func (e *Executor) getBrowserContext(name string) (context.Context, context.CancelFunc, error) {
	name, err := e.handleTemplateError(name, "name")
	if err != nil {
		return nil, nil, err
	}
	ctxValue := e.GetContextValue(name)
	if ctxValue == nil {
		return nil, nil, fmt.Errorf("browser context ('%s') is nil", name)
	}
	generalCtx := ctxValue.(context.Context)
	runtimeCtx, cancel := context.WithTimeout(generalCtx, time.Minute)
	return runtimeCtx, cancel, nil
}

func (e *Executor) sendBrowserKeys(task Task, input string) error {
	runtimeCtx, cancel, err := e.getBrowserContext(task.Extra)
	if err != nil {
		return fmt.Errorf("get the browser context; error: %s", err)
	}
	defer cancel()
	path, err := e.handleTemplateError(task.Target, "target")
	if err != nil {
		return err
	}
	err = chromedp.Run(runtimeCtx, chromedp.SendKeys(path, input, chromedp.ByQuery))
	if err != nil {
		return err
	}
	return nil
}

func (e *Executor) initContext() {
	if e.valuedCtx == nil {
		e.valuedCtx = new(sync.Map)
	}
	for _, env := range os.Environ() {
		parts := strings.Split(env, "=")
		e.valuedCtx.Store(parts[0], strings.Join(parts[1:], "="))
	}
}

func (e *Executor) tryAddScenario(key string, scenario Scenario) {
	_, has := e.PredefinedScenario[key]
	if has {
		logger.Infof("Replacing the existing scenario named [%s]", key)
	}
	logger.
		WithField(executorKey, e.Name).
		Debugf("Add a new scenario: %s", key)
	reason := "add new scenario"
	e.lock(reason)
	e.PredefinedScenario[key] = scenario
	e.unlock(reason)
}

func (e *Executor) addRepeated(name string, stopC chan bool) bool {
	if e.parent != nil {
		return e.parent.addRepeated(name, stopC)
	} else {
		reason := "add repeated"
		e.lock(reason)
		defer e.unlock(reason)
		for _, routine := range e.repeated {
			if routine.name == name {
				return false
			}
		}
		logger.
			WithField(executorKey, e.Name).
			Debugf("Add a new stop channel: ==| %s |==", name)
		e.repeated = append(e.repeated, &controlledRoutine{
			name: name,
			c:    stopC,
		})
		return true
	}
}

func (e *Executor) lock(reason string) {
	logger.
		WithField(executorKey, e.Name).
		Debugf("Lock: %s", reason)
	e.mutex.Lock()
}

func (e *Executor) unlock(reason string) {
	logger.
		WithField(executorKey, e.Name).
		Debugf("Unlock: %s", reason)
	e.mutex.Unlock()
}

func (e *Executor) amqpConnect(url string, task Task) error {
	connection := e.GetContextValue(url)
	if connection == nil {
		stopC := make(chan bool)
		connection, err := amqp.Connect(url, stopC, func() {
			e.wgDone()
		})
		if err != nil {
			return err
		}
		if !e.addRepeated(fmt.Sprintf("%s_amqp_connection", task.Name), stopC) {
			return fmt.Errorf("tried to add a stop channel with non-unique name which is a potential goroutine leak")
		}
		e.wgAdd()
		e.SetContextValue(url, connection)
	}
	return nil
}

func GetActionTypes() map[string]ActionType {
	return actionTypes
}

func (e *Executor) ProcessTaskFields(task *Task) error {
	v := reflect.ValueOf(task).Elem()
	t := v.Type()
	for i := 0; i < v.NumField(); i++ {
		field := t.Field(i)
		value := v.Field(i)
		processedValue, err := e.handleTemplateError(value.String(), field.Name)
		if err != nil {
			return err
		}
		if value.CanSet() {
			value.SetString(processedValue)
		}
	}
	return nil
}
