package executor

type ActionType struct {
	Fields map[string]string
	Build  func() Action
}

var actionTypes = map[string]ActionType{
	"http": {
		Fields: map[string]string{
			"name":   ".+",
			"method": ".+",
			"target": ".+",
		},
		Build: func() Action {
			return new(HTTPAction)
		},
	},
	"stat": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"labels": ".+",
		},
		Build: func() Action {
			return new(StatAction)
		},
	},
	"json_get": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".+",
			"data":   ".+",
		},
		Build: func() Action {
			return new(JSONGetAction)
		},
	},
	"json_set": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".+",
			"data":   ".*",
			"extra":  ".+",
		},
		Build: func() Action {
			return new(JSONSetAction)
		},
	},
	"json_delete": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".+",
			"data":   ".*",
		},
		Build: func() Action {
			return new(JSONDeleteAction)
		},
	},
	"print": {
		Fields: map[string]string{
			"data": ".+",
		},
		Build: func() Action {
			return new(PrintAction)
		},
	},
	"set": {
		Fields: map[string]string{
			"name":  ".+",
			"data":  ".*",
			"extra": "(bool|string|int|float|)",
		},
		Build: func() Action {
			return new(SetAction)
		},
	},
	"unset": {
		Fields: map[string]string{
			"name": ".+",
		},
		Build: func() Action {
			return new(UnsetAction)
		},
	},
	"shell": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".*",
			"target": ".+",
		},
		Build: func() Action {
			return new(ShellAction)
		},
	},
	"remote_shell": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"target": ".+",
		},
		Build: func() Action {
			return new(RemoteShellAction)
		},
	},
	"file_read": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".+",
		},
		Build: func() Action {
			return new(ReadFileAction)
		},
	},
	"file_write": {
		Fields: map[string]string{
			"name":   ".*",
			"target": ".+",
			"data":   ".+",
			"extra":  ".*",
		},
		Build: func() Action {
			return new(WriteFileAction)
		},
	},
	"loop": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"target": ".+",
		},
		Build: func() Action {
			return new(LoopAction)
		},
	},
	"regexp": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"target": ".+",
		},
		Build: func() Action {
			return new(RegexpAction)
		},
	},
	"db_select": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"target": ".+",
			"extra":  ".+",
		},
		Build: func() Action {
			return new(DBSelectAction)
		},
	},
	"db_exec": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".+",
			"data":   ".+",
			"extra":  ".+",
		},
		Build: func() Action {
			return new(DBExecAction)
		},
	},
	"wait": {
		Fields: map[string]string{
			"name": ".+",
			"data": ".+",
		},
		Build: func() Action {
			return new(WaitAction)
		},
	},
	"run": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".+",
		},
		Build: func() Action {
			return new(RunAction)
		},
	},
	"mongo_get": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"target": ".+",
			"extra":  ".*",
		},
		Build: func() Action {
			return new(MongoGetAction)
		},
	},
	"mongo_aggregate": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"target": ".+",
			"extra":  ".*",
		},
		Build: func() Action {
			return new(MongoAggregateAction)
		},
	},
	"mongo_update": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"target": ".+",
			"extra":  ".*",
		},
		Build: func() Action {
			return new(MongoUpdateAction)
		},
	},
	"mongo_insert": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"target": ".+",
		},
		Build: func() Action {
			return new(MongoInsertAction)
		},
	},
	"mongo_delete": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"target": ".+",
		},
		Build: func() Action {
			return new(MongoDeleteAction)
		},
	},
	"intern": {
		Fields: map[string]string{
			"name": ".+",
			"data": ".+",
		},
		Build: func() Action {
			return new(InternAction)
		},
	},
	"replace": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"target": ".+",
			"extra":  ".*",
		},
		Build: func() Action {
			return new(ReplaceAction)
		},
	},
	"replace_regexp": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"target": ".+",
			"extra":  ".*",
		},
		Build: func() Action {
			return new(ReplaceRegexpAction)
		},
	},
	"csv": {
		Fields: map[string]string{
			"name":  ".+",
			"data":  ".+",
			"extra": ".*",
		},
		Build: func() Action {
			return new(CSVParseAction)
		},
	},
	"sign": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"method": ".+",
			"extra":  ".+",
		},
		Build: func() Action {
			return new(SignAction)
		},
	},
	"break": {
		Fields: map[string]string{
			"name": ".+",
		},
		Build: func() Action {
			return new(BreakAction)
		},
	},
	"time": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".*",
			"format": ".*",
		},
		Build: func() Action {
			return new(TimeAction)
		},
	},
	"time_format": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"format": ".+",
		},
		Build: func() Action {
			return new(TimeFormatAction)
		},
	},
	"time_shift": {
		Fields: map[string]string{
			"name":     ".+",
			"data":     ".+",
			"duration": ".+",
		},
		Build: func() Action {
			return new(TimeShiftAction)
		},
	},
	"localize": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"target": ".+",
			"extra":  ".+",
		},
		Build: func() Action {
			return new(LocalizeAction)
		},
	},
	"template": {
		Fields: map[string]string{
			"name": ".+",
			"data": ".+",
		},
		Build: func() Action {
			return new(TemplateAction)
		},
	},
	"pipe": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".+",
			"data":   ".*",
		},
		Build: func() Action {
			return new(PipeAction)
		},
	},
	"websocket": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".+",
			"data":   ".+",
		},
		Build: func() Action {
			return new(WebsocketAction)
		},
	},
	"tcp": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".+:.+",
			"data":   ".+",
		},
		Build: func() Action {
			return new(TCPAction)
		},
	},
	"udp": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".+:.+",
			"data":   ".+",
		},
		Build: func() Action {
			return new(UDPAction)
		},
	},
	"encode": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"method": "(base32|base58|base64|url_query|url_path|hex|unicode)",
		},
		Build: func() Action {
			return new(EncodeAction)
		},
	},
	"decode": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"method": "(base32|base58|base64|url_query|url_path|hex|unicode)",
		},
		Build: func() Action {
			return new(DecodeAction)
		},
	},
	"while": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"target": ".+",
		},
		Build: func() Action {
			return new(WhileAction)
		},
	},
	"set_map_value": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"target": ".+",
		},
		Build: func() Action {
			return new(SetMapValueAction)
		},
	},
	"inject": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".*",
			"target": ".*",
			"format": "(json|yaml|yml)",
		},
		Build: func() Action {
			return new(InjectAction)
		},
	},
	"compare_images": {
		Fields: map[string]string{
			"data":   ".+",
			"target": ".+",
		},
		Build: func() Action {
			return new(CompareImagesAction)
		},
	},
	"pg_listen": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".+",
		},
		Build: func() Action {
			return new(PGListenAction)
		},
	},
	"hash": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"method": ".+",
		},
		Build: func() Action {
			return new(HashAction)
		},
	},
	"glob": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".+",
		},
		Build: func() Action {
			return new(GlobAction)
		},
	},
	"cron": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"target": ".+",
		},
		Build: func() Action {
			return new(CronAction)
		},
	},
	"calc": {
		Fields: map[string]string{
			"name": ".+",
			"data": ".+",
		},
		Build: func() Action {
			return new(CalcAction)
		},
	},
	"html_query": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"target": ".+",
		},
		Build: func() Action {
			return new(HTMLQueryAction)
		},
	},
	"browse": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"target": ".+",
		},
		Build: func() Action {
			return new(BrowseAction)
		},
	},
	"wait_visible": {
		Fields: map[string]string{
			"target": ".+",
			"extra":  ".+",
		},
		Build: func() Action {
			return new(WaitVisibleAction)
		},
	},
	"set_cookie": {
		Fields: map[string]string{
			"name":  ".+",
			"data":  ".+",
			"extra": ".+",
		},
		Build: func() Action {
			return new(SetCookieAction)
		},
	},
	"screenshot_viewport": {
		Fields: map[string]string{
			"target": ".+",
			"extra":  ".+",
		},
		Build: func() Action {
			return new(ScreenshotViewportAction)
		},
	},
	"screenshot": {
		Fields: map[string]string{
			"target": ".+",
			"data":   ".+",
			"extra":  ".+",
		},
		Build: func() Action {
			return new(ScreenshotAction)
		},
	},
	"form_get_value": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".+",
			"extra":  ".+",
		},
		Build: func() Action {
			return new(FormGetValueAction)
		},
	},
	"form_set_value": {
		Fields: map[string]string{
			"target": ".+",
			"data":   ".+",
			"extra":  ".+",
		},
		Build: func() Action {
			return new(FormSetValueAction)
		},
	},
	"page_get_text": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".+",
			"extra":  ".+",
		},
		Build: func() Action {
			return new(BrowserGetTextOnPageAction)
		},
	},
	"click": {
		Fields: map[string]string{
			"target": ".+",
			"extra":  ".+",
		},
		Build: func() Action {
			return new(ClickAction)
		},
	},
	"dom_nodes": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".+",
			"extra":  ".+",
		},
		Build: func() Action {
			return new(GetDOMNodesAction)
		},
	},
	"parse": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".*",
			"data":   ".*",
			"format": "(json|toml|yaml|yml|properties|props|prop|hcl|dotenv|env|ini)",
		},
		Build: func() Action {
			return new(ParseAction)
		},
	},
	"poll": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".+",
			"data":   ".+",
		},
		Build: func() Action {
			return new(PollAction)
		},
	},
	"amqp_consume": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".+",
			"data":   ".+",
		},
		Build: func() Action {
			return new(AMQPConsumeAction)
		},
	},
	"amqp_publish": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".+",
			"data":   ".+",
		},
		Build: func() Action {
			return new(AMQPPublishAction)
		},
	},
	"parse_rss": {
		Fields: map[string]string{
			"name": ".+",
			"data": ".+",
		},
		Build: func() Action {
			return new(ParseRSSAction)
		},
	},
	"validate_json": {
		Fields: map[string]string{
			"name":   ".+",
			"data":   ".+",
			"target": ".+",
		},
		Build: func() Action {
			return new(ValidateJSONAction)
		},
	},
	"stop": {
		Fields: map[string]string{},
		Build: func() Action {
			return new(StopAction)
		},
	},
	"file_stat": {
		Fields: map[string]string{
			"name":   ".+",
			"target": ".+",
		},
		Build: func() Action {
			return new(FileStatAction)
		},
	},
	"qr": {
		Fields: map[string]string{
			"name":   ".*",
			"target": ".+",
			"data":   ".*",
		},
		Build: func() Action {
			return new(QRAction)
		},
	},
	"email": {
		Fields: map[string]string{
			"name":   ".*",
			"target": ".+",
			"data":   ".*",
		},
		Build: func() Action {
			return new(EmailAction)
		},
	},
	"input": {
		Fields: map[string]string{
			"name":  ".+",
			"data":  ".+",
			"extra": "(bool|string|int|float|)",
		},
		Build: func() Action {
			return new(InputAction)
		},
	},
}

func getAction(key string) Action {
	action, ok := actionTypes[key]
	if ok {
		return action.Build()
	}
	return nil
}
