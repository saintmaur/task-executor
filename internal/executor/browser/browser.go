package browser

import (
	"context"
	"encoding/json"
	"strings"
	"time"

	"github.com/chromedp/cdproto/emulation"
	"github.com/chromedp/chromedp"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/task-executor/internal/utils"
)

type Viewport struct {
	Width  int64
	Height int64
}

type Browser struct {
	name      string
	ctx       context.Context
	cancel    context.CancelFunc
	userAgent string
	mobile    bool
	viewport  Viewport
}

const (
	DefaultUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36"
	UserAgentKey     = "userAgent"
	ViewportKey      = "viewport"
	MobileKey        = "mobile"
)

func New(data string) *Browser {
	b := &Browser{
		viewport: Viewport{
			Width:  1430,
			Height: 900,
		},
		userAgent: DefaultUserAgent,
		mobile:    false,
	}
	b.initParams(data)
	b.initContext()
	return b
}

func (b *Browser) initContext() {
	b.ctx, b.cancel = chromedp.NewExecAllocator(
		context.Background(),
		append(chromedp.DefaultExecAllocatorOptions[:], chromedp.UserAgent(b.userAgent))...)
	b.ctx, b.cancel = chromedp.NewContext(
		b.ctx,
		chromedp.WithLogf(logger.Infof),
	)
	b.ctx, b.cancel = context.WithTimeout(b.ctx, time.Minute)
	chromedp.Run(b.ctx, emulation.SetDeviceMetricsOverride(b.viewport.Width, b.viewport.Height, 1.0, b.mobile))
}

func (b *Browser) initParams(data string) {
	params := make(map[string]string)
	err := json.Unmarshal([]byte(data), &params)
	if err != nil {
		logger.Warnf("failed to parse parameters: %s", err)
	}
	userAgent, present := params[UserAgentKey]
	if present && len(userAgent) > 0 {
		b.userAgent = userAgent
	}
	viewport, present := params[ViewportKey]
	if present && len(viewport) > 0 {
		pair := strings.Split(params[ViewportKey], ":")
		if len(pair) > 1 {
			b.viewport.Width, _ = utils.PrepareInt(pair[0])
			b.viewport.Height, _ = utils.PrepareInt(pair[1])
		}
	}
	mobile, present := params[MobileKey]
	isMobile, err := utils.PrepareInt(mobile)
	if present && len(mobile) > 0 && err != nil {
		b.mobile = isMobile > 0
	}
}

func (b *Browser) GetContext() (context.Context, context.CancelFunc) {
	return b.ctx, b.cancel
}

func (b *Browser) Navigate(url string, handler func()) {
	chromedp.Run(b.ctx, chromedp.Navigate(url))
	handler()
}
