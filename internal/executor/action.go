package executor

import (
	"bufio"
	"bytes"
	"context"
	"crypto/tls"
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"image"
	"image/png"
	"io"
	"net"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"reflect"
	"regexp"
	"sort"
	"strings"
	"syscall"
	"time"

	"github.com/Knetic/govaluate"
	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/chromedp"
	"github.com/gorilla/websocket"
	"github.com/hpcloud/tail"
	"github.com/krayzpipes/cronticker/cronticker"
	"github.com/leonelquinteros/gotext"
	"github.com/lib/pq"
	"github.com/makiuchi-d/gozxing"
	"github.com/makiuchi-d/gozxing/qrcode"
	"github.com/mmcdole/gofeed"
	"github.com/spf13/viper"
	"github.com/tidwall/gjson"
	"github.com/tidwall/sjson"
	"gitlab.com/saintmaur/lib/amqp"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/lib/stat"
	extUtils "gitlab.com/saintmaur/lib/utils"
	"gitlab.com/saintmaur/task-executor/internal/executor/browser"
	"gitlab.com/saintmaur/task-executor/internal/utils"
	"golang.org/x/crypto/ssh"

	// necessary for db operations
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/mattn/go-sqlite3"
	"github.com/xeipuuv/gojsonschema"
)

// ActionFunc is an alias for the action function signature
type ActionFunc func(e *Executor, task Task) error

// Action an interface representing an action
type Action interface {
	Run(e *Executor, task Task) error
}

// HTTPGetAction name is expressive
type HTTPAction struct{}

// Run is for satisfying the Action interface
func (a *HTTPAction) Run(e *Executor, task Task) error {
	ctx, cancel := context.WithTimeout(context.Background(), e.GetContextValue(fmt.Sprintf("%s_timeout", task.Name)).(time.Duration))
	defer cancel()
	req, err := http.NewRequestWithContext(ctx, strings.ToUpper(task.Method), task.Target, strings.NewReader(task.Data))
	if err != nil {
		return err
	}
	req.Close = true
	err = e.parseAndAddHeaders(req, task.Headers)
	if err != nil {
		return fmt.Errorf("failed on parsing headers: %s", err)
	}
	tr := http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}}
	client := http.Client{Transport: &tr}
	startTime := time.Now()
	resp, err := client.Do(req)
	e.saveTiming(task.Name, startTime)
	if err != nil {
		return err
	}
	e.SetContextValue(task.Name+"_status_code", resp.StatusCode)
	e.SetContextValue(task.Name+"_headers", resp.Header)
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("error on sending the http.%s request: %s", "GET", err)
	}
	e.SetContextValue(task.Name, string(body))
	return nil
}

// PrintAction name is expressive
type PrintAction struct{}

// Run is for satisfying the Action interface
func (a *PrintAction) Run(e *Executor, task Task) error {
	switch task.Target {
	default:
		fmt.Printf("%s", task.Data)
	case "log":
		switch task.Extra {
		case "debug":
			logger.
				WithField(executorKey, e.Name).
				WithField("task", task.Name).
				Debugf("%s", task.Data)
		default:
			logger.
				WithField(executorKey, e.Name).
				WithField("task", task.Name).
				Infof("%s", task.Data)
		}
	}
	return nil
}

// StatAction name is expressive
type StatAction struct{}

// Run is for satisfying the Action interface
func (a *StatAction) Run(e *Executor, task Task) error {
	v, err := utils.PrepareFloat(task.Data)
	if err != nil {
		return fmt.Errorf("stat value is invalid: %+v(%s), error: %s ", v, reflect.TypeOf(v), err)
	}
	var labelsMap map[string]string
	var labels []string
	var values []string
	err = json.Unmarshal([]byte(task.Labels), &labelsMap)
	if err != nil {
		return fmt.Errorf("stat labels are invalid: >>%s<<, error: %s ", task.Labels, err)
	}
	for label := range labelsMap {
		labels = append(labels, label)
	}
	sort.Strings(labels)
	for _, label := range labels {
		values = append(values, labelsMap[label])
	}
	e.StatChan <- stat.Rec{
		Name:        task.Name,
		Value:       v,
		Labels:      labels,
		LabelValues: values,
	}
	return nil
}

// InternAction name is expressive
type InternAction struct{}

// Run is for satisfying the Action interface
func (a *InternAction) Run(e *Executor, task Task) error {
	gjsoned := utils.GetGJSON(task.Data)
	var result interface{}
	if gjsoned.IsArray() {
		result = make([]interface{}, len(gjsoned.Array()))
	} else if gjsoned.IsObject() {
		result = make(map[string]interface{})
	} else {
		logger.
			WithField(executorKey, e.Name).
			WithField("task", task.Name).
			Debug("The given data is not an object or array")
	}
	err := json.Unmarshal([]byte(task.Data), &result)
	if err == nil {
		e.SetContextValue(task.Name, result)
	}
	return err
}

// JSONGetAction name is expressive
type JSONGetAction struct{}

// Run is for satisfying the Action interface
func (a *JSONGetAction) Run(e *Executor, task Task) error {
	rawResult := gjson.Get(task.Data, task.Target)
	if len(task.Extra) > 0 {
		result, err := utils.Prepare(rawResult, task.Extra)
		if err != nil {
			return err
		}
		e.SetContextValue(task.Name, result)
	} else {
		e.SetContextValue(task.Name, rawResult)
	}
	return nil
}

// JSONSetAction name is expressive
type JSONSetAction struct{}

// Run is for satisfying the Action interface
func (a *JSONSetAction) Run(e *Executor, task Task) error {
	data := e.GetContextValue(task.Extra)
	result, err := sjson.Set(task.Data, task.Target, data)
	if err != nil {
		return err
	}
	e.SetContextValue(task.Name, result)
	return nil
}

// JSONDeleteAction name is expressive
type JSONDeleteAction struct{}

// Run is for satisfying the Action interface
func (a *JSONDeleteAction) Run(e *Executor, task Task) error {
	result, err := sjson.Delete(task.Data, task.Target)
	if err != nil {
		return err
	}
	e.SetContextValue(task.Name, result)
	return nil
}

// SetAction name is expressive
type SetAction struct{}

// Run is for satisfying the Action interface
func (a *SetAction) Run(e *Executor, task Task) error {
	var result interface{}
	var err error
	if len(task.Extra) > 0 {
		result, err = utils.Prepare(task.Data, task.Extra)
		if err != nil {
			return err
		}
	} else {
		result = task.Data
	}
	e.SetContextValue(task.Name, result)
	return nil
}

// UnsetAction name is expressive
type UnsetAction struct{}

// Run is for satisfying the Action interface
func (a *UnsetAction) Run(e *Executor, task Task) error {
	e.DeleteContextValue(task.Name)
	return nil
}

// ShellAction name is expressive
type ShellAction struct{}

// Run is for satisfying the Action interface
func (a *ShellAction) Run(e *Executor, task Task) error {
	var taskError error
	tmpScriptFile := fmt.Sprintf(".runner-%d.sh", time.Now().UnixNano())
	flags := os.O_RDWR | os.O_CREATE | os.O_TRUNC
	file, err := os.OpenFile(tmpScriptFile, flags, 0644)
	if err != nil {
		return err
	}
	defer os.Remove(tmpScriptFile)
	_, err = file.Write([]byte(task.Target))
	if err != nil {
		return err
	}
	file.Close()
	cmd := exec.Command("sh", tmpScriptFile)
	if len(task.Data) > 0 {
		var envMap map[string]string
		err = json.Unmarshal([]byte(task.Data), &envMap)
		if err != nil {
			return err
		}
		cmd.Env = os.Environ()
		for envName, envValue := range envMap {
			cmd.Env = append(cmd.Env, fmt.Sprintf("%s=%s", envName, envValue))
		}
	}
	var stdout, stderr bytes.Buffer
	exitCode := 0
	defaultFailedCode := 1
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	logger.
		WithField(executorKey, e.Name).
		WithField("task", task.Name).
		Debugf("Run the command(s): %s", task.Target)
	err = cmd.Run()
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			ws := exitError.Sys().(syscall.WaitStatus)
			exitCode = ws.ExitStatus()
		} else {
			// This will happen (in OSX) if `name` is not available in $PATH,
			// in this situation, exit code could not be get, and stderr will be
			// empty string very likely, so we use the default fail code
			exitCode = defaultFailedCode
		}
		taskError = fmt.Errorf("the command has exited with error: %s. stderr: %s", err, stderr.String())
	} else {
		// success, exitCode should be 0 if go is ok
		ws := cmd.ProcessState.Sys().(syscall.WaitStatus)
		exitCode = ws.ExitStatus()
	}
	e.SetContextValue(task.Name, stdout.String())
	e.SetContextValue(fmt.Sprintf("%s_exit_code", task.Name), exitCode)
	e.SetContextValue(fmt.Sprintf("%s_error", task.Name), stderr.String())
	return taskError
}

// RemoteShellAction name is expressive
type RemoteShellAction struct{}

// Run is for satisfying the Action interface
func (a *RemoteShellAction) Run(e *Executor, task Task) error {
	logger.
		WithField(executorKey, e.Name).
		WithField("task", task.Name).
		Debug("Prepare ssh config")
	data := SSHConfig{
		Key:  fmt.Sprintf("%s/.ssh/id_rsa", getCurrentUserHomeDir()),
		Port: "22",
		User: getCurrentUsername(),
	}
	err := json.Unmarshal([]byte(task.Data), &data)
	if err != nil {
		return err
	}
	auth, err := makeKeyring(data.Key)
	if err != nil {
		return err
	}
	clientConfig := &ssh.ClientConfig{
		User:            data.User,
		Auth:            []ssh.AuthMethod{auth},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}
	conn, err := ssh.Dial("tcp", fmt.Sprintf("%s:%s", data.Host, data.Port), clientConfig)
	if err != nil {
		return err
	}
	defer conn.Close()
	session, err := conn.NewSession()
	if err != nil {
		return err
	}
	defer session.Close()
	cmd := task.Target
	var stdout, stderr bytes.Buffer
	session.Stdout = &stdout
	session.Stderr = &stderr
	if len(task.Extra) > 0 {
		var envMap map[string]string
		err = json.Unmarshal([]byte(task.Extra), &envMap)
		if err != nil {
			return err
		}
		for envName, envValue := range envMap {
			cmd = fmt.Sprintf(`%s="%s" %s`, envName, envValue, cmd)
		}
	}
	logger.
		WithField(executorKey, e.Name).
		WithField("task", task.Name).
		Debugf("Run the command: %s", task.Target)
	if err := session.Run(cmd); err != nil {
		e.SetContextValue(fmt.Sprintf("%s_error", task.Name), stderr.String())
		return fmt.Errorf("command run has finished with error: %s. stderr: %s", err, stderr.String())
	}
	e.SetContextValue(task.Name, stdout.String())
	return nil
}

// ReadFileAction name is expressive
type ReadFileAction struct{}

// Run is for satisfying the Action interface
func (a *ReadFileAction) Run(e *Executor, task Task) error {
	file, err := os.OpenFile(ExpandPath(task.Target), os.O_RDONLY, 0644)
	if err != nil {
		return err
	}
	bts, err := io.ReadAll(file)
	if err != nil {
		return err
	}
	e.SetContextValue(task.Name, string(bts))
	return nil
}

// WriteFileAction name is expressive
type WriteFileAction struct{}

// Run is for satisfying the Action interface
func (a *WriteFileAction) Run(e *Executor, task Task) error {
	target := ExpandPath(task.Target)
	doTruncate, err := utils.PrepareBool(task.Extra)
	if len(task.Extra) == 0 || err != nil {
		doTruncate = false
	}
	flags := os.O_RDWR | os.O_CREATE
	if doTruncate {
		flags |= os.O_TRUNC
	} else {
		flags |= os.O_APPEND
	}
	err = extUtils.CreateDir(target)
	if err != nil {
		return err
	}
	file, err := os.OpenFile(target, flags, 0644)
	if err != nil {
		return err
	}
	defer file.Close()
	_, err = file.Write([]byte(task.Data))
	if err != nil {
		return err
	}
	return nil
}

// RegexpAction name is expressive
type RegexpAction struct{}

// Run is for satisfying the Action interface
func (a *RegexpAction) Run(e *Executor, task Task) error {
	var matches [][]string
	re, err := regexp.Compile(task.Target)
	if err != nil {
		return err
	}
	matches = re.FindAllStringSubmatch(task.Data, -1)
	e.SetContextValue(task.Name, matches)
	return nil
}

// LoopAction name is expressive
type LoopAction struct{}

// Run is for satisfying the Action interface
func (a *LoopAction) Run(e *Executor, task Task) error {
	scenario, ok := e.PredefinedScenario[task.Target]
	if !ok {
		return fmt.Errorf("subscenario '%s' is missing", task.Target)
	}
	finishedC := make(chan struct{})
	stopC := make(chan bool, 2)
	repeatedName := fmt.Sprintf("%s_%d", task.Name, time.Now().UnixNano())
	if !e.addRepeated(repeatedName, stopC) {
		return fmt.Errorf("tried to add a stop channel with non-unique name which is a potential goroutine leak")
	}
	loopData := e.GetContextValue(task.Data)
	data := utils.PrepareArray(loopData)
	f := func() {
		defer e.wgDone()
		for recKey, recVal := range data {
			select {
			case <-stopC:
				logger.
					WithField(executorKey, e.Name).
					WithField("task", task.Name).
					Debug("Exit the loop")
				finishedC <- struct{}{}
				return
			default:
				e.SetContextValue(task.Name+"_item", recVal)
				e.SetContextValue(task.Name+"_item_value", recVal)
				e.SetContextValue(task.Name+"_item_key", recKey)
				logger.
					WithField(executorKey, e.Name).
					WithField("task", task.Name).
					Debugf("Run the loop subscenario '%s'", task.Target)
				e.RunScenario(scenario, e.currentTaskID)
				if len(task.Extra) > 0 {
					if e.TrueCondition(task.Extra) {
						e.StopRepeat(repeatedName)
					}
				}
			}
		}
		finishedC <- struct{}{}
	}
	e.wgAdd()
	e.tPool.Submit(f)
	logger.
		WithField(executorKey, e.Name).
		WithField("task", task.Name).
		Debug("Loop has started. Wait for exit")
	<-finishedC
	e.StopRepeat(repeatedName)
	logger.
		WithField(executorKey, e.Name).
		WithField("task", task.Name).
		Debug("Finally exit")
	return nil
}

// RunAction name is expressive
type RunAction struct{}

// Run is for satisfying the Action interface
func (a *RunAction) Run(e *Executor, task Task) error {
	scenario, ok := e.PredefinedScenario[task.Target]
	if !ok {
		return fmt.Errorf("the '%s' subscenario is missing", task.Target)
	}
	logger.
		WithField(executorKey, e.Name).
		WithField("task", task.Name).
		Debugf("Run subscenario '%s'", task.Target)
	if len(task.Async) > 0 && e.TrueCondition(task.Async) {
		capturedE := e.clone(task.Extra)
		f := func() {
			logger.
				WithField(executorKey, capturedE.Name).
				WithField("task", task.Name).
				Debug("Run the scenario with the captured context")
			capturedE.RunScenario(scenario, e.currentTaskID)
			logger.
				WithField(executorKey, capturedE.Name).
				WithField("task", task.Name).
				Debug("Finished running the scenario with the captured context")
			e.wgDone()
		}
		e.wgAdd()
		e.tPool.Submit(f)
	} else {
		e.RunScenario(scenario, e.currentTaskID)
	}
	return nil
}

// DBSelectAction name is expressive
type DBSelectAction struct{}

// Run is for satisfying the Action interface
func (a *DBSelectAction) Run(e *Executor, task Task) error {
	err := processQuery(e, &task)
	if err != nil {
		return err
	}
	return nil
}

// DBExecAction name is expressive
type DBExecAction struct{}

// Run is for satisfying the Action interface
func (a *DBExecAction) Run(e *Executor, task Task) error {
	return runDBAction(e, &task)
}

// WaitAction name is expressive
type WaitAction struct{}

// Run is for satisfying the Action interface
func (a *WaitAction) Run(e *Executor, task Task) error {
	parsedDuration, err := time.ParseDuration(task.Data)
	if err != nil {
		return err
	}
	time.Sleep(parsedDuration)
	return nil
}

// ReplaceAction name is expressive
type ReplaceAction struct{}

// Run is for satisfying the Action interface
func (a *ReplaceAction) Run(e *Executor, task Task) error {
	result := strings.ReplaceAll(task.Data, task.Target, task.Extra)
	e.SetContextValue(task.Name, result)
	return nil
}

// ReplaceRegexpAction name is expressive
type ReplaceRegexpAction struct{}

// Run is for satisfying the Action interface
func (a *ReplaceRegexpAction) Run(e *Executor, task Task) error {
	re, err := regexp.Compile(task.Target)
	if err != nil {
		return fmt.Errorf("failed on compiling the regexp (%s): %s", task.Target, err)
	}
	result := re.ReplaceAllString(task.Data, task.Extra)
	e.SetContextValue(task.Name, result)
	return nil
}

// MongoGetAction name is expressive
type MongoGetAction struct{}

// Run is for satisfying the Action interface
func (a *MongoGetAction) Run(e *Executor, task Task) error {
	data, err := utils.MongoGet(task.Target, task.Data, task.Extra)
	if err != nil {
		return err
	}
	e.SetContextValue(task.Name, data)
	return nil
}

// MongoInsertAction name is expressive
type MongoInsertAction struct{}

// Run is for satisfying the Action interface
func (a *MongoInsertAction) Run(e *Executor, task Task) error {
	result, err := utils.MongoInsert(task.Target, task.Data)
	if err != nil {
		return err
	}
	e.SetContextValue(task.Name, result)
	return nil
}

// MongoUpdateAction name is expressive
type MongoUpdateAction struct{}

// Run is for satisfying the Action interface
func (a *MongoUpdateAction) Run(e *Executor, task Task) error {
	matched, modified, err := utils.MongoUpdate(task.Target, task.Extra, task.Data)
	if err != nil {
		return fmt.Errorf("mongo: %s", err)
	}
	e.SetContextValue(task.Name+"_matched", matched)
	e.SetContextValue(task.Name+"_modified", modified)
	return nil
}

// MongoDeleteAction name is expressive
type MongoDeleteAction struct{}

// Run is for satisfying the Action interface
func (a *MongoDeleteAction) Run(e *Executor, task Task) error {
	if task.Data == "{}" {
		return fmt.Errorf("deleting all records is not permitted")
	}
	result, err := utils.MongoDelete(task.Target, task.Data)
	if err != nil {
		return fmt.Errorf("mongo: %s", err)
	}
	e.SetContextValue(task.Name, result)
	return nil
}

// MongoAggregateAction name is expressive
type MongoAggregateAction struct{}

// Run is for satisfying the Action interface
func (a *MongoAggregateAction) Run(e *Executor, task Task) error {
	data, err := utils.MongoAggregate(task.Target, task.Data)
	if err != nil {
		return fmt.Errorf("mongo: %s", err)
	}
	e.SetContextValue(task.Name, data)
	return nil
}

// CSVParseAction name is expressive
type CSVParseAction struct{}

// Run is for satisfying the Action interface
func (a *CSVParseAction) Run(e *Executor, task Task) error {
	reader := csv.NewReader(strings.NewReader(task.Data))
	reader.Comment = rune('#')
	reader.ReuseRecord = true
	if len(task.Extra) > 0 {
		reader.Comma = rune(task.Extra[0])
	}
	row, err := reader.Read()
	if err != nil {
		return err
	}
	headers := make([]string, len(row))
	copy(headers, row)
	var result []map[string]string
	for {
		values, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		obj := make(map[string]string)
		for i, value := range values {
			if i < len(headers) {
				header := headers[i]
				obj[header] = value
			}
		}
		result = append(result, obj)
	}
	e.SetContextValue(task.Name, result)
	return nil
}

// SignAction name is expressive
type SignAction struct{}

// Run is for satisfying the Action interface
func (a *SignAction) Run(e *Executor, task Task) error {
	result := utils.Sign(task.Method, task.Extra, task.Data)
	e.SetContextValue(task.Name, result)
	return nil
}

// HashAction name is expressive
type HashAction struct{}

// Run is for satisfying the Action interface
func (a *HashAction) Run(e *Executor, task Task) error {
	result := utils.Hash(task.Method, task.Data)
	e.SetContextValue(task.Name, result)
	return nil
}

// CronAction name is expressive
type CronAction struct{}

// Run is for satisfying the Action interface
func (a *CronAction) Run(e *Executor, task Task) error {
	scenario, ok := e.PredefinedScenario[task.Target]
	if !ok {
		return fmt.Errorf("scenario '%s' is missing", task.Target)
	}
	var timeChan <-chan time.Time
	if duration, err := extUtils.IsTimeDuration(task.Data); err == nil {
		ticker := time.NewTicker(duration)
		timeChan = ticker.C
	} else {
		ticker, err := cronticker.NewTicker(task.Data)
		if err != nil {
			return err
		}
		timeChan = ticker.C
	}
	stopC := make(chan bool)
	if !e.addRepeated(task.Name, stopC) {
		return fmt.Errorf("tried to add a stop channel with non-unique name which is a potential goroutine leak")
	}
	e.wgAdd()
	go func() {
		for {
			select {
			case <-stopC:
				logger.
					WithField(executorKey, e.Name).
					WithField("task", task.Name).
					Debug("Exit cron")
				e.wgDone()
				return
			case <-timeChan:
				logger.
					WithField(executorKey, e.Name).
					WithField("task", task.Name).
					Debug("Run the cron task")
				e.RunScenario(scenario, fmt.Sprintf("%d", time.Now().UnixNano()))
			}
		}
	}()
	return nil
}

// WhileAction name is expressive
type WhileAction struct{}

// Run is for satisfying the Action interface
func (a *WhileAction) Run(e *Executor, task Task) error {
	if !e.TrueCondition(task.Data) {
		return nil
	}
	scenario, ok := e.PredefinedScenario[task.Target]
	if !ok {
		return fmt.Errorf("subscenario '%s' is missing", task.Target)
	}
	// A chan of size 2 is necessary for ability to have control over the task run
	stopC := make(chan bool, 2)
	repeatedName := fmt.Sprintf("%s_%d", task.Name, time.Now().UnixNano())
	if !e.addRepeated(repeatedName, stopC) {
		return fmt.Errorf("tried to add a stop channel with non-unique name which is a potential goroutine leak")
	}
	finishedC := make(chan struct{})
	f := func() {
		defer e.wgDone()
		for e.TrueCondition(task.Data) {
			select {
			case <-stopC:
				logger.
					WithField(executorKey, e.Name).
					WithField("task", task.Name).
					Debug("Exit the 'while' loop")
				finishedC <- struct{}{}
				return
			default:
				logger.
					WithField(executorKey, e.Name).
					WithField("task", task.Name).
					Debugf("Run the subscenario '%s'", task.Target)
				e.RunScenario(scenario, fmt.Sprintf("%d", time.Now().UnixNano()))
			}
		}
		logger.
			WithField(executorKey, e.Name).
			WithField("task", task.Name).
			Infof("Loop condition is false, exit")
		finishedC <- struct{}{}
	}
	e.wgAdd()
	e.tPool.Submit(f)
	logger.
		WithField(executorKey, e.Name).
		WithField("task", task.Name).
		Debug("Loop has started. Wait for exit")
	<-finishedC
	e.StopRepeat(repeatedName)
	logger.
		WithField(executorKey, e.Name).
		WithField("task", task.Name).
		Debug("Finally exit")
	return nil
}

// BreakAction name is expressive
type BreakAction struct{}

// Run is for satisfying the Action interface
func (a *BreakAction) Run(e *Executor, task Task) error {
	e.StopRepeat(task.Name)
	return nil
}

// TimeAction name is expressive
type TimeAction struct{}

// Run is for satisfying the Action interface
func (a *TimeAction) Run(e *Executor, task Task) error {
	var result int64
	if len(task.Data) == 0 {
		result = time.Now().UnixNano()
	} else {
		if len(task.Format) > 0 {
			goFormat := extUtils.PrepareDTFormat(task.Format)
			tm, err := time.Parse(goFormat, task.Data)
			if err != nil {
				return err
			}
			result = tm.UnixNano()
		}
	}
	e.SetContextValue(task.Name, result)
	return nil
}

// TimeShiftAction name is expressive
type TimeShiftAction struct{}

// Run is for satisfying the Action interface
func (a *TimeShiftAction) Run(e *Executor, task Task) error {
	shift, err := time.ParseDuration(task.Duration)
	if err != nil {
		return err
	}
	value, err := utils.PrepareInt(task.Data)
	if err != nil {
		return err
	}
	tm := extUtils.MakeTimestamp(value)
	tm = tm.Add(shift)
	e.SetContextValue(task.Name, tm.UnixNano())
	return nil
}

// TimeFormatAction name is expressive
type TimeFormatAction struct{}

// Run is for satisfying the Action interface
func (a *TimeFormatAction) Run(e *Executor, task Task) error {
	value, err := utils.PrepareInt(task.Data)
	if err != nil {
		return err
	}
	tm := extUtils.MakeTimestamp(value)
	goFormat := extUtils.PrepareDTFormat(task.Format)
	e.SetContextValue(task.Name, tm.Format(goFormat))
	return nil
}

// LocalizeAction name is expressive
type LocalizeAction struct{}

// Run is for satisfying the Action interface
func (a *LocalizeAction) Run(e *Executor, task Task) error {
	_, err := os.Stat(task.Extra)
	if err != nil {
		return err
	}
	l := gotext.NewLocale(task.Extra, task.Target)
	l.AddDomain(defaultKey)
	e.SetContextValue(task.Name, l.Get(task.Data))
	return nil
}

// TemplateAction name is expressive
type TemplateAction struct{}

// Run is for satisfying the Action interface
func (a *TemplateAction) Run(e *Executor, task Task) error {
	processed, err := e.ProcessTemplate(task.Data)
	if err != nil {
		return err
	}
	e.SetContextValue(task.Name, processed)
	return nil
}

// EncodeAction name is expressive
type EncodeAction struct{}

// Run is for satisfying the Action interface
func (a *EncodeAction) Run(e *Executor, task Task) error {
	result := utils.Encode(task.Method, task.Data, "")
	e.SetContextValue(task.Name, result)
	return nil
}

// EncodeAction name is expressive
type DecodeAction struct{}

// Run is for satisfying the Action interface
func (a *DecodeAction) Run(e *Executor, task Task) error {
	result := utils.Decode(task.Method, task.Data)
	e.SetContextValue(task.Name, result)
	return nil
}

// SetMapValueAction name is expressive
type SetMapValueAction struct{}

// Run is for satisfying the Action interface
func (a *SetMapValueAction) Run(e *Executor, task Task) error {
	data := e.GetContextValue(task.Data)
	targetMap := e.GetContextValue(task.Name)
	if targetMap == nil {
		targetMap = make(map[string]interface{})
	}
	resultb, err := json.Marshal(targetMap)
	if err != nil {
		return err
	}
	resultb, err = sjson.SetBytes(resultb, task.Target, data)
	if err != nil {
		return err
	}
	err = json.Unmarshal([]byte(resultb), &targetMap)
	if err == nil {
		e.SetContextValue(task.Name, targetMap)
	}
	return err
}

// InjectAction name is expressive
type InjectAction struct{}

// Run is for satisfying the Action interface
func (a *InjectAction) Run(e *Executor, task Task) error {
	var scenarios map[string]Scenario
	configParser := viper.New()
	configParser.SetConfigType(task.Format)
	if len(task.Data) == 0 {
		configParser.SetConfigFile(task.Target)
		if err := configParser.ReadInConfig(); err != nil {
			return err
		}
	} else {
		err := configParser.ReadConfig(strings.NewReader(task.Data))
		if err != nil {
			return err
		}
	}
	err := configParser.Unmarshal(&scenarios)
	if err != nil {
		return err
	}
	if len(scenarios) == 0 {
		return fmt.Errorf("failed to parse the scenario")
	}
	for name, scenario := range scenarios {
		if !e.validateScenario(scenario) {
			logger.
				WithField(executorKey, e.Name).
				WithField("task", task.Name).
				Warnf("Failed on validating '%s' scenario. Remove it.", task.Name)
			continue
		}
		e.tryAddScenario(name, scenario)
		e.tryAddAppendFuncMap(name, scenario)
	}
	return nil
}

// PipeAction name is expressive
type PipeAction struct{}

// Run is for satisfying the Action interface
func (a *PipeAction) Run(e *Executor, task Task) error {
	if len(task.Data) > 0 {
		pipe := e.GetContextValue(task.Target)
		if pipe != nil {
			logger.
				WithField(executorKey, e.Name).
				WithField("task", task.Name).
				Debugf("Send the data to the pipe '%s'", task.Target)
			pipe.(chan interface{}) <- task.Data
		} else {
			return fmt.Errorf("the pipe '%s' is missing", task.Target)
		}
	} else {
		var pipe chan interface{}
		pipeRaw := e.GetContextValue(task.Target)
		if pipeRaw == nil {
			logger.
				WithField(executorKey, e.Name).
				WithField("task", task.Name).
				Debugf("Try to create a pipe '%s'", task.Name)
			scenario, ok := e.PredefinedScenario[task.Target]
			if !ok {
				return fmt.Errorf("subscenario '%s' is missing", task.Target)
			}
			stopC := make(chan bool)
			if !e.addRepeated(task.Name, stopC) {
				return fmt.Errorf("tried to add a stop channel with non-unique name which is a potential goroutine leak")
			}
			extra := task.Extra
			if len(extra) == 0 {
				extra = "1000"
			}
			capacity, err := utils.PrepareInt(extra)
			if err != nil {
				capacity = 1000
			}
			pipe = make(chan interface{}, capacity)
			f := func() {
				for {
					select {
					case <-stopC:
						logger.
							WithField(executorKey, e.Name).
							WithField("task", task.Name).
							Debugf("Exit '%s' pipe processor", task.Name)
						e.wgDone()
						return
					case data := <-pipe:
						logger.
							WithField(executorKey, e.Name).
							WithField("task", task.Name).
							Debugf("Got a message: %s", data)
						e.SetContextValue(fmt.Sprintf("%s_item", task.Name), data)
						e.RunScenario(scenario, fmt.Sprintf("%d", time.Now().UnixNano()))
					}
				}
			}
			e.wgAdd()
			e.tPool.Submit(f)
		} else {
			pipe = pipeRaw.(chan interface{})
		}
		e.SetContextValue(task.Name, pipe)
	}
	return nil
}

// WebsocketAction name is expressive
type WebsocketAction struct{}

// Run is for satisfying the Action interface
func (a *WebsocketAction) Run(e *Executor, task Task) error {
	if e.GetContextValue(task.Name) != nil {
		logger.
			WithField(executorKey, e.Name).
			WithField("task", task.Name).
			Infof("A websocket connection named by '%s' already exists", task.Name)
		return nil
	}
	pipeRaw := e.GetContextValue(task.Data)
	if pipeRaw == nil {
		return fmt.Errorf("pipe '%s' is missing", task.Data)
	}
	pipe := pipeRaw.(chan interface{})
	conn := utils.NewWebSocketConnection(task.Target)
	if conn == nil {
		return fmt.Errorf("failed on connecting to the websocket")
	}
	stopC := make(chan bool)
	if !e.addRepeated(task.Name, stopC) {
		return fmt.Errorf("tried to add a stop channel with non-unique name which is a potential goroutine leak")
	}
	outputPipe := make(chan interface{})
	processOutgoing := func() {
		for {
			select {
			case <-stopC:
				logger.
					WithField(executorKey, e.Name).
					WithField("task", task.Name).
					Infof("Exit '%s' websocket processor", task.Name)
				err := conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
				if err != nil {
					logger.
						WithField(executorKey, e.Name).
						WithField("task", task.Name).
						Warnf("Websocket write close error: %s", err)
					return
				}
				conn.Close()
				e.wgDone()
				return
			case data := <-outputPipe:
				logger.
					WithField(executorKey, e.Name).
					WithField("task", task.Name).
					Debugf("Got a message for sending to the websocket['%s']: %s", task.Name, data)
				message, err := utils.PrepareString(data)
				if err != nil {
					logger.
						WithField(executorKey, e.Name).
						WithField("task", task.Name).
						Debugf("Failed on preparing a message for sending to the websocket[%s]: %s", task.Name, err)
				}
				err = conn.WriteMessage(websocket.TextMessage, []byte(message))
				if err != nil {
					logger.
						WithField(executorKey, e.Name).
						WithField("task", task.Name).
						Warnf("Failed on sending a message to the websocket[%s]: %s", task.Name, err)
				}
			}
		}
	}
	processIncoming := func() {
		for {
			_, message, err := conn.ReadMessage()
			if err != nil {
				e.wgDone()
				return
			}
			pipe <- string(message)
		}
	}
	e.wgAdd()
	e.tPool.Submit(processOutgoing)
	e.wgAdd()
	e.tPool.Submit(processIncoming)
	e.SetContextValue(fmt.Sprintf("%s_pipe", task.Name), outputPipe)
	return nil
}

// TCPAction name is expressive
type TCPAction struct{}

// Run is for satisfying the Action interface
func (a *TCPAction) Run(e *Executor, task Task) error {
	const defaultMessageSize = int64(1024)
	messageSize, err := utils.PrepareInt(task.Extra)
	if err != nil {
		messageSize = defaultMessageSize
	}
	if e.GetContextValue(task.Name) != nil && e.GetContextValue(task.Name).(bool) {
		logger.
			WithField(executorKey, e.Name).
			WithField("task", task.Name).
			Infof("A TCP connection named by '%s' already exists", task.Name)
		return nil
	}
	pipeRaw := e.GetContextValue(task.Data)
	if pipeRaw == nil {
		return fmt.Errorf("a pipe '%s' for input messages is missing", task.Data)
	}
	inputPipe := pipeRaw.(chan interface{})
	parts := strings.Split(task.Target, ":")
	if len(parts) < 2 {
		return fmt.Errorf("target must be in a <host>:<port> format")
	}
	conn, err := net.DialTimeout("tcp", net.JoinHostPort(parts[0], parts[1]), 3*time.Second)
	if err != nil || conn == nil {
		e.SetContextValue(task.Name, false)
		return fmt.Errorf("failed on connecting to the TCP server: %s", err)
	}
	e.SetContextValue(task.Name, true)
	stopC := make(chan bool)
	if !e.addRepeated(task.Name, stopC) {
		return fmt.Errorf("tried to add a stop channel with non-unique name which is a potential goroutine leak")
	}
	outputPipe := make(chan interface{})
	processOutgoing := func() {
		for {
			select {
			case <-stopC:
				logger.
					WithField(executorKey, e.Name).
					WithField("task", task.Name).
					Debugf("exit '%s' TCP processor", task.Name)
				conn.Close()
				e.wgDone()
				return
			case data := <-outputPipe:
				logger.
					WithField(executorKey, e.Name).
					WithField("task", task.Name).
					Debugf("got a message for sending by TCP channel['%s']: %s", task.Name, data)
				message, err := utils.PrepareByte(data)
				if err != nil {
					logger.
						WithField(executorKey, e.Name).
						WithField("task", task.Name).
						Infof("failed on preparing a message for sending by TCP channel[%s]: %s", task.Name, err)
				}
				_, err = conn.Write(message)
				if err != nil {
					logger.
						WithField(executorKey, e.Name).
						WithField("task", task.Name).
						Warnf("failed on sending a message by TCP channel[%s]: %s", task.Name, err)
				}
			}
		}
	}
	processIncoming := func() {
		for {
			recvBuf := make([]byte, messageSize)
			_, err := conn.Read(recvBuf[:])
			if err != nil {
				logger.
					WithField(executorKey, e.Name).
					WithField("task", task.Name).
					Errorf("TCP input processor error: %s", err)
				logger.
					WithField(executorKey, e.Name).
					WithField("task", task.Name).
					Debugf("Exit '%s' TCP input processor", task.Name)
				e.wgDone()
				return
			}
			message := string(recvBuf)
			message = strings.TrimSpace(message)
			inputPipe <- message
		}
	}
	e.wgAdd()
	e.tPool.Submit(processOutgoing)
	e.wgAdd()
	e.tPool.Submit(processIncoming)
	e.SetContextValue(fmt.Sprintf("%s_pipe", task.Name), outputPipe)
	return nil
}

// UDPAction name is expressive
type UDPAction struct{}

// Run is for satisfying the Action interface
func (a *UDPAction) Run(e *Executor, task Task) error {
	const defaultMessageSize = int64(1024)
	messageSize, err := utils.PrepareInt(task.Extra)
	if err != nil {
		messageSize = defaultMessageSize
	}
	if e.GetContextValue(task.Name) != nil && e.GetContextValue(task.Name).(bool) {
		logger.
			WithField(executorKey, e.Name).
			WithField("task", task.Name).
			Infof("A TCP connection named by '%s' already exists", task.Name)
		return nil
	}
	pipeRaw := e.GetContextValue(task.Data)
	if pipeRaw == nil {
		return fmt.Errorf("a pipe '%s' for input messages is missing", task.Data)
	}
	inputPipe := pipeRaw.(chan interface{})
	parts := strings.Split(task.Target, ":")
	if len(parts) < 2 {
		return fmt.Errorf("target must be in a <host>:<port> format")
	}
	conn, err := net.DialTimeout("udp", net.JoinHostPort(parts[0], parts[1]), 3*time.Second)
	if err != nil || conn == nil {
		e.SetContextValue(task.Name, false)
		return fmt.Errorf("failed on connecting to the UDP server: %s", err)
	}
	e.SetContextValue(task.Name, true)
	stopC := make(chan bool)
	if !e.addRepeated(task.Name, stopC) {
		return fmt.Errorf("tried to add a stop channel with non-unique name which is a potential goroutine leak")
	}
	outputPipe := make(chan interface{})
	processOutgoing := func() {
		for {
			select {
			case <-stopC:
				logger.
					WithField(executorKey, e.Name).
					WithField("task", task.Name).
					Debugf("exit '%s' TCP processor", task.Name)
				conn.Close()
				e.wgDone()
				return
			case data := <-outputPipe:
				logger.
					WithField(executorKey, e.Name).
					WithField("task", task.Name).
					Debugf("got a message for sending by UDP channel['%s']: %s", task.Name, data)
				message, err := utils.PrepareByte(data)
				if err != nil {
					logger.
						WithField(executorKey, e.Name).
						WithField("task", task.Name).
						Infof("failed on preparing a message for sending by UDP channel[%s]: %s", task.Name, err)
				}
				_, err = conn.Write(message)
				if err != nil {
					logger.
						WithField(executorKey, e.Name).
						WithField("task", task.Name).
						Warnf("failed on sending a message by UDP channel[%s]: %s", task.Name, err)
				}
			}
		}
	}
	processIncoming := func() {
		for {
			recvBuf := make([]byte, messageSize)
			_, err := conn.Read(recvBuf[:])
			if err != nil {
				logger.
					WithField(executorKey, e.Name).
					WithField("task", task.Name).
					Errorf("UDP input processor error: %s", err)
				logger.
					WithField(executorKey, e.Name).
					WithField("task", task.Name).
					Debugf("Exit '%s' UDP input processor", task.Name)
				e.wgDone()
				return
			}
			message := string(recvBuf)
			message = strings.TrimSpace(message)
			inputPipe <- message
		}
	}
	e.wgAdd()
	e.tPool.Submit(processOutgoing)
	e.wgAdd()
	e.tPool.Submit(processIncoming)
	e.SetContextValue(fmt.Sprintf("%s_pipe", task.Name), outputPipe)
	return nil
}

// BrowseAction name is expressive
type BrowseAction struct {
}

// Run is for satisfying the Action interface
func (a *BrowseAction) Run(e *Executor, task Task) error {
	browser := browser.New(task.Data)
	generalCtx, cancel := browser.GetContext()
	defer cancel()
	logger.
		WithField(executorKey, e.Name).
		WithField("task", task.Name).
		Debug("Run the 'before' scenario")
	e.SetContextValue(task.Name, generalCtx)
	scenario, ok := e.PredefinedScenario[fmt.Sprintf("%s_before", task.Name)]
	if ok {
		logger.
			WithField(executorKey, e.Name).
			WithField("task", task.Name).
			Debugf("Run subscenario '%s'", task.Name)
		e.RunScenario(scenario, fmt.Sprintf("%d", time.Now().UnixNano()))
	}
	startTime := time.Now()
	browser.Navigate(task.Target, func() {
		e.saveTiming(fmt.Sprintf("%s_navigate_duration", task.Name), startTime)
		logger.
			WithField(executorKey, e.Name).
			WithField("task", task.Name).
			Debug("Run the 'target' scenario")
		scenario, ok = e.PredefinedScenario[task.Name]
		if ok {
			logger.
				WithField(executorKey, e.Name).
				WithField("task", task.Name).
				Debugf("Run subscenario '%s'", task.Name)
			e.RunScenario(scenario, fmt.Sprintf("%d", time.Now().UnixNano()))
		}
	})

	logger.
		WithField(executorKey, e.Name).
		WithField("task", task.Name).
		Debug("Run the 'after' scenario")
	scenario, ok = e.PredefinedScenario[fmt.Sprintf("%s_after", task.Name)]
	if ok {
		logger.
			WithField(executorKey, e.Name).
			WithField("task", task.Name).
			Debugf("Run subscenario '%s'", task.Name)
		e.RunScenario(scenario, fmt.Sprintf("%d", time.Now().UnixNano()))
	}
	logger.
		WithField(executorKey, e.Name).
		WithField("task", task.Name).
		Debug("Run successfully")
	return nil
}

// WaitVisibleAction name is expressive
type WaitVisibleAction struct {
}

// Run is for satisfying the Action interface
func (a *WaitVisibleAction) Run(e *Executor, task Task) error {
	runtimeCtx, cancel, err := e.getBrowserContext(task.Extra)
	if err != nil {
		return fmt.Errorf("get the browser context; error: %s", err)
	}
	defer cancel()
	err = chromedp.Run(runtimeCtx, chromedp.WaitVisible(task.Target, chromedp.ByQueryAll))
	return err
}

// ScreenshotViewportAction name is expressive
type ScreenshotViewportAction struct {
}

// Run is for satisfying the Action interface
func (a *ScreenshotViewportAction) Run(e *Executor, task Task) error {
	var buf []byte
	runtimeCtx, cancel, err := e.getBrowserContext(task.Extra)
	if err != nil {
		return fmt.Errorf("get the browser context; error: %s", err)
	}
	defer cancel()
	err = chromedp.Run(runtimeCtx, utils.VisibleViewportScreenshot(90, &buf))
	if err != nil {
		return err
	}
	err = os.WriteFile(task.Target, buf, 0644)
	return err
}

// ScreenshotAction name is expressive
type ScreenshotAction struct {
}

// Run is for satisfying the Action interface
func (a *ScreenshotAction) Run(e *Executor, task Task) error {
	var buf []byte
	runtimeCtx, cancel, err := e.getBrowserContext(task.Extra)
	if err != nil {
		return fmt.Errorf("get the browser context; error: %s", err)
	}
	defer cancel()
	err = chromedp.Run(runtimeCtx, chromedp.Screenshot(task.Target, &buf, chromedp.ByQueryAll))
	if err != nil {
		return err
	}
	err = os.WriteFile(task.Data, buf, 0644)
	return err
}

// SetCookieAction name is expressive
type SetCookieAction struct {
}

// Run is for satisfying the Action interface
func (a *SetCookieAction) Run(e *Executor, task Task) error {
	runtimeCtx, cancel, err := e.getBrowserContext(task.Extra)
	if err != nil {
		return fmt.Errorf("get the browser context; error: %s", err)
	}
	defer cancel()
	err = chromedp.Run(runtimeCtx, utils.SetCookie(runtimeCtx, task.Data))
	if err != nil {
		return fmt.Errorf("set the cookie; error: %s", err)
	}
	return nil
}

// FormGetValueAction name is expressive
type FormGetValueAction struct {
}

// Run is for satisfying the Action interface
func (a *FormGetValueAction) Run(e *Executor, task Task) error {
	runtimeCtx, cancel, err := e.getBrowserContext(task.Extra)
	if err != nil {
		return fmt.Errorf("get the browser context; error: %s", err)
	}
	defer cancel()
	var value string
	err = chromedp.Run(runtimeCtx, chromedp.Value(task.Target, &value, chromedp.ByQueryAll))
	if err != nil {
		return err
	}
	e.SetContextValue(task.Name, value)
	return err
}

// FormSetValueAction name is expressive
type FormSetValueAction struct {
}

// Run is for satisfying the Action interface
func (a *FormSetValueAction) Run(e *Executor, task Task) error {
	return e.sendBrowserKeys(task, task.Data)
}

// FormSendKeysAction name is expressive
type FormSendKeysAction struct {
}

// Run is for satisfying the Action interface
func (a *FormSendKeysAction) Run(e *Executor, task Task) error {
	codes := strings.Split(task.Data, "+")
	data := utils.GetStringFromCodes(codes)
	return e.sendBrowserKeys(task, data)
}

// FormSubmitAction name is expressive
type FormSubmitAction struct {
}

// Run is for satisfying the Action interface
func (a *FormSubmitAction) Run(e *Executor, task Task) error {
	runtimeCtx, cancel, err := e.getBrowserContext(task.Extra)
	if err != nil {
		return fmt.Errorf("get the browser context; error: %s", err)
	}
	defer cancel()
	err = chromedp.Run(runtimeCtx, chromedp.Submit(task.Target, chromedp.ByQueryAll))
	return err
}

// BrowserGetTextOnPageAction name is expressive
type BrowserGetTextOnPageAction struct {
}

// Run is for satisfying the Action interface
func (a *BrowserGetTextOnPageAction) Run(e *Executor, task Task) error {
	runtimeCtx, cancel, err := e.getBrowserContext(task.Extra)
	if err != nil {
		return fmt.Errorf("get the browser context; error: %s", err)
	}
	defer cancel()
	var value string
	err = chromedp.Run(runtimeCtx, chromedp.Text(task.Target, &value, chromedp.ByQuery))
	if err != nil {
		return err
	}
	e.SetContextValue(task.Name, value)
	return nil
}

// ClickAction name is expressive
type ClickAction struct {
}

// Run is for satisfying the Action interface
func (a *ClickAction) Run(e *Executor, task Task) error {
	runtimeCtx, cancel, err := e.getBrowserContext(task.Extra)
	if err != nil {
		return fmt.Errorf("get the browser context; error: %s", err)
	}
	defer cancel()
	err = chromedp.Run(runtimeCtx, chromedp.Click(task.Target, chromedp.ByQueryAll))
	if err != nil {
		return err
	}
	return nil
}

// GetDOMNodesAction name is expressive
type GetDOMNodesAction struct {
}

// Run is for satisfying the Action interface
func (a *GetDOMNodesAction) Run(e *Executor, task Task) error {
	runtimeCtx, cancel, err := e.getBrowserContext(task.Extra)
	if err != nil {
		return fmt.Errorf("get the browser context; error: %s", err)
	}
	defer cancel()
	var nodes []*cdp.Node
	err = chromedp.Run(runtimeCtx, chromedp.Nodes(task.Target, &nodes, chromedp.ByQueryAll))
	if err != nil {
		return err
	}
	e.SetContextValue(task.Name, nodes)
	return nil
}

// CompareImagesAction name is expressive
type CompareImagesAction struct {
}

// Run is for satisfying the Action interface
func (a *CompareImagesAction) Run(e *Executor, task Task) error {
	result, err := utils.CheckImagesAreSimilar(task.Target, task.Data)
	if err != nil {
		return err
	}
	e.SetContextValue(task.Name, result)
	return nil
}

// PGListenAction name is expressive
type PGListenAction struct {
}

// Run is for satisfying the Action interface
func (a *PGListenAction) Run(e *Executor, task Task) error {
	errorReporter := func(ev pq.ListenerEventType, err error) {
		if err != nil {
			logger.
				WithField(executorKey, e.Name).
				WithField("task", task.Name).
				Warnf("got an error from PG pipe: %s", err)
		}
	}
	if e.GetContextValue(task.Name) != nil {
		logger.
			WithField(executorKey, e.Name).
			WithField("task", task.Name).
			Infof("A PG longpolling-connection named by '%s' already exists", task.Name)
		return nil
	}
	listener := pq.NewListener(task.Target, 10*time.Second, time.Minute, errorReporter)
	err := listener.Listen(task.Name)
	if err != nil {
		return fmt.Errorf("failed on starting to listen: %s", err)
	}
	scenario, ok := e.PredefinedScenario[task.Name]
	if !ok {
		return fmt.Errorf("scenario '%s' is missing", task.Name)
	}
	stopC := make(chan bool, 2)
	if !e.addRepeated(task.Name, stopC) {
		return fmt.Errorf("tried to add a stop channel with non-unique name which is a potential goroutine leak")
	}
	f := func() {
		for {
			select {
			case <-stopC:
				logger.
					WithField(executorKey, e.Name).
					WithField("task", task.Name).
					Info("Exit the event handler")
				listener.Unlisten(task.Name)
				listener.Close()
				e.wgDone()
				return
			case notification := <-listener.Notify:
				e.SetContextValue(fmt.Sprintf("%s_item", task.Name), notification.Extra)
				logger.
					WithField(executorKey, e.Name).
					WithField("task", task.Name).
					Debug("Run the event handler")
				e.RunScenario(scenario, fmt.Sprintf("%d", time.Now().UnixNano()))
			case <-time.After(90 * time.Second):
				f := func() {
					logger.
						WithField(executorKey, e.Name).
						WithField("task", task.Name).
						Info("Make a ping to the PG pipe")
					err := listener.Ping()
					if err != nil {
						logger.
							WithField(executorKey, e.Name).
							WithField("task", task.Name).
							Errorf("%s", err)
					}
					e.wgDone()
				}
				e.wgAdd()
				e.tPool.Submit(f)
			}
		}
	}
	e.wgAdd()
	e.tPool.Submit(f)
	return nil
}

// CalcAction name is expressive
type CalcAction struct {
}

// Run is for satisfying the Action interface
func (a *CalcAction) Run(e *Executor, task Task) error {
	expression, err := govaluate.NewEvaluableExpression(task.Data)
	if err != nil {
		return err
	}
	result, err := expression.Evaluate(e.Context())
	if err != nil {
		return err
	}
	e.SetContextValue(task.Name, result)
	return nil
}

// HTMLQueryAction name is expressive
type HTMLQueryAction struct {
}

// Run is for satisfying the Action interface
func (a *HTMLQueryAction) Run(e *Executor, task Task) error {
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(task.Data))
	if err != nil {
		return err
	}
	selection := doc.Find(task.Target)
	if selection == nil {
		return fmt.Errorf("nothing has been found by selector '%s'", task.Target)
	}
	e.SetContextValue(task.Name, selection.Nodes)
	return nil
}

// GlobAction name is expressive
type GlobAction struct {
}

// Run is for satisfying the Action interface
func (a *GlobAction) Run(e *Executor, task Task) error {
	matches, err := filepath.Glob(task.Target)
	if err != nil {
		return err
	}
	e.SetContextValue(task.Name, matches)
	return nil
}

// ParseAction name is expressive
type ParseAction struct{}

// Run is for satisfying the Action interface
func (a *ParseAction) Run(e *Executor, task Task) error {
	configParser := viper.New()
	configParser.SetConfigType(task.Format)
	if len(task.Data) == 0 {
		configParser.SetConfigFile(task.Target)
		if err := configParser.ReadInConfig(); err != nil {
			return err
		}
	} else {
		err := configParser.ReadConfig(strings.NewReader(task.Data))
		if err != nil {
			return err
		}
	}
	result := make(map[string]interface{})
	err := configParser.Unmarshal(&result)
	if err != nil {
		return err
	}
	e.SetContextValue(task.Name, result)
	return nil
}

// PollAction name is expressive
type PollAction struct{}

// Run is for satisfying the Action interface
func (a *PollAction) Run(e *Executor, task Task) error {
	scenario, ok := e.PredefinedScenario[task.Target]
	if !ok {
		return fmt.Errorf("subscenario '%s' is missing", task.Target)
	}
	stopC := make(chan bool, 2)
	if !e.addRepeated(task.Name, stopC) {
		return fmt.Errorf("tried to add a stop channel with non-unique name which is a potential goroutine leak")
	}
	t, err := tail.TailFile(task.Data, tail.Config{Follow: true, Poll: true})
	if err != nil {
		return err
	}
	f := func() {
		for {
			select {
			case <-stopC:
				logger.
					WithField(executorKey, e.Name).
					WithField("task", task.Name).
					Debugf("exit '%s' poll processor", task.Name)
				e.wgDone()
				return
			case line, ok := <-t.Lines:
				if ok {
					e.SetContextValue(fmt.Sprintf("%s_item", task.Name), line.Text)
					e.RunScenario(scenario, fmt.Sprintf("%d", time.Now().UnixNano()))
				}
			}
		}
	}
	e.wgAdd()
	e.tPool.Submit(f)
	return nil
}

// AMQPConsumeAction name is expressive
type AMQPConsumeAction struct{}

// Run is for satisfying the Action interface
func (a *AMQPConsumeAction) Run(e *Executor, task Task) error {
	scenario, ok := e.PredefinedScenario[task.Data]
	if !ok {
		return fmt.Errorf("subscenario '%s' is missing", task.Data)
	}
	cfg, err := amqp.ParseConfig(task.Target)
	if err != nil {
		return err
	}
	err = e.amqpConnect(cfg.URL, task)
	if err != nil {
		return err
	}
	consumerStopC := make(chan bool)
	if !e.addRepeated(fmt.Sprintf("%s_amqp_consumer", task.Name), consumerStopC) {
		return fmt.Errorf("tried to add a stop channel with non-unique name which is a potential goroutine leak")
	}
	connection := e.GetContextValue(cfg.URL)
	e.wgAdd()
	return amqp.StartConsumer(connection.(*amqp.Connection), cfg, func(message amqp.Message) {
		e.SetContextValue(fmt.Sprintf("%s_item", task.Name), message.Delivery)
		e.RunScenario(scenario, fmt.Sprintf("%d", time.Now().UnixNano()))
	}, func() {
		e.wgDone()
	}, consumerStopC, false)
}

// AMQPPublishAction name is expressive
type AMQPPublishAction struct{}

// Run is for satisfying the Action interface
func (a *AMQPPublishAction) Run(e *Executor, task Task) error {
	cfg, err := amqp.ParseConfig(task.Target)
	if err != nil {
		return err
	}
	err = e.amqpConnect(cfg.URL, task)
	if err != nil {
		return err
	}
	connection := e.GetContextValue(cfg.URL)
	amqp.Publish(connection.(*amqp.Connection), cfg, task.Data)
	return nil
}

// ParseRSSAction name is expressive
type ParseRSSAction struct{}

// Run is for satisfying the Action interface
func (a *ParseRSSAction) Run(e *Executor, task Task) error {
	fp := gofeed.NewParser()
	feed, err := fp.ParseString(task.Data)
	if err != nil {
		return err
	}
	l := len(feed.Items)
	items := make([]*gofeed.Item, l)
	for i := range feed.Items {
		items[i] = feed.Items[l-i-1]
	}
	feed.Items = items
	e.SetContextValue(task.Name, feed)
	return nil
}

// ValidateJSONAction name is expressive
type ValidateJSONAction struct{}

// Run is for satisfying the Action interface
func (a *ValidateJSONAction) Run(e *Executor, task Task) error {
	schemaLoader := gojsonschema.NewStringLoader(task.Target)
	if _, err := schemaLoader.LoadJSON(); err != nil {
		return fmt.Errorf("schema: %s", err)
	}
	documentLoader := gojsonschema.NewStringLoader(task.Data)
	if _, err := documentLoader.LoadJSON(); err != nil {
		return fmt.Errorf("document: %s", err)
	}
	result, err := gojsonschema.Validate(schemaLoader, documentLoader)
	if err != nil {
		return err
	}
	e.SetContextValue(task.Name, result.Valid())
	var errstr string
	if !result.Valid() {
		for _, e := range result.Errors() {
			errstr += e.String() + " | "
		}
		return errors.New(errstr)
	}
	return nil
}

// StopAction name is expressive
type StopAction struct{}

// Run is for satisfying the Action interface
func (a *StopAction) Run(e *Executor, task Task) error {
	e.Stop()
	return nil
}

// FileStatAction name is expressive
type FileStatAction struct{}

// Run is for satisfying the Action interface
func (a *FileStatAction) Run(e *Executor, task Task) error {
	file, err := os.Open(ExpandPath(task.Target))
	if err != nil {
		return err
	}
	defer file.Close()
	stat, err := file.Stat()
	result := struct {
		Name    string
		Size    int64
		Mode    uint32
		ModTime time.Time
		IsDir   bool
	}{
		Name:    stat.Name(),
		Size:    stat.Size(),
		Mode:    uint32(stat.Mode()),
		ModTime: stat.ModTime(),
		IsDir:   stat.IsDir(),
	}
	if err != nil {
		return err
	}
	e.SetContextValue(task.Name, result)
	return nil
}

type QRAction struct{}

// Run is for satisfying the Action interface
func (a *QRAction) Run(e *Executor, task Task) error {
	if len(task.Data) > 0 {
		err := extUtils.CreateDir(task.Target)
		if err != nil {
			return fmt.Errorf("file creating failed: %s", err)
		}
		file, err := os.Create(task.Target)
		if err != nil {
			return fmt.Errorf("file opening failed: %s", err)
		}
		defer file.Close()
		writer := qrcode.NewQRCodeWriter()
		img, err := writer.Encode(task.Data, gozxing.BarcodeFormat_QR_CODE, 128, 128, nil)
		if err != nil {
			return fmt.Errorf("QR encoding failed: %s", err)
		}
		_ = png.Encode(file, img)
	} else {
		file, err := os.Open(task.Target)
		if err != nil {
			return err
		}
		defer file.Close()
		img, _, err := image.Decode(file)
		if err != nil {
			return fmt.Errorf("image decoding failed: %s", err)
		}
		// prepare BinaryBitmap
		bmp, err := gozxing.NewBinaryBitmapFromImage(img)
		if err != nil {
			return fmt.Errorf("bitmap preparing failed: %s", err)
		}
		// decode image
		qrReader := qrcode.NewQRCodeReader()
		result, err := qrReader.Decode(bmp, nil)
		if err != nil {
			return fmt.Errorf("QR decoding failed: %s", err)
		}
		e.SetContextValue(task.Name, result.String())
	}
	return nil
}

type EmailAction struct{}

// Run is for satisfying the Action interface
func (a *EmailAction) Run(e *Executor, task Task) error {
	emailTarget, err := utils.ParseEmailTransport(task.Target)
	if err != nil {
		return err
	}
	emailHeaders, err := utils.ParseEmailHeaders(task.Extra)
	if err != nil {
		return err
	}
	return utils.SendEmail(emailTarget, emailHeaders, task.Data)
}

type InputAction struct{}

// Run is for satisfying the Action interface
func (a *InputAction) Run(e *Executor, task Task) error {
	scanner := bufio.NewScanner(os.Stdin)
	value := ""
	goon := true
	var result any
	for goon {
		if e.Stopped {
			return nil
		}
		fmt.Printf("%s", task.Data)
		scanner.Scan()
		if err := scanner.Err(); err != nil {
			return err
		}
		value = scanner.Text()
		if len(value) == 0 {
			fmt.Printf("empty input, try again\n")
			continue
		}
		if len(task.Extra) > 0 {
			var err error
			result, err = utils.Prepare(value, task.Extra)
			if err != nil {
				fmt.Printf("failed to prepare %s\n", task.Extra)
				continue
			}
		} else {
			result = value
		}
		goon = false
	}
	e.SetContextValue(task.Name, result)
	return nil
}

// CustomAction name is expressive
type CustomAction struct {
	f ActionFunc
}

// Run is for satisfying the Action interface
func (a *CustomAction) Run(e *Executor, task Task) error {
	return a.f(e, task)
}
