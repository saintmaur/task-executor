package executor

import (
	"fmt"
	"reflect"
	"regexp"
	"strings"
)

func validate(task Task) error {
	action, ok := actionTypes[task.Type]
	if !ok {
		return fmt.Errorf("unsupported action type: '%s'", task.Type)
	}
	for field, pattern := range action.Fields {
		value := reflect.ValueOf(task).FieldByName(strings.ToUpper(string(field[0])) + field[1:])
		re, err := regexp.Compile(pattern)
		if err != nil {
			return fmt.Errorf("failed on compiling the regexp (%s): %s", pattern, err)
		}
		if !re.MatchString(value.String()) {
			return fmt.Errorf("failed on validating the '%s' task: invalid field '%s': '%s' (pattern: '%s'). Too bad, skip the whole scenario", task.Type, field, value, pattern)
		}
	}
	return nil
}
