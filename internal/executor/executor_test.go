package executor

import (
	"fmt"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/thanhpk/randstr"
)

func TestExecutor_CheckStartupRun(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "check_startup_run",
		Period:         "1h",
		SkipStartupRun: false,
		Scenario: []Task{
			{Name: "result", Data: "print\n", Type: "print"},
			{Name: "result", Data: "1", Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		{
			e.Start()
			time.Sleep(time.Second)
			val := e.GetContextValue("result")
			e.Stop()
			if val == nil {
				t.Fatal("Should run")
			}
			e.DeleteContextValue("result")
		}
		if !e.Stopped {
			t.Fatal("Should be stopped")
		}
		{
			e.skipStartupRun = true
			e.Start()
			time.Sleep(time.Second)
			val := e.GetContextValue("result")
			e.Stop()
			if val != nil {
				t.Fatal("Should not run")
			}
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}
func TestExecutor_CheckClonedExecutorName(t *testing.T) {
	randName := randstr.String(10)
	tmpFile := "/tmp/executor_name"
	predefined := make(map[string]Scenario)
	predefined["run"] = Scenario{
		{Name: "file_write", Data: "{{.executor_name}}", Type: "file_write", Target: tmpFile},
	}
	executorConfig := ExecutorConfig{Name: "executor",
		Period:         "1h",
		SkipStartupRun: false,
		Predefined:     predefined,
		Scenario: []Task{
			{Name: "run", Target: "run", Type: "run", Extra: randName, Async: "true"},
			{Name: "wait 1s", Data: "1s", Type: "wait"},
			{Name: "result", Target: tmpFile, Type: "file_read"},
			{Name: "shell", Target: "rm " + tmpFile, Type: "shell"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		name := e.GetContextValue("result").(string)
		if name != randName {
			t.Fatalf("Names are distinct: \n\tactual: %s\n\texpected: %s\n", name, randName)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}

func TestExecutor_processTemplate(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "template",
		Period: "1m",
		Scenario: []Task{
			{Name: "one", Data: "1", Type: "set"},
			{Name: "three", Data: "3", Type: "set"},
			{Name: "result", Data: "{{.one}}-{{.one}}, 2 and {{.three}}", Type: "set"},
			{Name: "expected", Data: "1-1, 2 and 3", Type: "set"},
			{Name: "check", Data: "{{ if eq .result .expected }}1{{ else }}0{{ end }}", Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "check")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}

func TestExecutor_CheckContext(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "check_context",
		Period: "1m",
		Scenario: []Task{
			{Name: "one", Data: "_1_", Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		e.SetContextValue("{{.one}}", "one")
		result := e.GetContextValue("{{.one}}")
		if result == nil {
			fmt.Printf("%v+\n", e.Context())
			t.Fatalf("Failed to set context value by key '%s'", "_1_")
		}
		result = e.GetContextValue("{{._1_}}")
		if result == nil {
			fmt.Printf("%v+\n", e.Context())
			t.Fatalf("Failed to get context value by key '%s'", "one")
		}
		e.DeleteContextValue("{{.one}}")
		result = e.GetContextValue("{{.one}}")
		if result != nil {
			fmt.Printf("%v+\n", e.Context())
			t.Fatalf("Failed to delete context value by key '%s'", "{{.one}}")
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}

func TestExecutor_GlobalVars(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "global_vars",
		Period: "1m",
		Scenario: []Task{
			{Name: "one", Data: "1", Type: "set"},
		},
	}
	varsConfig := VariablesConfig{
		{Name: "second_one", Value: "1"},
	}
	if e := NewExecutor(executorConfig, varsConfig, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		one := e.GetContextValue("one")
		if one == nil {
			t.Fatal("one is nil")
		}
		secondOne := e.GetContextValue("second_one")
		if one == nil {
			t.Fatal("second one is nil")
		}
		if secondOne != one {
			t.Fatalf("Not equal:\n\tone: %v\n\tsecond one: %v", one, secondOne)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}
func TestExecutor_CheckRegisterTask(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "register_check",
		Period: "1m",
		Scenario: []Task{
			{Name: "one", Data: "1", Type: "set_value"},
		},
	}
	RegisterTask("set_value", func(e *Executor, task Task) error {
		e.SetContextValue(task.Name, task.Data)
		return nil
	})
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "one")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}

func TestExecutor_CheckTemplateFunctions(t *testing.T) {
	predefined := make(map[string]Scenario)
	predefined["inc"] = Scenario{
		{Name: "value", Data: "{{ index . (index .inc_arguments 0) }}", Type: "set", Extra: "float"},
		{Name: "value", Data: "value+1", Type: "calc"},
		{Name: "inc_return_value", Data: "{{ .value }}", Type: "set"},
	}
	predefined["/non-callable"] = Scenario{
		{Name: "/non-callable_return_value", Data: "1", Type: "set"},
	}
	executorConfig := ExecutorConfig{Name: "template_function",
		Predefined: predefined,
		Period:     "1m",
		Scenario: []Task{
			{Name: "init_value", Data: "10", Type: "set"},
			{Name: "final_value", Data: `{{ print (inc "init_value") }}`, Type: "set"},
			{Name: "expected", Data: "11", Type: "set"},
			{Name: "result", Data: "{{ if eq .expected .final_value}}1{{ else }}0{{ end }}", Type: "set"},
			{Name: "impossible", Data: `{{ /non-callable }}`, Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Start()
		time.Sleep(time.Second * 1)
		err := checkEqualResult(e, "result")
		if err != nil {
			e.Stop()
			t.Fatal(err)
		}
		err = checkEqualResult(e, "impossible")
		impossible := e.GetContextValue("impossible")
		if err == nil && impossible != nil {
			e.Stop()
			t.Fatal("Succeeded on calling a function in the template by the malformed name: ", impossible)
		}
		e.Stop()
	} else {
		t.Fatal("Faled to create an executor")
	}
}

func TestExecutor_CheckTask_EnvVar(t *testing.T) {
	predefined := make(map[string]Scenario)
	value := randstr.String(10)
	os.Setenv("TEST_VAR", value)
	executorConfig := ExecutorConfig{Name: "check_env_var",
		Predefined: predefined,
		Scenario: Scenario{
			{Name: "data", Data: "{{.TEST_VAR}}", Type: "set"},
			{Name: "result", Data: fmt.Sprintf(`{{ if eq "%s" .data}}1{{ else }}0{{ end }}`, value), Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}

func TestExecutor_CheckTimeout(t *testing.T) {
	predefined := make(map[string]Scenario)
	value := randstr.String(10)
	defaultValue := randstr.String(10)
	predefined["on_timeout_scenario"] = Scenario{
		{Type: "set", Name: "value", Data: value},
	}
	predefined["on_timeout_default_scenario"] = Scenario{
		{Type: "set", Name: "value", Data: defaultValue},
	}
	executorConfig := ExecutorConfig{Name: "check_timeout",
		Predefined: predefined,
		Defaults: DefaultsConfig{
			Timeout:   "1s",
			OnTimeout: "on_timeout_default_scenario",
		},
		Scenario: Scenario{
			{Name: "wait", Data: "5s", Type: "wait", Timeout: "100ms", OnTimeout: "on_timeout_scenario"},
			{Name: "result1", Data: fmt.Sprintf(`{{ if eq "%s" .value}}1{{ else }}0{{ end }}`, value), Type: "set"},
			{Name: "wait", Data: "5s", Type: "wait"},
			{Name: "result2", Data: fmt.Sprintf(`{{ if eq "%s" .value}}1{{ else }}0{{ end }}`, defaultValue), Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result1")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result2")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}

func TestExecutor_CheckParseHeaders(t *testing.T) {
	randStr := randstr.String(10)
	headers := fmt.Sprintf(`{"authorization": "%s", "content-type": "{{.text}}"}`, randStr)
	predefined := make(map[string]Scenario)
	executorConfig := ExecutorConfig{Name: "check_headers",
		Predefined: predefined,
		Scenario: Scenario{
			{Name: "wait", Data: "1s", Type: "wait"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{{Name: "type", Value: randStr}}, map[string]Scenario{}, statChan); e != nil {
		req, err := http.NewRequest("GET", "http://localhost", nil)
		if err != nil {
			t.Fatal("failed on creating a request")
		}
		err = e.parseAndAddHeaders(req, headers)
		if err != nil {
			t.Fatal("failed on parsing the headers: ", err)
		}
		if randStr != req.Header.Get("authorization") {
			t.Fatal("failed on setting the header value")
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}
