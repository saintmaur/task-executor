package executor

import (
	"bufio"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"reflect"
	"strings"
	"testing"
	"time"

	httpex "gitlab.com/saintmaur/lib/http"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/lib/stat"
	"gitlab.com/saintmaur/task-executor/internal/utils"

	"github.com/gliderlabs/ssh"
	"github.com/gorilla/websocket"
	"github.com/thanhpk/randstr"
	gossh "golang.org/x/crypto/ssh"
)

func TestAction_HTTP(t *testing.T) {
	startServer("2121", "/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("x-header", r.Method)
		w.WriteHeader(200)
		w.Write([]byte(r.Method))
	})
	executorConfig := ExecutorConfig{Name: "http_check",
		Period: "1m",
		Scenario: []Task{
			{Name: "url", Type: "set", Data: "http://localhost:2121/"},
			{Name: "http_get", Target: "{{.url}}", Type: "http", Method: "get", Headers: `{"Content-Type": "application/text"}`},
			{Name: "result_get", Type: "set", Data: `{{if eq .http_get "GET"}}1{{else}}0{{end}}`},
			{Name: "http_post", Data: "data", Target: "{{.url}}", Type: "http", Method: "post", Headers: `{"Content-Type": "application/text"}`},
			{Name: "result_post", Type: "set", Data: `{{if eq .http_post "POST"}}1{{else}}0{{end}}`},
			{Name: "http_put", Data: "data", Target: "{{.url}}", Type: "http", Method: "put", Headers: `{"Content-Type": "application/text"}`},
			{Name: "result_put", Type: "set", Data: `{{if eq .http_put "PUT"}}1{{else}}0{{end}}`},
			{Name: "http_patch", Data: "data", Target: "{{.url}}", Type: "http", Method: "patch", Headers: `{"Content-Type": "application/text"}`},
			{Name: "result_patch", Type: "set", Data: `{{if eq .http_patch "PATCH"}}1{{else}}0{{end}}`},
			{Name: "http_delete", Target: "{{.url}}", Type: "http", Method: "delete", Headers: `{"Content-Type": "application/text"}`},
			{Name: "result_delete", Type: "set", Data: `{{if eq .http_delete "DELETE"}}1{{else}}0{{end}}`},
			{Name: "http_head", Target: "{{.url}}", Type: "http", Method: "head", Headers: `{"Content-Type": "application/text"}`},
			{Name: "result_head", Type: "set", Data: `{{if eq (index (index .http_head_headers "X-Header") 0) "HEAD"}}1{{else}}0{{end}}`},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result_get")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result_post")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result_put")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result_patch")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result_delete")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result_head")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}
func TestAction_Shell(t *testing.T) {
	tempFileName := "/tmp/temp file.file"
	expected := "test"
	executorConfig := ExecutorConfig{Name: "shell",
		Period: "1m",
		Scenario: []Task{
			{Name: "filename", Type: "set", Data: tempFileName},
			{Name: "input", Type: "set", Data: expected},
			{Name: "create_file", Target: `printf "$INPUT_DATA" > "$FILENAME"`, Data: `{"INPUT_DATA": "{{.input}}", "FILENAME": "{{.filename}}"}`, Type: "shell"},
			{Name: "check", Target: `cat "$FILENAME"`, Data: `{"FILENAME": "{{.filename}}"}`, Type: "shell"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		actual, err := utils.PrepareString(e.GetContextValue("check"))
		if err != nil {
			t.Fatal(err)
		}
		if actual != expected {
			t.Fatalf("Failed to write or read a file:\n\tactual: %s\n\texpected: %s", actual, expected)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
	executorConfig.Scenario = []Task{
		{Name: "filename", Type: "set", Data: tempFileName},
		{Name: "delete_file", Target: `rm "$FILENAME"`, Data: `{"FILENAME": "{{.filename}}"}`, Type: "shell"},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
	} else {
		t.Fatal("Faled to create an executor")
	}
	if _, err := os.Stat(tempFileName); !os.IsNotExist(err) {
		t.Fatal("File was not deleted")
	}
}

func TestAction_RemoteShell(t *testing.T) {
	go runSSHServer()
	keyData := `
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAaAAAABNlY2RzYS
1zaGEyLW5pc3RwMjU2AAAACG5pc3RwMjU2AAAAQQQ00yYWBrIhS82xZIn3fjFRL/1elryk
RMQbe0ZbY0ECbGGgZG5GceTDkbia6N8POhtjcy6W78D8g2IIuX1EwfbYAAAAuPtJ9aj7Sf
WoAAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBDTTJhYGsiFLzbFk
ifd+MVEv/V6WvKRExBt7RltjQQJsYaBkbkZx5MORuJro3w86G2NzLpbvwPyDYgi5fUTB9t
gAAAAgLbJWGTu14bLgEJ6SvoNkkrZlkPYMOPZuxmUIkarxdNwAAAAac2V5bW91ckBzYWlu
dG1hdXJpLTIubG9jYWwBAgMEBQY=
-----END OPENSSH PRIVATE KEY-----
	`
	executorConfig := ExecutorConfig{Name: "remote_shell",
		Period: "1m",
		Scenario: []Task{
			{Name: "file_write", Type: "file_write", Data: keyData, Target: "/tmp/key"},
			{Name: "remote_result", Target: "date",
				Data: `{"host": "localhost", "port": "2222", "user": "user", "key": "/tmp/key"}`,
				Type: "remote_shell", Extra: `{"VAR": "VAL"}`},
			{Name: "file_remove", Type: "shell", Target: "rm /tmp/key"},
			{Data: "remote_result: {{.remote_result}}", Type: "print"},
			{Name: "result", Type: "set", Data: "{{if .remote_result}}1{{else}}0{{end}}"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}

func TestAction_JSONGet(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "json_get_checker",
		Period: "1m",
		Scenario: []Task{
			{Name: "json", Type: "set", Data: `{"string": "string", "int": 3, "float": 3.14, "bool": true}`},
			{Name: "value0", Type: "json_get", Data: "{{.json}}", Target: "string"},
			{Name: "result0", Type: "set", Data: `{{if eq (typeOf .value0) "gjson.Result"}}1{{else}}0{{end}}`, Target: "string"},
			{Name: "value1", Type: "json_get", Data: "{{.json}}", Target: "string", Extra: "string"},
			{Name: "result1", Type: "set", Data: `{{if eq (typeOf .value1) "string"}}1{{else}}0{{end}}`, Target: "string"},
			{Name: "value2", Type: "json_get", Data: "{{.json}}", Target: "int", Extra: "int"},
			{Name: "result2", Type: "set", Data: `{{if eq (typeOf .value2) "int64"}}1{{else}}0{{end}}`},
			{Name: "value3", Type: "json_get", Data: "{{.json}}", Target: "float", Extra: "float"},
			{Name: "result3", Type: "set", Data: `{{if eq (typeOf .value3) "float64"}}1{{else}}0{{end}}`},
			{Name: "value4", Type: "json_get", Data: "{{.json}}", Target: "bool", Extra: "bool"},
			{Name: "result4", Type: "set", Data: `{{if eq (typeOf .value4) "bool"}}1{{else}}0{{end}}`},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result0")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result1")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result2")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result3")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result4")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}

func TestAction_SetMapValue(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "set_map_value_checker",
		Period: "1m",
		Scenario: []Task{
			{Name: "raw_map", Type: "set", Data: "{}"},
			{Name: "index", Type: "set", Data: "abra.kadabra"},
			{Name: "value", Type: "set", Data: "voila"},
			{Name: "target_map", Target: "{{.index}}", Type: "set_map_value", Data: "value"},
			{Name: "actual", Type: "set", Data: "{{ .target_map.abra.kadabra }}"},
			{Name: "result", Type: "set", Data: "{{ if eq .value .actual }}1{{ else }}0{{ end }}"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}

func TestAction_JSONSet(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "json_set_checker",
		Period: "1m",
		Scenario: []Task{
			{Name: "name", Type: "set", Data: "actual"},
			{Name: "data", Type: "set", Data: "value1"},
			{Name: "path", Type: "set", Data: "key"},
			{Name: "actual", Type: "json_set", Data: "", Target: "{{.path}}", Extra: "data"},
			{Name: "expected", Type: "set", Data: `{"key":"value1"}`},
			{Name: "result1", Type: "set", Data: "{{ if eq .expected .actual }}1{{ else }}0{{ end }}"},
			{Name: "data", Type: "set", Data: "value2"},
			{Name: "{{.name}}", Type: "json_set", Data: "{{.actual}}", Target: "{{.path}}", Extra: "data"},
			{Name: "expected", Type: "set", Data: `{"key":"value2"}`},
			{Name: "expected", Type: "print", Data: `expected: {{.expected}}`},
			{Name: "expected", Type: "print", Data: `actual: {{.actual}}`},
			{Name: "result2", Type: "set", Data: "{{ if eq .expected .actual }}1{{ else }}0{{ end }}"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result1")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result2")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_JSONDelete(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "json_delete_checker",
		Period: "1m",
		Scenario: []Task{
			{Name: "data", Type: "set", Data: `[["0","1"],["1","2"],["2","3"]]`},
			{Name: "actual", Type: "json_delete", Data: "{{.data}}", Target: "1"},
			{Name: "expected", Type: "set", Data: `[["0","1"],["2","3"]]`},
			{Name: "result1", Type: "set", Data: "{{ if eq .expected .actual }}1{{ else }}0{{ end }}"},
			{Name: "data", Type: "set", Data: `{"0":"1","1":"2","2":"3"}`},
			{Name: "actual", Type: "json_delete", Data: "{{.data}}", Target: "2"},
			{Name: "expected", Type: "set", Data: `{"0":"1","1":"2"}`},
			{Name: "result2", Type: "set", Data: "{{ if eq .expected .actual }}1{{ else }}0{{ end }}"},
			{Name: "data", Type: "set", Data: `{"code":"200000","data":{"time":1662129178895,"sequence":"1615955958001","bids":[["0.08061","1.4098486"],["0.080608","0.51"],["0.080607","0.9692114"]]}}`},
			{Name: "actual", Type: "json_delete", Data: "{{.data}}", Target: "data.bids.2"},
			{Name: "expected", Type: "set", Data: `{"code":"200000","data":{"time":1662129178895,"sequence":"1615955958001","bids":[["0.08061","1.4098486"],["0.080608","0.51"]]}}`},
			{Name: "result3", Type: "set", Data: "{{ if eq .expected .actual }}1{{ else }}0{{ end }}"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result1")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result2")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result3")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}

func TestAction_Replace(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "replace_checker",
		Period: "1m",
		Scenario: []Task{
			{Name: "data", Type: "set", Data: "unreal"},
			{Name: "actual", Type: "replace", Data: "{{.data}}", Target: "un", Extra: ""},
			{Name: "expected", Type: "set", Data: "real"},
			{Name: "result", Type: "set", Data: "{{ if eq .expected .actual }}1{{ else }}0{{ end }}"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_ReplaceRegexp(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "replace_regexp",
		Period: "1m",
		Scenario: []Task{
			{Name: "actual", Type: "replace_regexp", Data: "world hello", Target: "([a-z]+) ([a-z]+)", Extra: "$2, $1"},
			{Name: "expected", Type: "set", Data: "hello, world"},
			{Name: "result", Type: "set", Data: "{{ if eq .expected .actual }}1{{ else }}0{{ end }}"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_Intern(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "intern_checker",
		Period: "1m",
		Scenario: []Task{
			{Name: "json_str", Type: "set", Data: "{\"key\":\"value\", \"sorting_key\":{\"key\":1}}"},
			{Name: "interned", Type: "intern", Data: "{{.json_str}}"},
			{Name: "actual", Type: "set", Data: "{{.interned.key}}"},
			{Name: "expected", Type: "set", Data: "value"},
			{Name: "result", Type: "set", Data: "{{ if eq .expected .actual }}1{{ else }}0{{ end }}"},
			{Name: "nil_value", Type: "intern", Data: "non-existent"},
			{Name: "non_changable", Type: "set", Data: "1"},
			{Name: "non_changable", Type: "set", Data: "2", SkipCond: "not .nil_value"},
			{Name: "json_arr_str", Type: "set", Data: "[\"key\",\"value\",\"sorting_key\"]"},
			{Name: "interned_arr", Type: "intern", Data: "{{.json_arr_str}}"},
			{Name: "arr_len_is_correct", Type: "set", Data: "{{ if eq (len .interned_arr) 3 }}1{{ else }}0{{ end }}"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "non_changable")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "arr_len_is_correct")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}

func TestAction_RunLoop(t *testing.T) {
	predefined := make(map[string]Scenario)
	expected := "1"
	predefined["subtask"] = Scenario{
		{Type: "set", Name: "sub_sub_item", Data: expected},
	}
	executorConfig := ExecutorConfig{Name: "runloop_checker",
		Period:     "1m",
		Predefined: predefined,
		Scenario: []Task{
			{Name: "arr_str", Type: "set", Data: fmt.Sprintf("{\"arr_key\":[\"%s\"]}", expected)},
			{Name: "path", Type: "set", Data: "arr_key"},
			{Name: "arr", Target: "{{.path}}", Type: "json_get", Data: "{{.arr_str}}"},
			{Name: "name", Data: "subtask", Type: "set"},
			{Name: "sub", Target: "{{.name}}", Type: "loop", Data: "arr"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "sub_sub_item")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "sub_item")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_RunLoopStop(t *testing.T) {
	predefined := make(map[string]Scenario)
	predefined["subtask"] = Scenario{
		{Type: "wait", Name: "3s", Data: "3s"},
		{Type: "set", Name: "value", Data: "value"},
	}
	executorConfig := ExecutorConfig{Name: "runloop_checker",
		Period:     "1m",
		Predefined: predefined,
		Scenario: []Task{
			{Name: "arr", Type: "intern", Data: `[1,2,3]`},
			{Name: "sub", Target: "subtask", Type: "loop", Data: "arr"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		go e.Run()
		time.Sleep(time.Second)
		e.StopRepeated()
		value := e.GetContextValue("value")
		if value != nil {
			t.Fatal("Should not run")
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_RunLoopStopCondition(t *testing.T) {
	predefined := make(map[string]Scenario)
	predefined["subtask"] = Scenario{
		{Type: "calc", Name: "value", Data: "value+1"},
	}
	executorConfig := ExecutorConfig{Name: "runloop_checker",
		Period:     "1m",
		Predefined: predefined,
		Scenario: []Task{
			{Name: "value", Type: "calc", Data: `1`},
			{Name: "arr", Type: "intern", Data: `[1,2,3]`},
			{Name: "sub", Target: "subtask", Type: "loop", Data: "arr", Extra: "true"},
			{Name: "result", Type: "set", Data: "{{if eq (int .value) 2}}1{{else}}0{{end}}"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_Inject(t *testing.T) {
	// t.Skip("Skipping unready functionality")
	predefined := make(map[string]Scenario)
	data := `
set1:
- type: set
  name: item1
  data: 1
set2:
- type: set
  name: item2
  data: 1`
	filename := "sc.yaml"
	file, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		t.Fatal(err)
	}
	defer file.Close()
	_, err = file.Write([]byte(data))
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(filename)
	executorConfig := ExecutorConfig{Name: "inject_checker",
		Period:     "1m",
		Predefined: predefined,
		Scenario: []Task{
			{Name: "inject_data", Data: data, Type: "set"},
			{Name: "name", Data: "injected", Type: "set"},
			{Name: "injected1", Type: "inject", Data: "{{.inject_data}}", Format: "yaml"},
			{Name: "run", Type: "run", Target: "set1"},
			{Name: "injected1", Type: "inject", Target: filename, Format: "yaml"},
			{Name: "run", Type: "run", Target: "set2"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "item1")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "item2")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_Pipe(t *testing.T) {
	predefined := make(map[string]Scenario)
	expected := "piped-value"
	predefined["pipe_scenario"] = Scenario{
		{Type: "set", Name: "result", Data: "{{ if eq .expected .pipe_item }}1{{ else }}0{{ end }}"},
	}
	executorConfig := ExecutorConfig{Name: "pipe_checker",
		Period:     "1m",
		Predefined: predefined,
		Scenario: []Task{
			{Name: "expected", Data: expected, Type: "set"},
			{Name: "pipe", Target: "pipe_scenario", Type: "pipe"},
			{Name: "pipe", Target: "pipe", Data: expected, Type: "pipe"},
			{Name: "wait", Data: "1s", Type: "wait"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		e.StopRepeat("pipe")
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_Websocket(t *testing.T) {
	expected := "pong"
	srv := httpex.New(&httpex.ServerConfig{
		Listen: ":2020",
	})
	if srv == nil {
		t.Fatal("Failed on creating an HTTP server. Stopping")
	}
	srv.AddWSHandler("/", func(msg []byte, mt int, c *websocket.Conn) {
		err := c.WriteMessage(websocket.TextMessage, []byte(expected))
		if err != nil {
			logger.Warnf("write error: %s", err)
		}
	})
	srv.Run()
	defer srv.Stop()
	predefined := make(map[string]Scenario)
	predefined["process"] = Scenario{
		{Type: "print", Name: "pipe_item", Data: "{{.pipe_item}}"},
		{Type: "set", Name: "result", Data: "{{ if eq .pipe_item .expected }}1{{ else }}0{{ end }}"},
	}
	executorConfig := ExecutorConfig{Name: "pipe_checker",
		Period:     "1m",
		Predefined: predefined,
		Scenario: []Task{
			{Name: "expected", Data: expected, Type: "set"},
			{Name: "pipe", Target: "process", Type: "pipe"},
			{Name: "ws", Target: "ws://localhost:2020", Data: "pipe", Type: "websocket"},
			{Name: "wait", Data: "3s", Type: "wait"},
			{Name: "pipe", Target: "ws_pipe", Data: "ping", Type: "pipe"},
			{Name: "wait", Data: "3s", Type: "wait"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		e.StopRepeat("pipe")
		e.StopRepeat("ws")
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_RunSub(t *testing.T) {
	predefined := make(map[string]Scenario)
	expected := "1"
	predefined["subtask"] = Scenario{
		{Type: "set", Name: "sub_sub_item", Data: expected},
	}
	executorConfig := ExecutorConfig{Name: "run_checker",
		Period:     "1m",
		Predefined: predefined,
		Scenario: []Task{
			{Name: "name", Data: "subtask", Type: "set"},
			{Name: "sub", Target: "{{.name}}", Type: "run"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "sub_sub_item")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}

func TestAction_While(t *testing.T) {
	predefined := make(map[string]Scenario)
	predefined["subtask"] = Scenario{
		{Type: "calc", Name: "actual", Data: "actual+1"},
	}
	executorConfig := ExecutorConfig{Name: "while_checker",
		Period:     "1m",
		Predefined: predefined,
		Scenario: []Task{
			{Name: "actual", Data: "0", Type: "set", Extra: "float"},
			{Name: "max", Data: "3", Type: "set", Extra: "float"},
			{Name: "while", Target: "subtask", Data: "lt .actual .max", Type: "while"},
			{Name: "result", Data: "1", Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}

func TestAction_WhileStop(t *testing.T) {
	predefined := make(map[string]Scenario)
	predefined["subtask"] = Scenario{
		{Type: "calc", Name: "actual", Data: "actual"},
	}
	executorConfig := ExecutorConfig{Name: "runloop_checker",
		Period:     "1m",
		Predefined: predefined,
		Scenario: []Task{
			{Name: "actual", Data: "0", Type: "set", Extra: "float"},
			{Name: "max", Data: "3", Type: "set", Extra: "float"},
			{Name: "while", Target: "subtask", Data: "lt .actual .max", Type: "while"},
			{Name: "result", Data: "1", Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		go e.Run()
		time.Sleep(time.Second)
		e.StopRepeated()
		time.Sleep(time.Second)
		value := e.GetContextValue("result")
		if value == nil {
			t.Fatal("Should exit")
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}

func TestAction_Localize(t *testing.T) {
	expected := "тест-тест-тест"
	executorConfig := ExecutorConfig{Name: "localize",
		Period: "1m",
		Scenario: []Task{
			{Type: "set", Name: "locales_path", Data: "../../configs/tasker/locales/"},
			{Type: "set", Name: "locale", Data: "ru"},
			{Type: "localize", Name: "localized", Data: "test", Extra: "{{.locales_path}}", Target: "{{.locale}}"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		actual, err := utils.PrepareString(e.GetContextValue("localized"))
		if err != nil {
			t.Fatal(err)
		}
		if actual != expected {
			t.Fatalf("Translation has failed:\n\texpected: %s\n\tactual: %s", expected, actual)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_Template(t *testing.T) {
	executorConfig := ExecutorConfig{
		Name:   "template",
		Period: "1m",
		Scenario: []Task{
			{Type: "set", Name: "one", Data: `1`},
			{Type: "set", Name: "two", Data: `2`},
			{Type: "set", Name: "expected", Data: `1+2`},
			{Type: "set", Name: "template", Data: `{{.one}}+{{.two}}`},
			{Type: "template", Name: "actual", Data: "{{.template}}"},
			{Type: "set", Name: "result", Data: "{{ if eq .expected .actual }}1{{ else }}0{{ end }}"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_Cron(t *testing.T) {
	predefined := make(map[string]Scenario)
	predefined["repeat"] = Scenario{
		{Type: "calc", Name: "actual", Data: "actual+1"},
	}
	executorConfig := ExecutorConfig{Name: "repeated",
		Period:     "1m",
		Predefined: predefined,
		Scenario: []Task{
			{Type: "set", Name: "expected", Data: "2", Extra: "float"},
			{Type: "set", Name: "actual", Data: "0", Extra: "float"},
			{Type: "cron", Name: "repeat", Data: "* * * * * *", Target: "repeat"},
			{Type: "wait", Name: "wait", Data: "3s"},
			{Type: "print", Name: "debug", Data: "{{.actual}}"},
			{Type: "set", Name: "result1", Data: "{{ if ge .actual .expected }}1{{ else }}0{{ end }}"},
			{Type: "break", Name: "repeat"},
			{Type: "set", Name: "actual", Data: "0", Extra: "float"},
			{Type: "cron", Name: "repeat", Data: "1s", Target: "repeat"},
			{Type: "wait", Name: "wait", Data: "4s"},
			{Type: "print", Name: "debug", Data: "{{.actual}}"},
			{Type: "set", Name: "result2", Data: "{{ if ge .actual .expected }}1{{ else }}0{{ end }}"},
			{Type: "break", Name: "repeat"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result1")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result2")
		if err != nil {
			t.Fatal(err)
		}
		if len(e.repeated) > 0 {
			t.Fatal("Not deleted")
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_Regexp(t *testing.T) {
	template := `{{ index (index .matches 0) 1 }}`
	executorConfig := ExecutorConfig{Name: "arrval_checker",
		Period: "1m",
		Scenario: []Task{
			{Name: "str", Type: "set", Data: "get_exchanges, size=74, pending=10"},
			{Name: "pattern", Type: "set", Data: "size=(\\d+)"},
			{Name: "matches", Type: "regexp", Target: "{{.pattern}}", Data: "{{.str}}"},
			{Name: "value", Data: template, Type: "set"},
			{Name: "expected", Type: "set", Data: "74"},
			{Name: "result", Type: "set", Data: "{{ if eq .expected .value }}1{{ else }}0{{ end }}"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		expected := int64(1)
		actual, err := utils.PrepareInt(e.GetContextValue("result"))
		if err != nil {
			t.Fatal(err)
		}
		if actual != expected {
			t.Fatalf("Not equal: %+v", e.Context())
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_DBActions(t *testing.T) {
	dsn := "test.sqlite"
	executorConfig := ExecutorConfig{Name: "dbinsert_checker",
		Period: "1m",
		Before: []Task{
			{Name: "init", Target: dsn, Type: "db_exec", Data: "create table test (id integer, name varchar(50))", Extra: "sqlite3"},
		},
		Scenario: []Task{
			{Name: "insert_query", Type: "set", Data: `insert into test (id,name) values (1,'test')`},
			{Name: "insert", Type: "db_exec", Target: dsn, Data: "{{.insert_query}}", Extra: "sqlite3"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.RunScenario(e.Before, fmt.Sprintf("%d", time.Now().UnixNano()))
		e.RunScenario(e.Scenario, fmt.Sprintf("%d", time.Now().UnixNano()))
	} else {
		t.Fatal("Failed to create an executor")
	}
	executorConfig = ExecutorConfig{Name: "dbselect_checker",
		Period: "1m",
		After: []Task{
			{Name: "cleanup", Target: dsn, Type: "db_exec", Data: "drop table test", Extra: "sqlite3"},
			{Name: "delete the DB file", Target: fmt.Sprintf("rm %s", dsn), Type: "shell"},
		},
		Scenario: []Task{
			{Name: "select_query", Type: "set", Data: "select *, count(*) count, trim(name) trim from test where id = 1"},
			{Name: "db_data", Type: "db_select", Target: dsn, Data: "{{.select_query}}", Extra: "sqlite3"},
			{Type: "set", Name: "actual", Data: `{{ index (index .db_data 0) "name"}}`},
			{Name: "expected", Type: "set", Data: "test"},
			{Name: "db_data_count", Type: "set", Data: "{{len .db_data}}"},
			{Name: "count", Type: "set", Data: `{{index (index .db_data 0) "count"}}`},
			{Name: "result", Type: "set", Data: "{{ if eq .actual .expected }}1{{ else }}0{{ end }}"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.RunScenario(e.Scenario, fmt.Sprintf("%d", time.Now().UnixNano()))
		e.RunScenario(e.After, fmt.Sprintf("%d", time.Now().UnixNano()))
		err := checkEqualResult(e, "db_data_count")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "count")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_Calc(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "calculator",
		Period: "1m",
		Scenario: []Task{
			{Name: "ten", Data: "10", Type: "set", Extra: "float"},
			{Name: "expected", Data: "0", Type: "set", Extra: "float"},
			{Name: "actual", Data: "(ten/2)%5", Type: "calc"},
			{Name: "result", Data: "{{ if eq .actual .expected}}1{{ else }}0{{ end }}", Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal("Failed on '(ten/2)%5': ", err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}
func TestAction_ReadFile(t *testing.T) {
	filename := "/tmp/file.txt"
	executorConfig := ExecutorConfig{Name: "closer",
		Period: "1m",
		Scenario: []Task{
			{Name: "text", Data: "text", Type: "set"},
			{Name: "filename", Type: "set", Data: filename},
			{Name: "save", Target: "{{.filename}}", Type: "file_write", Data: "{{.text}}"},
			{Name: "readData", Target: "{{.filename}}", Type: "file_read"},
			{Name: "delete_file", Target: "rm $FILENAME", Data: `{"FILENAME": "{{.filename}}"}`, Type: "shell"},
			{Name: "result", Type: "set", Data: "{{ if eq .readData .text }}1{{ else }}0{{ end }}"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		expected, err := utils.PrepareInt(1)
		if err != nil {
			t.Fatal(err)
		}
		actual, err := utils.PrepareInt(e.GetContextValue("result"))
		if err != nil {
			t.Fatal(err)
		}
		if expected != actual {
			t.Fatalf("Values are not equal:\n\tactual: %v\n\texpected: %v", actual, expected)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}
func TestAction_Wait(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "waiter",
		Period: "1m",
		Scenario: []Task{
			{Name: "doit", Data: "3s", Type: "wait"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		started := time.Now()
		e.Run()
		finished := time.Now()
		if finished.Sub(started) < 3*time.Second {
			t.Fatal("Wait has failed")
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_Time(t *testing.T) {
	tm, err := time.Parse("02.01.2006T15:04:05", "01.01.1970T00:00:01")
	if err != nil {
		t.Fatal(err)
	}
	executorConfig := ExecutorConfig{Name: "time",
		Period: "1m",
		Scenario: []Task{
			{Name: "format", Type: "set", Data: "dd.mm.yyyyThh:MM:ss"},
			{Name: "dt", Data: "01.01.1970T00:00:01", Type: "set"},
			{Name: "timestamp", Data: "{{.dt}}", Format: "{{.format}}", Type: "time"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		rawTimestamp := e.GetContextValue("timestamp")
		if rawTimestamp == nil {
			t.Fatal("No 'timestamp' value found")
		}
		if tm.UnixNano() != rawTimestamp.(int64) {
			t.Fatalf("Timestamps are different:\n\t%v\n\t%v", tm.UnixNano(), rawTimestamp)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_TimeShift(t *testing.T) {
	tm1, err := time.Parse("02.01.2006T15:04:05", "01.01.1970T00:00:10")
	if err != nil {
		t.Fatal(err)
	}
	tm2, err := time.Parse("02.01.2006T15:04:05", "01.01.1970T00:00:01")
	if err != nil {
		t.Fatal(err)
	}
	executorConfig := ExecutorConfig{Name: "time_shift",
		Period: "1m",
		Scenario: []Task{
			{Name: "format", Type: "set", Data: "dd.mm.yyyyThh:MM:ss"},
			{Name: "dt", Data: "01.01.1970T00:00:01", Type: "set"},
			{Name: "duration", Data: "9s", Type: "set"},
			{Name: "timestamp1", Data: "{{.dt}}", Format: "{{.format}}", Type: "time"},
			{Name: "timestamp2", Data: "{{.timestamp1}}", Duration: "{{.duration}}", Type: "time_shift"},
			{Name: "duration", Data: "-9s", Type: "set"},
			{Name: "timestamp3", Data: "{{.timestamp2}}", Duration: "{{.duration}}", Type: "time_shift"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		rawTimestamp := e.GetContextValue("timestamp2")
		if rawTimestamp == nil {
			t.Fatal("No 'timestamp2' value found")
		}
		if tm1.UnixNano() != rawTimestamp.(int64) {
			t.Fatalf("Timestamps are different:\n\t%v\n\t%v", tm1.UnixNano(), rawTimestamp)
		}
		rawTimestamp = e.GetContextValue("timestamp3")
		if rawTimestamp == nil {
			t.Fatal("No 'timestamp3' value found")
		}
		if tm2.UnixNano() != rawTimestamp.(int64) {
			t.Fatalf("Timestamps are different:\n\t%v\n\t%v", tm2.UnixNano(), rawTimestamp)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_TimeFormat(t *testing.T) {
	tm, err := time.Parse("02.01.2006T15:04:05", "01.01.1970T00:00:10")
	if err != nil {
		t.Fatal(err)
	}
	executorConfig := ExecutorConfig{Name: "time_format",
		Period: "1m",
		Scenario: []Task{
			{Name: "format1", Type: "set", Data: "dd.mm.yyyyThh:MM:ss"},
			{Name: "dt", Data: "01.01.1970T00:00:01", Type: "set"},
			{Name: "format2", Data: "ddmmyyyy", Type: "set"},
			{Name: "timestamp", Data: "{{.dt}}", Format: "{{.format1}}", Type: "time"},
			{Name: "formatted", Data: "{{.timestamp}}", Format: "{{.format2}}", Type: "time_format"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		formatted := e.GetContextValue("formatted")
		if formatted == nil {
			t.Fatal("No 'formatted' value found")
		}
		if tm.Format("02012006") != formatted.(string) {
			t.Fatalf("Timestamps are different:\n\t%v\n\t%v", tm.Format("02012006"), formatted)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}

func TestAction_StopOnError(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "stopper",
		Period: "1m",
		Scenario: []Task{
			{Name: "url", Type: "set", Data: "https://non-existing"},
			{Name: "get", Target: "{{.url}}", Type: "http", Method: "get", StopOnError: "true"},
			{Name: "check", Type: "set", Data: "unreachable"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		if e.GetContextValue("check") != nil {
			t.Fatal("Stop has failed")
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_OnError(t *testing.T) {
	randStr := randstr.String(20)
	predefined := make(map[string]Scenario)
	predefined["on_error"] = Scenario{
		{Name: "value", Data: "1", Type: "set"},
	}
	predefined["on_error_default"] = Scenario{
		{Name: "value", Data: "2", Type: "set"},
	}
	executorConfig := ExecutorConfig{Name: "stopper",
		Predefined: predefined,
		Defaults: DefaultsConfig{
			OnError: "on_error_default",
		},
		Period: "1m",
		Scenario: []Task{
			{Name: "url", Type: "set", Data: "https://non-existing"},
			{Name: "get", Target: "{{.url}}", Type: "http", Method: "get", OnError: "on_error"},
			{Name: "result1", Data: `{{ if eq .value "1"}}1{{ else }}0{{ end }}`, Type: "set"},
			{Name: "url", Type: "set", Data: "https://non-existing"},
			{Name: "get", Target: "{{.url}}", Type: "http", Method: "get"},
			{Name: "result2", Data: `{{ if eq .value "2"}}1{{ else }}0{{ end }}`, Type: "set"},
			{Name: "hundred", Data: "100", Type: "set", OnError: "on_error", ErrorIf: `eq .hundred "100"`, ErrorMessage: randStr},
			{Name: "result3", Data: fmt.Sprintf(`{{ if eq (toString .hundred_result) "%s"}}1{{ else }}0{{ end }}`, randStr), Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result1")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result2")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result3")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_CSVParse(t *testing.T) {
	data := `id,name
1,one
2,two
3,three`
	dataTabbed := `id	name
1	one
2	two
3	three`
	executorConfig := ExecutorConfig{Name: "csv_parser",
		Period: "1m",
		Scenario: []Task{
			{Name: "parsed1", Data: data, Type: "csv"},
			{Name: "value", Type: "set", Data: `{{ index (index .parsed1 2) "name" }}`},
			{Name: "expected", Type: "set", Data: "three"},
			{Name: "result1", Type: "set", Data: "{{if eq .value .expected}}1{{ else }}0{{ end }}"},
			{Name: "parsed2", Data: dataTabbed, Type: "csv", Extra: "\t"},
			{Name: "value", Type: "set", Data: `{{ index (index .parsed2 1) "name" }}`},
			{Name: "expected", Type: "set", Data: "two"},
			{Name: "result2", Type: "set", Data: "{{if eq .value .expected}}1{{ else }}0{{ end }}"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result1")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result2")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}

func TestAction_CheckShell(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "shell-checker",
		Period: "1m",
		Scenario: []Task{
			{Name: "shell_1", Target: "mkdir double", Type: "shell"},
			{Name: "result_1", Data: "{{ if gt .shell_1_exit_code 0 }}0{{ else }}1{{ end }}", Type: "set"},
			{Name: "shell_2", Target: "mkdir double", Type: "shell"},
			{Name: "result_2", Data: "{{ if gt .shell_2_exit_code 0 }}1{{ else }}0{{ end }}", Type: "set"},
			{Name: "shell_3", Target: "rm -rf double", Type: "shell"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result_1")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result_2")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}
func TestAction_Skipper(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "skip_checker",
		Period: "1m",
		Scenario: []Task{
			{Name: "value", Data: "1", Type: "set"},
			{Name: "value", Data: "2", Type: "set", SkipCond: `eq .value "1"`},
			{Name: "expected", Data: "1", Type: "set"},
			{Name: "result", Data: "{{if eq .value .expected }}1{{ else }}0{{ end }}", Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}
func TestAction_Unset(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "unset_checker",
		Period: "1m",
		Scenario: []Task{
			{Name: "data", Data: "data", Type: "set"},
			{Name: "data", Type: "unset"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		data := e.GetContextValue("data")
		if data != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}

func TestAction_CheckRun(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "check_run",
		Before: []Task{
			{Name: "set_before", Data: "1", Type: "set", Extra: "float"},
		},
		After: []Task{
			{Name: "set_after", Data: "1", Type: "set"},
		},
		Period: "1m",
		Scenario: []Task{
			{Name: "expected", Data: "2", Type: "set", Extra: "float"},
			{Name: "actual", Data: "1+set_before", Type: "calc"},
			{Name: "result", Data: "{{ if eq .actual .expected }}1{{ else }}0{{ end }}", Type: "set"},
			{Name: "wait", Data: "3s", Type: "wait"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Start()
		time.Sleep(time.Second)
		err := checkEqualResult(e, "result")
		if err != nil {
			e.Stop()
			t.Fatal(err)
		}
		e.Stop()
		err = checkEqualResult(e, "set_after")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}
func TestAction_CheckAsync(t *testing.T) {
	predefined := make(map[string]Scenario)
	predefined["run_async"] = Scenario{
		{Name: "wait", Data: "3s", Type: "wait"},
	}
	executorConfig := ExecutorConfig{Name: "check_async",
		Predefined: predefined,
		Period:     "1m",
		Scenario: []Task{
			{Name: "run", Target: "run_async", Type: "run", Async: "true"},
			{Name: "result", Data: "1", Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Start()
		time.Sleep(time.Second * 1)
		err := checkEqualResult(e, "result")
		if err != nil {
			e.Stop()
			t.Fatal(err)
		}
		e.Stop()
	} else {
		t.Fatal("Faled to create an executor")
	}
}

func TestAction_Sign(t *testing.T) {
	signed1 := "93b14bc4741a014c4a3976b7f16da76a"
	signed2 := "c5a6c616dba00d6e7fdb22cc29ed952b579981b1883aba6c533e518b33d888ea"
	signed3 := "ff2f5a81910f42ca9ee641482223a8febfcabc0706b8e5e6437212ec842c76d3047cf64fead89cde1fd2478d5e1e9ae7557514bc0358cedf8ce61f26d97283f6"
	executorConfig := ExecutorConfig{Name: "_checker",
		Period: "1m",
		Scenario: []Task{
			{Name: "signed", Extra: "secret", Method: "md5", Data: "10", Type: "sign"},
			{Name: "result1", Data: fmt.Sprintf(`{{ if eq "%s" .signed}}1{{ else }}0{{ end }}`, signed1), Type: "set"},
			{Name: "signed", Extra: "secret", Method: "sha256", Data: "10", Type: "sign"},
			{Name: "result2", Data: fmt.Sprintf(`{{ if eq "%s" .signed}}1{{ else }}0{{ end }}`, signed2), Type: "set"},
			{Name: "signed", Extra: "secret", Method: "sha512", Data: "10", Type: "sign"},
			{Name: "result3", Data: fmt.Sprintf(`{{ if eq "%s" .signed}}1{{ else }}0{{ end }}`, signed3), Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result1")
		if err != nil {
			t.Fatal("Failed on signing: ", err)
		}
		err = checkEqualResult(e, "result2")
		if err != nil {
			t.Fatal("Failed on signing: ", err)
		}
		err = checkEqualResult(e, "result3")
		if err != nil {
			t.Fatal("Failed on signing: ", err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}
func TestAction_Hash(t *testing.T) {
	hash := "4A44DC15364204A80FE80E9039455CC1608281820FE2B24F1E5233ADE6AF1DD5"
	executorConfig := ExecutorConfig{Name: "hash_checker",
		Period: "1m",
		Scenario: []Task{
			{Name: "hash", Method: "sha256", Data: "10", Type: "hash"},
			{Name: "hash", Data: "{{upper .hash}}", Type: "set"},
			{Name: "result", Data: fmt.Sprintf(`{{ if eq "%s" .hash}}1{{ else }}0{{ end }}`, hash), Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal("Failed on hashing: ", err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}
func TestAction_Encode(t *testing.T) {
	encoded := "MTA="
	executorConfig := ExecutorConfig{Name: "encode_checker",
		Period: "1m",
		Scenario: []Task{
			{Name: "encoded", Method: "base64", Data: "10", Type: "encode"},
			{Name: "eresult", Data: fmt.Sprintf(`{{ if eq "%s" .encoded}}1{{ else }}0{{ end }}`, encoded), Type: "set"},
			{Name: "decoded", Method: "base64", Data: encoded, Type: "decode"},
			{Name: "dresult", Data: `{{ if eq .decoded "10" }}1{{ else }}0{{ end }}`, Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "eresult")
		if err != nil {
			t.Fatal("Failed on encoding: ", err)
		}
		err = checkEqualResult(e, "dresult")
		if err != nil {
			t.Fatal("Failed on decoding: ", err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}
func TestAction_Stat(t *testing.T) {
	name := randstr.String(20)
	executorConfig := ExecutorConfig{Name: "stat_checker",
		Period: "1m",
		Scenario: []Task{
			{Name: name, Labels: `{"label": "value"}`, Data: "10", Type: "stat"},
			{Name: "broken_stat_value", Labels: `{"label": "value"}`, Data: "sdchb", Type: "stat"},
			{Name: "broken_labels_value", Labels: `"label": "value"}`, Data: "0", Type: "stat"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		back := make(chan stat.Rec)
		go func() {
			select {
			case <-time.After(time.Second * 3):
				return
			case message := <-statChan:
				back <- message
			}
		}()
		e.Run()
		select {
		case <-time.After(time.Second * 3):
			t.Fatal("Stat message has timedout")
		case message := <-back:
			if message.Name != name {
				t.Fatal("Metric name is unknown: ", message)
			}
		}
		if e.GetContextValue("broken_stat_value_result") == nil {
			t.Fatal("Should fail on broken stat value")
		}
		if e.GetContextValue("broken_labels_value_result") == nil {
			t.Fatal("Should fail on broken labels value")
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}
func TestAction_QR(t *testing.T) {
	data := randstr.String(200)
	executorConfig := ExecutorConfig{Name: "qr_checker",
		Period: "1m",
		Scenario: []Task{
			{Data: "/tmp/qr.png", Name: "filename", Type: "set"},
			{Target: "{{.filename}}", Data: data, Type: "qr"},
			{Name: "decoded", Target: "{{.filename}}", Type: "qr"},
			{Name: "result", Data: fmt.Sprintf(`{{ if eq .decoded "%s" }}1{{ else }}0{{ end }}`, data), Type: "set"},
			{Name: "rm", Target: "rm {{.filename}}", Type: "shell"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal("Failed: ", err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}

func TestAction_CheckMongoOps(t *testing.T) {
	predefined := make(map[string]Scenario)
	predefined["mongo_get"] = Scenario{
		{Name: "got_count", Data: "got_count", Type: "unset"},
		{Name: "got", Data: "{{.filter}}", Target: "{{.dsn}}", Type: "mongo_get"},
		{Name: "got_count", Target: "#", Data: "{{.got}}", Type: "json_get"},
		{Type: "set", Name: "got_count", Data: "{{.got_count}}"},
	}
	predefined["mongo_aggr"] = Scenario{
		{Name: "aggregated_data", Data: "{{.aggr_filter}}", Target: "{{.dsn}}", Type: "mongo_aggregate"},
		{Name: "extra_id", Target: "0.extra.0.id", Data: "{{.aggregated_data}}", Type: "json_get"},
		{Type: "set", Name: "extra_id", Data: "{{.extra_id}}"},
		{Type: "set", Name: "aggregate_result", Data: "{{ if eq .extra_id .expected_extra_id}}1{{ else }}0{{ end }}"},
	}
	predefined["check_count_in_mongo"] = Scenario{
		{Name: "result", Data: "result", Type: "unset"},
		{Type: "run", Name: "run", Target: "mongo_get"},
		{Name: "result", Data: "{{ if eq .expected .got_count }}1{{ else }}0{{ end }}", Type: "set"},
	}
	executorConfig := ExecutorConfig{Name: "check_mongo_ops",
		Predefined: predefined,
		Variables: []VariableConfig{
			{
				Name:  "expected_extra_id",
				Value: "1000",
			},
			{
				Name:  "filter",
				Value: "{}",
			},
			{
				Name:  "aggr_filter",
				Value: `[{ "$match": { "tag": "extra" } }, {"$lookup":{"from":"extra","localField":"name","foreignField":"name","as":"extra"}}]`,
			},
			{
				Name:  "dsn",
				Value: "mongodb://localhost:27017/?db=test&collection=test",
			},
			{
				Name:  "extra_dsn",
				Value: "mongodb://localhost:27017/?db=test&collection=extra",
			},
		},
		After: Scenario{
			{Name: "deleted", Data: `{"id": {"$in": ["1000","2000"]}}`, Target: "{{.extra_dsn}}", Type: "mongo_delete"},
			{Name: "deleted", Data: `{"id": {"$in": ["1","2"]}}`, Target: "{{.dsn}}", Type: "mongo_delete"},
			{Name: "delete_result", Data: "{{ if eq .deleted .inserted }}1{{ else }}0{{ end }}", Type: "set"},
		},
		Period: "10m",
		Scenario: Scenario{
			{Name: "data", Data: `[{"id":"1","name":"one","tag":"extra"}, {"id":"2","name":"two"}]`, Type: "set"},
			{Name: "extra_data", Data: `[{"id":"{{.expected_extra_id}}","name":"one"}, {"id":"2000","name":"two"}]`, Type: "set"},
			{Name: "insert_expected", Data: "2", Type: "set"},
			{Name: "inserted", Data: "{{.data}}", Target: "{{.dsn}}", Type: "mongo_insert", Extra: "{{.filter}}"},
			{Name: "inserted_exrta", Data: "{{.extra_data}}", Target: "{{.extra_dsn}}", Type: "mongo_insert", Extra: "{{.filter}}"},
			{Name: "insert_result", Data: "{{ if eq .inserted 2 }}1{{ else }}0{{ end }}", Type: "set"},
			{Name: "expected", Data: "2", Type: "set"},
			{Type: "run", Name: "run", Target: "check_count_in_mongo"},
			{Type: "run", Name: "run", Target: "mongo_aggr"},
			{Name: "update", Data: `{"$set": {"name": "uno"}}`, Target: "{{.dsn}}", Type: "mongo_update", Extra: `{"id": "1"}`},
			{Name: "update_result", Data: `{{ if eq .update_modified 1 }}1{{ else }}0{{ end }}`, Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Start()
		time.Sleep(time.Second * 1)
		err := checkEqualResult(e, "insert_result")
		if err != nil {
			e.Stop()
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result")
		if err != nil {
			e.Stop()
			t.Fatal(err)
		}
		err = checkEqualResult(e, "aggregate_result")
		if err != nil {
			e.Stop()
			t.Fatal(err)
		}
		err = checkEqualResult(e, "update_result")
		if err != nil {
			e.Stop()
			t.Fatal(err)
		}
		e.Stop()
		err = checkEqualResult(e, "delete_result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}

func TestAction_Glob(t *testing.T) {
	predefined := make(map[string]Scenario)
	executorConfig := ExecutorConfig{Name: "check_glob",
		Predefined: predefined,
		Scenario: Scenario{
			{Name: "create the temp files", Target: "mkdir /tmp/tmp && cd /tmp/tmp && touch file1 && touch file2 && touch file3", Type: "shell"},
			{Name: "files", Target: "/tmp/tmp/file*", Type: "glob"},
			{Name: "files", Data: "{{.files}}", Type: "print"},
			{Name: "length", Data: "{{ len .files }}", Type: "set"},
			{Name: "result", Data: `{{ if eq .length "3" }}1{{ else }}0{{ end }}`, Type: "set"},
			{Name: "remove the temp files", Target: "rm -rf /tmp/tmp", Type: "shell"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}

func TestAction_HTMLQuery(t *testing.T) {
	predefined := make(map[string]Scenario)
	executorConfig := ExecutorConfig{Name: "check_html_query",
		Predefined: predefined,
		Period:     "10h",
		Scenario: Scenario{
			{Type: "set", Name: "html_data", Data: `<html><head><style>table, th, td { border: 1px solid black; border-collapse: collapse;}th, td { padding: 5px; text-align: left;}</style></head><body><div><p><a class="link unique" href="https://example.com/">Title1</a></p></div><h2>Table Caption</h2><p>To add a caption to a table, use the caption tag.</p><table style="width:100%"> <caption>Monthly savings</caption> <tr>  <th>Month</th>  <th>Savings</th> </tr> <tr>  <td id="month">January</td>  <td>$100</td> </tr> <tr>  <td>February</td>  <td>$50</td> </tr></table><a class="link" href="https://example.com/">Title2</a></body></html>`},
			{Name: "all_links", Data: "{{.html_data}}", Target: "a.link", Type: "html_query"},
			{Name: "result1", Data: "{{ if eq (len .all_links) 2 }}1{{ else }}0{{ end }}", Type: "set"},
			{Name: "custom_links", Data: "{{.html_data}}", Target: "a.unique", Type: "html_query"},
			{Name: "result2", Data: "{{ if eq (len .custom_links) 1 }}1{{ else }}0{{ end }}", Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result1")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result2")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}

func TestAction_TCP(t *testing.T) {
	randStr := randstr.String(10)
	port := "9090"
	err := runTCPServer(port)
	if err != nil {
		t.Fatal("Failed on starting a TCP server: ", err)
	}
	predefined := make(map[string]Scenario)
	predefined["process"] = Scenario{
		{Type: "set", Name: "result1", Data: fmt.Sprintf(`{{ if eq .process_pipe_item "%s" }}1{{ else }}0{{ end }}`, randStr)},
	}
	executorConfig := ExecutorConfig{Name: "check_tcp_client",
		Predefined: predefined,
		Scenario: Scenario{
			{Name: "process_pipe", Target: "process", Type: "pipe"},
			{Name: "client1", Data: "process_pipe", Target: fmt.Sprintf("localhost:%s", port), Type: "tcp", Extra: "10"},
			{Name: "pipe", Target: "client1_pipe", Data: fmt.Sprintf("%s\n", randStr), Type: "pipe"},
			{Name: "client2", Data: "process_pipe", Target: "localhost:1098", Type: "tcp"},
			{Name: "result2", Data: `{{ if not .client2 }}1{{ else }}0{{ end }}`, Type: "set"},
			{Name: "wait", Data: "3s", Type: "wait"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{{Name: "type", Value: randStr}}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		e.StopRepeat("process_pipe")
		e.StopRepeat("client1")
		err := checkEqualResult(e, "result1")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result2")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}

func runTCPServer(port string) error {
	l, err := net.Listen("tcp", fmt.Sprintf("localhost:%s", port))
	if err != nil {
		return err
	}
	go func() {
		defer l.Close()
		for {
			conn, err := l.Accept()
			if err != nil {
				fmt.Println(err)
				return
			}
			go func(c net.Conn) {
				for {
					message, err := bufio.NewReader(c).ReadString('\n')
					if err != nil {
						fmt.Println(err)
						return
					}

					message = strings.TrimSpace(message)
					if message == "stop" {
						break
					}
					c.Write([]byte(message))
				}
				c.Close()
			}(conn)
		}
	}()
	return nil
}
func TestAction_Set(t *testing.T) {
	data := "1.0"
	executorConfig := ExecutorConfig{Name: "closer",
		Period: "1m",
		Scenario: []Task{
			{Name: "data", Type: "set", Data: data},
			{Name: "result1", Type: "set", Data: fmt.Sprintf(`{{ if eq .data "%s" }}1{{ else }}0{{ end }}`, data)},
			{Name: "data", Type: "set", Data: data, Extra: "string"},
			{Name: "result1", Type: "set", Data: fmt.Sprintf(`{{ if eq .data "%s" }}1{{ else }}0{{ end }}`, data)},
			{Name: "data", Type: "set", Data: data, Extra: "sdf"},
			{Name: "result2", Type: "set", Data: fmt.Sprintf(`{{ if eq .data "%s" }}1{{ else }}0{{ end }}`, data)},
			{Name: "data", Type: "set", Data: data, Extra: ""},
			{Name: "result3", Type: "set", Data: fmt.Sprintf(`{{ if eq .data "%s" }}1{{ else }}0{{ end }}`, data)},
			{Name: "data", Type: "set", Data: data, Extra: "float"},
			{Name: "result4", Type: "set", Data: fmt.Sprintf(`{{ if eq .data %s }}1{{ else }}0{{ end }}`, data)},
			{Name: "data", Type: "set", Data: "test"},
			{Name: "result5", Type: "set", Data: `{{ if eq .data "test" }}1{{ else }}0{{ end }}`},
			{Name: "data", Type: "set", Data: data, Extra: "int"},
			{Name: "result6", Type: "set", Data: `{{ if eq .data 1 }}1{{ else }}0{{ end }}`},
			{Name: "data", Type: "set", Data: data, Extra: "bool"},
			{Name: "result7", Type: "set", Data: `{{ if .data }}1{{ else }}0{{ end }}`},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result1")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result2")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result3")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result4")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result5")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result6")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result7")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}
func TestAction_CompareImages(t *testing.T) {
	img1Filepath := "image1.png"
	img2Filepath := "image2.png"
	createImage(100, 200, 200, img1Filepath)
	createImage(110, 210, 210, img2Filepath)
	executorConfig := ExecutorConfig{Name: "comparator",
		Period: "1m",
		Scenario: []Task{
			{Name: "equal", Type: "compare_images", Data: img1Filepath, Target: img2Filepath},
			{Name: "result", Type: "set", Data: `{{ if .equal }}1{{ else }}0{{ end }}`},
			{Name: "shell", Type: "shell", Target: fmt.Sprintf(`rm %s %s`, img1Filepath, img2Filepath)},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}

// TODO: move to self hosted html
func TestAction_Browse(t *testing.T) {
	randCookie := randstr.String(20)
	randBefore := randstr.String(10)
	randAfter := randstr.String(10)
	randTarget := randstr.String(10)
	const indexHTML = `<!doctype html>
	<html>
	<head>
	  <title>example</title>
	</head>
	<body>
	  <div id="box1" style="display:none">
	    <div id="box2">
	      <p>box2</p>
	    </div>
	  </div>
	  <div id="box3">
	    <h2>Wikipedia</h2>
	    <p id="box4">
	      box4 text
	      <input id="input1" value="some value"><br><br>
	      <textarea id="textarea1" style="width:500px;height:400px">textarea</textarea><br><br>
	      <input id="input2" type="submit" value="Next">
	      <select id="select1">
	        <option value="one">1</option>
	        <option value="two">2</option>
	        <option value="three">3</option>
	        <option value="four">4</option>
	      </select>
	    </p>
	  </div>
	</body>
	</html>`
	startServer("2221", "/", func(w http.ResponseWriter, req *http.Request) {
		cookie, _ := req.Cookie("cookie")
		if cookie.String() != fmt.Sprintf("cookie=%s", randCookie) {
			logger.Errorf("cookie is invalid: %s <> %s", cookie.String(), randCookie)
			os.Exit(1)
		}
		w.Write([]byte(indexHTML))
	})
	defer stopServer("2221")
	predefined := make(map[string]Scenario)
	predefined["browser_before"] = Scenario{
		{Type: "set", Name: "result_before", Data: randBefore},
		{Type: "set_cookie", Name: "cookie", Data: `{"name": "broken_cookie", "domain": "localhost"}`, Extra: "browser"},
		{Type: "set_cookie", Name: "cookie", Data: fmt.Sprintf(`{"value": "%s", "domain": "localhost"}`, randCookie), Extra: "browser"},
		{Type: "set_cookie", Name: "cookie", Data: fmt.Sprintf(`{"name": "cookie", "value": "%s", "domain": "localhost", "httpOnly": true}`, randCookie), Extra: "browser"},
	}
	predefined["browser_after"] = Scenario{
		{Type: "set", Name: "result_after", Data: randAfter},
	}
	predefined["process_nodes"] = Scenario{
		{Type: "set", Name: "uninterned", Data: `{{toJson .process_nodes_loop_item}}`},
		{Type: "intern", Name: "interned", Data: `{{ .uninterned }}`},
		{Type: "set", Name: "node_text", Data: `{{ index (index .interned.children 0) "nodeValue"}}`},
	}
	predefined["browser"] = Scenario{
		{Type: "wait_visible", Name: "visible", Target: "#box3", Extra: "browser"},
		{Type: "screenshot", Name: "screenshot_form", Data: "screenshot_form.png", Target: "#box3", Extra: "browser"},
		{Type: "screenshot_viewport", Name: "screenshot1", Target: "screenshot1.png", Extra: "browser"},
		{Type: "screenshot_viewport", Name: "screenshot2", Target: "screenshot2.png", Extra: "browser"},
		{Type: "compare_images", Name: "compare_result1", Target: "screenshot1.png", Data: "screenshot2.png"},
		{Type: "page_get_text", Name: "text", Target: "#box3 h2", Extra: "browser"},
		{Type: "click", Name: "click", Target: "select", Extra: "browser"},
		{Type: "screenshot_viewport", Name: "screenshot", Target: "screenshot_after_click.png", Extra: "browser"},
		{Type: "form_set_value", Name: "form_set_value", Target: "#input1", Data: "password", Extra: "browser"},
		{Type: "form_get_value", Name: "form_get_value", Target: "#input1", Extra: "browser"},
		{Type: "dom_nodes", Name: "nodes", Target: "#box3 h2", Extra: "browser"},
		{Type: "loop", Name: "process_nodes_loop", Data: "nodes", Target: "process_nodes"},
		{Type: "shell", Name: "shell", Target: "rm screenshot*.png"},
		{Type: "set", Name: "result_target", Data: randTarget},
	}
	executorConfig := ExecutorConfig{Name: "browser",
		Period: "1m",
		Scenario: []Task{
			{Name: "browser", Type: "browse", Data: `{"viewport":"768:1024","user_agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36"}`, Target: "http://localhost:2221/"},
			{Name: "result", Type: "set", Data: `{{ if .browser }}1{{ else }}0{{ end }}`},
			{Name: "result1", Type: "set", Data: fmt.Sprintf(`{{ if eq .result_before "%s" }}1{{ else }}0{{ end }}`, randBefore)},
			{Name: "result2", Type: "set", Data: fmt.Sprintf(`{{ if eq .result_after "%s" }}1{{ else }}0{{ end }}`, randAfter)},
			{Name: "result3", Type: "set", Data: fmt.Sprintf(`{{ if eq .result_target "%s" }}1{{ else }}0{{ end }}`, randTarget)},
			{Name: "result4", Type: "set", Data: `{{ if .compare_result1 }}1{{ else }}0{{ end }}`},
			{Name: "result5", Type: "set", Data: `{{ if eq .text "Wikipedia" }}1{{ else }}0{{ end }}`},
			{Name: "result6", Type: "set", Data: `{{ if not .compare_result2 }}1{{ else }}0{{ end }}`},
			{Name: "result7", Type: "set", Data: `{{ if eq (trim .node_text) "Wikipedia" }}1{{ else }}0{{ end }}`},
			{Name: "result8", Type: "set", Data: `{{if eq .form_get_value "password"}}1{{else}}0{{end}}`},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, predefined, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result1")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result2")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result3")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result4")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result5")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result6")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result7")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Faled to create an executor")
	}
}

func TestAction_Parse(t *testing.T) {
	predefined := make(map[string]Scenario)
	randString := randstr.String(20)
	yamlFilepath := "cfg.yaml"
	data1 := fmt.Sprintf(`
set:
- type: set
  name: item
  data: %s`, randString)
	data2 := fmt.Sprintf(`{"set": [{"type": "set", "name": "item", "data": "%s"}]}`, randString)
	executorConfig := ExecutorConfig{Name: "parse_checker",
		Period:     "1m",
		Predefined: predefined,
		Scenario: []Task{
			{Name: "yaml_data", Data: data1, Type: "set"},
			{Name: "parsed1", Type: "parse", Data: "{{.yaml_data}}", Format: "yaml"},
			{Name: "result1", Data: fmt.Sprintf(`{{ if eq ( index ( index .parsed1.set 0 ) "data") "%s" }}1{{ else }}0{{ end }}`, randString), Type: "set"},
			{Name: "json_data", Data: data2, Type: "set"},
			{Name: "parsed2", Type: "parse", Data: "{{.json_data}}", Format: "json"},
			{Name: "result2", Data: fmt.Sprintf(`{{ if eq ( index ( index .parsed2.set 0 ) "data") "%s" }}1{{ else }}0{{ end }}`, randString), Type: "set"},
			{Name: "yaml_data", Target: yamlFilepath, Data: data1, Type: "file_write"},
			{Name: "parsed3", Type: "parse", Target: yamlFilepath, Format: "yaml"},
			{Name: "result3", Data: fmt.Sprintf(`{{ if eq ( index ( index .parsed1.set 0 ) "data") "%s" }}1{{ else }}0{{ end }}`, randString), Type: "set"},
			{Name: "rm", Type: "shell", Target: fmt.Sprintf("rm %s", yamlFilepath)},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result1")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result2")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result3")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_Poll(t *testing.T) {
	filename := "tailed.log"
	predefined := make(map[string]Scenario)
	predefined["action"] = []Task{
		{Type: "wait", Name: "wait", Data: "100ms"},
		{Type: "set", Name: "data", Data: "{{.index}}"},
		{Type: "set", Name: "expected", Data: "{{.expected}}+{{.data}}"},
		{Type: "file_write", Name: "write", Data: "{{.data}}\n", Target: filename},
		{Name: "index", Data: "index+1", Type: "calc"},
	}
	predefined["run_loop"] = []Task{
		{Type: "while", Name: "loop", Data: "le .index .limit", Target: "action"},
	}
	predefined["poll_action"] = []Task{
		{Type: "set", Name: "actual", Data: "{{.actual}}+{{.poll_item}}"},
	}
	executorConfig := ExecutorConfig{Name: "parse_checker",
		Period:     "1m",
		Predefined: predefined,
		Scenario: []Task{
			{Name: "index", Data: "0", Type: "calc"},
			{Name: "limit", Data: "10", Type: "calc"},
			{Name: "expected", Data: "", Type: "set"},
			{Name: "actual", Data: "", Type: "set"},
			{Name: "poll", Type: "poll", Data: filename, Target: "poll_action"},
			{Name: "run_loop", Target: "run_loop", Type: "run"},
			{Type: "wait", Name: "poll", Data: "500ms"},
			{Type: "break", Name: "poll"},
			{Type: "shell", Name: "rm", Target: fmt.Sprintf("rm %s", filename)},
			{Name: "result", Type: "set", Data: "{{ if eq .actual .expected }}1{{ else }}0{{ end }}"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_Base64SHA256HMAC(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "_checker",
		Period: "1m",
		Scenario: []Task{
			{Name: "expected", Data: "MDg1M2MzNjE0YWI1NmM1MDc0ZDRhZTU1NzFiODQ4NGEyN2IyMTNlM2UzZDE5ZjIyOGJkYmM5YWY1MTAxZjk2Yw==", Type: "set"},
			{Name: "secret", Data: "secret", Type: "set"},
			{Name: "msg", Data: "message_to_sign", Type: "set"},
			{Name: "signed", Data: "{{.msg}}", Type: "sign", Method: "sha256", Extra: "{{.secret}}"},
			{Name: "actual", Data: "{{.signed}}", Type: "encode", Method: "base64"},
			{Name: "result", Data: "{{if eq .actual .expected}}1{{else}}0{{end}}", Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}

func TestAction_FileStat(t *testing.T) {
	executorConfig := ExecutorConfig{Name: "_checker",
		Period: "1m",
		Scenario: []Task{
			{Name: "secret", Target: "secret", Data: "secret", Type: "file_write"},
			{Name: "secret", Target: "secret", Type: "file_stat"},
			{Name: "secret", Data: "secret: {{.secret.Name}}", Type: "print"},
			{Name: "result", Data: `{{if eq .secret.Name "secret"}}1{{else}}0{{end}}`, Type: "set"},
			{Name: "secret", Target: "rm secret", Type: "shell"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_AMQP(t *testing.T) {
	const amqpConfig = `
	{
		"name": "test_channel",
		"url": "amqp://guest:guest@localhost:5672/",
		"message": {
			"content_type": "text/plain"
		},
		"queue": {
			"name": "test_queue",
			"routing_key": "test_key"
		},
		"exchange": {
			"name": "test_exchange",
			"type": "direct",
			"auto_delete": false,
			"durable": true
		}
	}
	`
	randStr := randstr.String(20)
	predefined := make(map[string]Scenario)
	predefined["process"] = Scenario{
		{Type: "set", Name: "received", Data: `{{ printf "%s" .consumer_item.Body}}`},
	}
	executorConfig := ExecutorConfig{Name: "amqp_checker",
		Period:     "1m",
		Predefined: predefined,
		Scenario: []Task{
			{Name: "consumer", Target: amqpConfig, Data: "process", Type: "amqp_consume"},
			{Name: "amqp_publish", Data: randStr, Target: amqpConfig, Type: "amqp_publish"},
			{Name: "2s", Data: "2s", Type: "wait"},
			{Name: "result", Data: fmt.Sprintf(`{{ if eq "%s" .received}}1{{ else }}0{{ end }}`, randStr), Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_RSS(t *testing.T) {
	randStr := randstr.String(20)
	feed := fmt.Sprintf(`<rss version="2.0">
	<channel>
	<title>%s</title>
	<link>http://www.spotfire.tibco.com/en/</link>
	<description>Spotfire Analytics</description>
	<image>
	<url>http://www.spotfire.tibco.fr/spot-icon.gif</url>
	<link>http://www.spotfire.tibco.com/en/index.php</link>
	</image>
	<item>
	<title>Spotfire Today</title>
	<link>http://www.spotfire.tibco.com/en-xml-rss.html</link>
	<description>All you need to know about Spotfire</description>
	</item>
	<item>
	<title>Spotfire Tomorrow</title>
	<link>http://www.spotfire.tibco.com/en-xml-rdf.html</link>
	<description>And now, all about Spotfire Future</description>
	</item>
	</channel>
	</rss>`, randStr)
	executorConfig := ExecutorConfig{Name: "rss_checker",
		Period: "1m",
		Scenario: []Task{
			{Name: "data", Data: feed, Type: "parse_rss"},
			{Name: "title", Data: `{{ .data.Title}}`, Type: "set"},
			{Name: "result", Data: fmt.Sprintf(`{{ if eq "%s" .title}}1{{ else }}0{{ end }}`, randStr), Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_ProcessTaskFields(t *testing.T) {
	postfix := randstr.String(20)
	task := Task{
		Name:         "Name_{{.postfix}}",
		Type:         "Type_{{.postfix}}",
		Target:       "Target_{{.postfix}}",
		Method:       "Method_{{.postfix}}",
		Data:         "Data_{{.postfix}}",
		Duration:     "Duration_{{.postfix}}",
		Format:       "Format_{{.postfix}}",
		Labels:       "Labels_{{.postfix}}",
		Headers:      "Headers_{{.postfix}}",
		Extra:        "Extra_{{.postfix}}",
		SkipCond:     "SkipCond_{{.postfix}}",
		Async:        "Async_{{.postfix}}",
		Timeout:      "Timeout_{{.postfix}}",
		OnTimeout:    "OnTimeout_{{.postfix}}",
		OnError:      "OnError_{{.postfix}}",
		OnStart:      "OnStart_{{.postfix}}",
		OnFinish:     "OnFinish_{{.postfix}}",
		StopOnError:  "StopOnError_{{.postfix}}",
		ErrorIf:      "ErrorIf_{{.postfix}}",
		ErrorMessage: "ErrorMessage_{{.postfix}}",
	}
	executorConfig := ExecutorConfig{Name: "rss_checker",
		Period:   "1m",
		Scenario: []Task{},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{VariableConfig{Name: "postfix", Value: postfix}}, map[string]Scenario{}, statChan); e != nil {
		e.ProcessTaskFields(&task)
		v := reflect.ValueOf(task)
		tp := v.Type()
		for i := 0; i < v.NumField(); i++ {
			field := tp.Field(i)
			value := v.Field(i)
			if value.String() != fmt.Sprintf("%s_%s", field.Name, postfix) {
				t.Fatalf("%s is not processed", field.Name)
			}
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}
func TestAction_JSONValidate(t *testing.T) {
	schema := `{
		"$schema": "http://json-schema.org/draft-04/schema#",
		"$id": "https://example.com/employee.schema.json",
		"title": "Record of employee",
		"description": "This document records the details of an employee",
		"type": "object",
		"required": [
			"id"
		],
		"additionalProperties": false,
		"properties": {
			"id": {
				"description": "A unique identifier for an employee",
				"type": "number"
			},
			"name": {
				"description": "Full name of the employee",
				"type": "string"
			},
			"age": {
				"description": "Age of the employee",
				"type": "number"
			},
			"animal": {
				"type": "string",
				"enum": [
					"elephant",
					"mouse"
				]
			},
			"hobbies": {
				"description": "Hobbies of the employee",
				"type": "object",
				"properties": {
					"indoor": {
						"type": "array",
						"items": {
							"description": "List of indoor hobbies",
							"type": "string"
						}
					},
					"outdoor": {
						"type": "array",
						"items": {
							"description": "List of outdoor hobbies",
							"type": "string"
						}
					}
				}
			}
		}
	}`
	valid_data := `{
		"id": 7,
		"name": "John Doe",
		"age": 22,
		"hobbies": {
			"indoor": [
				"Chess"
			],
			"outdoor": [
				"BasketballStand-up Comedy"
			]
		}
	}`
	broken_json_data := `{
		"id": 7
		"name": "John Doe",
		"age": 22,
		"hobbies": {
			"indoor": [
				"Chess"
			],
			"outdoor": [
				"BasketballStand-up Comedy"
			]
		}
	}`
	missing_optional_data := `{
		"id": 7,
		"name": "John Doe",
		"hobbies": {
			"indoor": [
				"Chess"
			],
			"outdoor": [
				"BasketballStand-up Comedy"
			]
		}
	}`
	missing_required_data := `{
		"name": "John Doe",
		"age": 22,
		"hobbies": {
			"indoor": [
				"Chess"
			],
			"outdoor": [
				"BasketballStand-up Comedy"
			]
		}
	}`
	extra_data := `{
		"id": 7,
		"nickname": "whatever",
		"name": "John Doe",
		"age": 22,
		"hobbies": {
			"indoor": [
				"Chess"
			],
			"outdoor": [
				"BasketballStand-up Comedy"
			]
		}
	}`
	extra_enum_data := `{
		"id": 7,
		"animal": "whatever",
		"name": "John Doe",
		"age": 22,
		"hobbies": {
			"indoor": [
				"Chess"
			],
			"outdoor": [
				"BasketballStand-up Comedy"
			]
		}
	}`
	executorConfig := ExecutorConfig{Name: "json_checker",
		Period: "1m",
		Scenario: []Task{
			{Name: "schema", Data: schema, Type: "set"},
			{Name: "data", Data: valid_data, Type: "set"},
			{Name: "valid1", Data: "{{.data}}", Target: "{{.schema}}", Type: "validate_json"},
			{Name: "result1", Data: "{{if .valid1}}1{{else}}0{{end}}", Type: "set"},
			{Name: "data", Data: broken_json_data, Type: "set"},
			{Name: "valid2", Data: "{{.data}}", Target: "{{.schema}}", Type: "validate_json"},
			{Name: "result2", Data: "{{if .valid2}}0{{else}}1{{end}}", Type: "set"},
			{Name: "data", Data: missing_optional_data, Type: "set"},
			{Name: "valid3", Data: "{{.data}}", Target: "{{.schema}}", Type: "validate_json"},
			{Name: "result3", Data: "{{if .valid3}}1{{else}}0{{end}}", Type: "set"},
			{Name: "data", Data: missing_required_data, Type: "set"},
			{Name: "valid4", Data: "{{.data}}", Target: "{{.schema}}", Type: "validate_json"},
			{Name: "result4", Data: "{{if .valid4}}0{{else}}1{{end}}", Type: "set"},
			{Name: "data", Data: extra_data, Type: "set"},
			{Name: "valid5", Data: "{{.data}}", Target: "{{.schema}}", Type: "validate_json"},
			{Name: "result5", Data: "{{if .valid5}}0{{else}}1{{end}}", Type: "set"},
			{Name: "data", Data: extra_enum_data, Type: "set"},
			{Name: "valid6", Data: "{{.data}}", Target: "{{.schema}}", Type: "validate_json"},
			{Name: "result6", Data: "{{if .valid6}}0{{else}}1{{end}}", Type: "set"},
		},
	}
	if e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan); e != nil {
		e.Run()
		err := checkEqualResult(e, "result1")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result2")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result3")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result4")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result5")
		if err != nil {
			t.Fatal(err)
		}
		err = checkEqualResult(e, "result6")
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Failed to create an executor")
	}
}

func createImage(red, green, blue uint8, filepath string) {
	width := 200
	height := 100

	upLeft := image.Point{0, 0}
	lowRight := image.Point{width, height}

	img := image.NewRGBA(image.Rectangle{upLeft, lowRight})

	// Colors are defined by Red, Green, Blue, Alpha uint8 values.
	clr := color.RGBA{red, green, blue, 0xff}
	// Set color for each pixel.
	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			switch {
			case x < width/2 && y < height/2: // upper left quadrant
				img.Set(x, y, clr)
			case x >= width/2 && y >= height/2: // lower right quadrant
				img.Set(x, y, color.White)
			default:
				// Use zero value.
			}
		}
	}

	// Encode as PNG.
	f, _ := os.Create(filepath)
	png.Encode(f, img)
}

func runSSHServer() {
	ssh.Handle(func(s ssh.Session) {
		authorizedKey := gossh.MarshalAuthorizedKey(s.PublicKey())
		io.WriteString(s, fmt.Sprintf("public key used by %s:\n", s.User()))
		s.Write(authorizedKey)
	})

	publicKeyOption := ssh.PublicKeyAuth(func(ctx ssh.Context, key ssh.PublicKey) bool {
		return true // allow all keys, or use ssh.KeysEqual() to compare against known keys
	})

	log.Println("starting ssh server on port 2222...")
	log.Fatal(ssh.ListenAndServe(":2222", nil, publicKeyOption))
}
