package executor

import (
	"context"
	"database/sql"
	"fmt"
	"io"
	"os"
	"os/user"
	"reflect"
	"strings"
	"time"

	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/task-executor/internal/utils"
	"golang.org/x/crypto/ssh"
)

func makeSigner(keyname string) (signer ssh.Signer, err error) {
	fp, err := os.Open(keyname)
	if err != nil {
		return
	}
	defer fp.Close()

	buf, err := io.ReadAll(fp)
	if err != nil {
		return
	}
	signer, err = ssh.ParsePrivateKey(buf)
	if err != nil {
		return
	}
	return
}

func makeKeyring(keyPath string) (auth ssh.AuthMethod, err error) {
	signer, err := makeSigner(keyPath)
	if err != nil {
		return
	}
	auth = ssh.PublicKeys(signer)
	return
}

func runDBAction(e *Executor, task *Task) error {
	err := processQuery(e, task)
	if err != nil {
		return err
	}
	return err
}

func processQuery(e *Executor, task *Task) error {
	var data []map[string]interface{}
	name, err := e.handleTemplateError(task.Name, "name")
	if err != nil {
		return err
	}
	dsn, err := e.handleTemplateError(task.Target, "target")
	if err != nil {
		return err
	}
	query, err := e.handleTemplateError(task.Data, "data")
	if err != nil {
		return err
	}
	driver, err := e.handleTemplateError(task.Extra, "extra")
	if err != nil {
		return err
	}
	conn, err := sql.Open(driver, dsn)
	if err != nil {
		return err
	}
	if err := conn.Ping(); err != nil {
		return err
	}
	defer conn.Close()
	logger.
		WithField(executorKey, e.Name).
		WithField("task", task.Name).
		Debugf("Prepare a query: '%s'", query)
	start := time.Now()
	ctx, cancel := context.WithTimeout(context.Background(),
		e.GetContextValue(fmt.Sprintf("%s_timeout", task.Name)).(time.Duration))
	defer cancel()
	rows, err := conn.QueryContext(ctx, query)
	logger.
		WithField(executorKey, e.Name).
		WithField("task", name).
		Debugf("Query run for %v", time.Since(start))
	if err != nil {
		return err
	}
	columns, err := rows.Columns()
	if err != nil {
		return err
	}
	columnTypes, err := rows.ColumnTypes()
	if err != nil {
		return err
	}
	defer rows.Close()
	index := 0
	for rows.Next() {
		placeholders := utils.GetVariables(columnTypes)
		err := rows.Scan(placeholders...)
		if err != nil {
			return err
		}
		data = append(data, make(map[string]interface{}))
		for i, placeholder := range placeholders {
			nullable, _ := (*columnTypes[i]).Nullable()
			if nullable {
				data[index][columns[i]] = utils.GetSQLNullableValue(placeholder)
			} else {
				val := reflect.ValueOf(placeholder).Elem()
				data[index][columns[i]] = getReflectValue(val)
			}
		}
		index++
	}
	err = rows.Err()
	if err == nil {
		e.SetContextValue(name, data)
	}
	return err
}

func getReflectValue(val reflect.Value) interface{} {
	switch val.Kind() {
	case reflect.Bool:
		return val.Bool()
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return val.Int()
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return val.Uint()
	case reflect.Float32, reflect.Float64:
		return val.Float()
	default:
		return val.String()
	}
}

func getCurrentUsername() string {
	currentUser, _ := user.Current()
	if currentUser != nil {
		return currentUser.Username
	}
	return ""
}
func getCurrentUserHomeDir() string {
	currentUser, _ := user.Current()
	if currentUser != nil {
		return currentUser.HomeDir
	}
	return ""
}

func ExpandPath(p string) string {
	if strings.HasPrefix(p, "~/") {
		p = getCurrentUserHomeDir() + p[1:]
	}
	return p
}
