package executor

// Task is a type describing the most primitive action structure
type Task struct {
	Name         string `mapstructure:"name"`
	Type         string `mapstructure:"type"`
	Target       string `mapstructure:"target"`
	Method       string `mapstructure:"method"`
	Data         string `mapstructure:"data"`
	Duration     string `mapstructure:"duration"`
	Format       string `mapstructure:"format"`
	Labels       string `mapstructure:"labels"`
	Headers      string `mapstructure:"headers"`
	Extra        string `mapstructure:"extra"`
	SkipCond     string `mapstructure:"skip_if"`
	Async        string `mapstructure:"async"`
	Timeout      string `mapstructure:"timeout"`
	OnTimeout    string `mapstructure:"on_timeout"`
	OnError      string `mapstructure:"on_error"`
	OnStart      string `mapstructure:"on_start"`
	OnFinish     string `mapstructure:"on_finish"`
	StopOnError  string `mapstructure:"stop_on_error"`
	ErrorIf      string `mapstructure:"error_if"`
	ErrorMessage string `mapstructure:"error_message"`
}

// Scenario is a type alias for a steps list
type Scenario []Task

type DefaultsConfig struct {
	Timeout     string `mapstructure:"timeout"`
	OnTimeout   string `mapstructure:"on_timeout"`
	OnError     string `mapstructure:"on_error"`
	OnStart     string `mapstructure:"on_start"`
	OnFinish    string `mapstructure:"on_finish"`
	StopOnError string `mapstructure:"stop_on_error"`
}

// ExecutorConfig is a type describing an executor
type ExecutorConfig struct {
	Name           string              `mapstructure:"name"`
	Period         string              `mapstructure:"period"`
	SkipStartupRun bool                `mapstructure:"skip_startup_run"`
	Scenario       Scenario            `mapstructure:"scenario"`
	Before         Scenario            `mapstructure:"before"`
	After          Scenario            `mapstructure:"after"`
	Variables      VariablesConfig     `mapstructure:"variables"`
	Predefined     map[string]Scenario `mapstructure:"predefined"`
	Cycles         int                 `mapstructure:"cycles"`
	Disabled       bool                `mapstructure:"disabled"`
	Threads        int                 `mapstructure:"threads"`
	Defaults       DefaultsConfig      `mapstructure:"defaults"`
}

// ExecutorsConfig is a type alias for a tasks list
type ExecutorsConfig []ExecutorConfig

// VariableConfig is a type describing the config for global variables
type VariableConfig struct {
	Name  string      `mapstructure:"name"`
	Value interface{} `mapstructure:"value"`
	Type  string      `mapstructure:"type"`
}

// VariablesConfig is a type describing the config for global variables
type VariablesConfig []VariableConfig

// SSHConfig describes config for remote shell command execution
type SSHConfig struct {
	Host string `mapstructure:"host"`
	Port string `mapstructure:"port"`
	User string `mapstructure:"user"`
	Key  string `mapstructure:"key"`
}
