package executor

import (
	"fmt"
	"reflect"
	"regexp"
)

func (vc *VariablesConfig) String() string {
	if len(*vc) == 0 {
		return ""
	}
	result := "Additional variables:\n"
	for _, variable := range *vc {
		result = fmt.Sprintf("%s\t%s = %v(%s)\n", result, variable.Name, variable.Value, reflect.TypeOf(variable.Value))
	}
	return result
}

// Set string value in VariablesList
func (vc *VariablesConfig) Set(s string) error {
	var matches [][]string
	re, err := regexp.Compile(`(?U)^([^=]+)=(.*)(:(bool|int|float|string))?$`)
	if err != nil {
		return err
	}
	matches = re.FindAllStringSubmatch(s, -1)
	if len(matches) > 0 && len(matches[0]) > 0 {
		varName := matches[0][1]
		varValue := matches[0][2]
		varType := matches[0][4]
		*vc = append(*vc, VariableConfig{
			Name:  varName,
			Value: varValue,
			Type:  varType,
		})
	}
	return nil
}
