package executor

import (
	"fmt"
	"net/http"
	"os"
	"testing"

	httpex "gitlab.com/saintmaur/lib/http"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/lib/stat"
	"gitlab.com/saintmaur/task-executor/internal/utils"
)

var (
	statChan = make(chan stat.Rec)
)

var servers map[string]*httpex.Server

func startServer(port string, path string, f func(w http.ResponseWriter, r *http.Request)) *httpex.Server {
	var s *httpex.Server
	fmt.Printf("start server '%s'\n", port)
	if servers == nil {
		servers = make(map[string]*httpex.Server)
	}
	if _, has := servers[port]; !has {
		s = httpex.New(&httpex.ServerConfig{
			Listen: fmt.Sprintf(":%s", port),
		})
		s.AddHandler(path, f)
		s.Run()
		servers[port] = s
	} else {
		s = servers[port]
	}
	return s
}

func stopServer(port string) {
	if s, ok := servers[port]; ok {
		fmt.Printf("stop server '%s'\n", port)
		s.Stop()
		delete(servers, port)
	}
}

func TestMain(m *testing.M) {
	defer logger.Stop()
	if !logger.InitLogger("", "info", false) {
		fmt.Println("Failed to init the logger")
		os.Exit(1)
	}
	code := m.Run()
	os.Exit(code)
}

func checkEqualResult(e *Executor, key string) error {
	result, err := utils.PrepareInt(e.GetContextValue(key))
	if err != nil {
		fmt.Printf("'%s' key not found in:\n%v\n", key, e.Context())
		return err
	}
	expected := int64(1)
	if result != expected {
		return fmt.Errorf("Values are distinct:\n\texpected: %v\n\tactual: %v", expected, result)
	}
	return nil
}
