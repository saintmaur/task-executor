package executor

import (
	"bytes"
	"testing"
	"text/template"

	"github.com/thanhpk/randstr"
)

func BenchmarkTaskAsyncRun(b *testing.B) {
	predefined := make(map[string]Scenario)
	predefined["inc"] = Scenario{
		{Name: "value", Data: "{{.one}}+{{.two}}", Type: "set"},
	}
	executorConfig := ExecutorConfig{
		Name:       "benchmark",
		Period:     "1m",
		Threads:    10000,
		Scenario:   []Task{},
		Predefined: predefined,
	}
	e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan)
	if e == nil {
		b.Fatal("Failed to create an executor")
	}
	for i := 0; i < b.N; i++ {
		new(RunAction).Run(e, Task{Name: "run", Target: "inc", Async: "true"})
	}
	e.wg.Wait()
}

func BenchmarkTaskSetContexValueNoTemplate(b *testing.B) {
	executorConfig := ExecutorConfig{
		Name:     "benchmark",
		Period:   "1m",
		Scenario: []Task{},
	}
	e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan)
	if e == nil {
		b.Fatal("Failed to create an executor")
	}
	for i := 0; i < b.N; i++ {
		new(SetAction).Run(e, Task{Name: "result", Data: "1"})
	}
}
func BenchmarkTaskSetContextValueWithSimpleTemplate(b *testing.B) {
	executorConfig := ExecutorConfig{
		Name:     "benchmark",
		Period:   "1m",
		Scenario: []Task{},
		Variables: []VariableConfig{
			{
				Name:  "one",
				Value: "1",
			},
			{
				Name:  "two",
				Value: "2",
			},
		},
	}
	e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan)
	if e == nil {
		b.Fatal("Failed to create an executor")
	}
	for i := 0; i < b.N; i++ {
		new(SetAction).Run(e, Task{Name: "result", Data: "{{.one}}+{{.two}}"})
	}
}
func BenchmarkTaskSetContextValueWithComplexTemplate(b *testing.B) {
	executorConfig := ExecutorConfig{
		Name:     "benchmark",
		Period:   "1m",
		Scenario: []Task{},
	}
	e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan)
	if e == nil {
		b.Fatal("Failed to create an executor")
	}
	var err error
	err = new(ReadFileAction).Run(e, Task{Name: "predefined", Target: "../../configs/tasker/benchmark/predefined.yml"})
	if err != nil {
		b.Fatalf("Failed on reading the file: %s", err)
	}
	err = new(InjectAction).Run(e, Task{Name: "predefined", Data: "{{.predefined}}", Extra: "yaml"})
	if err != nil {
		b.Fatalf("Failed on injecting: %s", err)
	}
	for i := 0; i < b.N; i++ {
		new(SetAction).Run(e,
			Task{Name: "result",
				Data: `{{- $min := .min}}
{{- $result_connection_id := "notfound" -}}
{{- $result_pair := "notfound" -}}
{{ range $pair, $connection_id := .connections -}}
{{- if gt (len $connection_id) 0 -}}
{{- $popularity := (printf "%s_counter" $pair) -}}
{{- if le $popularity $min -}}
{{- $min = $popularity -}}
{{- $result_connection_id = $connection_id -}}
{{- $result_pair = $pair -}}
{{- end -}}
{{- end -}}
{{- end -}}
{"pair": "{{$result_pair}}", "connection_id": "{{ $result_connection_id }}"}`})
	}
}

func BenchmarkTemplateBuiltin(b *testing.B) {
	for i := 0; i < b.N; i++ {
		str := "{{ .Name }} has almost arrived."
		t, err := template.New("simple-template").Parse(str)
		if err != nil {
			b.Fatalf("Failed on parsing template: %s", err)
		}
		var buff bytes.Buffer
		err = t.Execute(&buff, map[string]string{"Name": "Boris Bodunov"})
		if err != nil {
			b.Fatalf("Failed on executing the template: %s", err)
		}
	}
}
func BenchmarkTemplateCustom(b *testing.B) {
	executorConfig := ExecutorConfig{
		Name:     "benchmark",
		Period:   "1m",
		Scenario: []Task{},
		Variables: []VariableConfig{
			{
				Name:  "Name",
				Value: "Boris Bodunov",
			},
		},
	}
	e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan)
	if e == nil {
		b.Fatal("Failed to create an executor")
	}
	err := new(ReadFileAction).Run(e, Task{Name: "predefined", Target: "../../configs/tasker/benchmark/predefined.yml"})
	if err != nil {
		b.Fatalf("Failed on reading the file: %s", err)
	}
	err = new(InjectAction).Run(e, Task{Name: "predefined", Data: "{{.predefined}}", Extra: "yaml"})
	if err != nil {
		b.Fatalf("Failed on injecting: %s", err)
	}
	action := new(SetAction)
	task := Task{Name: "result", Data: "{{ .Name }} has almost arrived."}
	for i := 0; i < b.N; i++ {
		action.Run(e, task)
	}
}
func BenchmarkTaskSetContextValueWithMoreComplexTemplate(b *testing.B) {
	executorConfig := ExecutorConfig{
		Name:     "benchmark",
		Period:   "1m",
		Scenario: []Task{},
	}
	e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan)
	if e == nil {
		b.Fatal("Failed to create an executor")
	}
	var err error
	err = new(ReadFileAction).Run(e, Task{Name: "predefined", Target: "../../configs/tasker/benchmark/predefined.yml"})
	if err != nil {
		b.Fatalf("Failed on reading the file: %s", err)
	}
	err = new(InjectAction).Run(e, Task{Name: "predefined", Data: "{{.predefined}}", Extra: "yaml"})
	if err != nil {
		b.Fatalf("Failed on injecting: %s", err)
	}
	err = new(ReadFileAction).Run(e, Task{Name: "update", Target: "../../configs/tasker/benchmark/update.json"})
	if err != nil {
		b.Fatalf("Failed on reading the file: %s", err)
	}
	err = new(InternAction).Run(e, Task{Name: "message", Data: "{{.update}}"})
	if err != nil {
		b.Fatalf("Failed on interning the data: %s", err)
	}
	new(SetAction).Run(e, Task{Name: "pair", Data: "ETH-BTC"})
	if err != nil {
		b.Fatalf("Failed on setting the data: %s", err)
	}
	action := new(RunAction)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		err = new(ReadFileAction).Run(e, Task{Name: "{{.pair}}", Target: "../../configs/tasker/benchmark/orderbook.json"})
		if err != nil {
			b.Fatalf("Failed on reading the file: %s", err)
		}
		b.StartTimer()
		action.Run(e, Task{Name: "result", Target: "update_orderbook"})
	}
}

func BenchmarkAMQPPublish(b *testing.B) {
	const amqpConfig = `
	{
		"name": "test_channel",
		"url": "amqp://guest:guest@localhost:5672/",
		"message": {
			"content_type": "text/plain"
		},
		"queue": {
			"name": "test_queue",
			"routing_key": "test_key"
		},
		"exchange": {
			"name": "test_exchange",
			"type": "direct",
			"auto_delete": false,
			"durable": true
		}
	}
	`
	predefined := make(map[string]Scenario)
	predefined["process"] = Scenario{
		{Type: "set", Name: "received", Data: "{{.consumer_item}}"},
	}
	executorConfig := ExecutorConfig{Name: "amqp_checker",
		Period:     "1m",
		Predefined: predefined,
	}
	e := NewExecutor(executorConfig, VariablesConfig{}, map[string]Scenario{}, statChan)
	if e == nil {
		b.Fatal("Failed to create an executor")
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		randStr := randstr.String(20)
		b.StartTimer()
		new(AMQPPublishAction).Run(e, Task{Name: "publish", Data: randStr, Target: amqpConfig})
	}
}
